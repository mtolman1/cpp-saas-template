{
  "version": 6,
  "cmakeMinimumRequired": {
    "major": 3,
    "minor": 25,
    "patch": 0
  },
  "configurePresets": [
    {
      "name": "cxx-root",
      "hidden": true,
      "displayName": "cxx base",
      "description": "Base for cxx builds",
      "generator": "Ninja",
      "cacheVariables": {
        "EXCLUDE_PODMAN_TESTS": {
          "type": "BOOL",
          "value": "ON"
        },
        "EXCLUDE_LIQUIBASE_TESTS": "ON",
        "WITH_INTEGRATION_TESTS": "OFF"
      },
      "environment": {
        "PATH": "$env{HOME}/ninja/bin:$penv{PATH}"
      }
    },
    {
      "inherits": "cxx-root",
      "binaryDir": "${sourceDir}/build/debug/basic",
      "name": "debug",
      "displayName": "Debug",
      "description": "Debug build. Runs unit tests but not integration tests (assumes main CI/CD process runs integration tests).",
      "generator": "Ninja",
      "cacheVariables": {
        "CMAKE_BUILD_TYPE": "Debug",
        "EXCLUDE_PODMAN_TESTS": {
          "type": "BOOL",
          "value": "ON"
        },
        "EXCLUDE_LIQUIBASE_TESTS": "ON",
        "WITH_INTEGRATION_TESTS": "OFF"
      },
      "environment": {
        "PATH": "$env{HOME}/ninja/bin:$penv{PATH}"
      }
    },
    {
      "name": "debug-with-integration-no-db",
      "inherits": "cxx-root",
      "binaryDir": "${sourceDir}/build/debug/integration",
      "displayName": "Debug - Integration, No DB",
      "description": "Debug build. Runs unit tests and integration tests.",
      "cacheVariables": {
        "CMAKE_BUILD_TYPE": "Debug",
        "EXCLUDE_LIQUIBASE_TESTS": "OFF",
        "WITH_INTEGRATION_TESTS": "ON"
      }
    },
    {
      "name": "debug-with-integration-local-db",
      "inherits": "cxx-root",
      "binaryDir": "${sourceDir}/build/debug/localdb",
      "displayName": "Debug - Integration, Local DB",
      "description": "Debug build. Runs unit tests and integration tests. Database must be a Postgresql DB running locally",
      "cacheVariables": {
        "CMAKE_BUILD_TYPE": "Debug",
        "EXCLUDE_LIQUIBASE_TESTS": "OFF",
        "WITH_INTEGRATION_TESTS": "ON",
        "DATA_INTEGRATION_TEST_USE_EXTERNAL_DB_AND_CACHE": "ON"
      }
    },
    {
      "name": "debug-with-integration-podman-db",
      "inherits": "cxx-root",
      "binaryDir": "${sourceDir}/build/debug/podman",
      "displayName": "Debug - Integration, Podman DB",
      "description": "Debug build. Runs unit tests and integration tests. Database will be ran inside a Podman container. Podman must be installed!",
      "cacheVariables": {
        "CMAKE_BUILD_TYPE": "Debug",
        "EXCLUDE_LIQUIBASE_TESTS": "OFF",
        "WITH_INTEGRATION_TESTS": "ON",
        "EXCLUDE_PODMAN_TESTS": "OFF"
      }
    },
    {
      "name": "release",
      "inherits": "cxx-root",
      "displayName": "Release",
      "description": "Release build. Does not build test servers. Runs unit tests but not integration tests (assumes main CI/CD process runs integration tests).",
      "generator": "Ninja",
      "binaryDir": "${sourceDir}/build/release/basic",
      "cacheVariables": {
        "CMAKE_BUILD_TYPE": "Release",
        "EXCLUDE_TEST_SERVERS": "ON"
      },
      "environment": {
        "PATH": "$env{HOME}/ninja/bin:$penv{PATH}"
      }
    },
    {
      "name": "release-test",
      "inherits": "cxx-root",
      "displayName": "Test Release",
      "description": "Release build. Does build test servers. Runs unit tests but not integration tests (assumes main CI/CD process runs integration tests).",
      "generator": "Ninja",
      "binaryDir": "${sourceDir}/build/release/test",
      "cacheVariables": {
        "CMAKE_BUILD_TYPE": "Release",
        "EXCLUDE_TEST_SERVERS": "OFF"
      },
      "environment": {
        "PATH": "$env{HOME}/ninja/bin:$penv{PATH}"
      }
    },
    {
      "name": "js-wasm-base",
      "hidden": true,
      "displayName": "JS/WASM base",
      "description": "Base for JS/WASM builds",
      "generator": "Unix Makefiles",
      "cacheVariables": {
        "EXCLUDE_PODMAN_TESTS": {
          "type": "BOOL",
          "value": "ON"
        },
        "EXCLUDE_LIQUIBASE_TESTS": "ON",
        "WITH_INTEGRATION_TESTS": "OFF",
        "EXCLUDE_TEST_SERVERS": "ON",
        "IS_EMSCRIPTEN_BUILD": "ON",
        "CMAKE_CROSSCOMPILING_EMULATOR": "$env{HOME}/emsdk/node/16.20.0_64bit/bin/node"
      },
      "environment": {
        "PATH": "$env{HOME}/ninja/bin:$penv{PATH}"
      },
      "toolchainFile": "$env{HOME}/emsdk/upstream/emscripten/cmake/Modules/Platform/Emscripten.cmake"
    },
    {
      "name": "js-wasm-release",
      "inherits": "js-wasm-base",
      "binaryDir": "${sourceDir}/build/release/wasm",
      "displayName": "JS/WASM Build",
      "description": "Build for JS/WASM code",
      "cacheVariables": {
        "CMAKE_BUILD_TYPE": "Release"
      }
    }
  ],
  "buildPresets": [
    {
      "name": "release",
      "configurePreset": "release"
    },
    {
      "name": "wasm",
      "configurePreset": "js-wasm-release"
    }
  ],
  "testPresets": [
    {
      "name": "release",
      "configurePreset": "release",
      "output": {"outputOnFailure": true},
      "execution": {"noTestsAction": "error", "stopOnFailure": true}
    },
    {
      "name": "wasm",
      "configurePreset": "js-wasm-release",
      "output": {"outputOnFailure": true},
      "execution": {"noTestsAction": "error", "stopOnFailure": true}
    }
  ],
  "workflowPresets": [
    {
      "name": "release",
      "steps": [
        {
          "type": "configure",
          "name": "release"
        },
        {
          "type": "build",
          "name": "release"
        },
        {
          "type": "test",
          "name": "release"
        },
        {
          "type": "package",
          "name": "release"
        }
      ]
    },
    {
      "name": "wasm",
      "steps": [
        {
          "type": "configure",
          "name": "js-wasm-release"
        },
        {
          "type": "build",
          "name": "wasm"
        },
        {
          "type": "test",
          "name": "wasm"
        },
        {
          "type": "package",
          "name": "wasm"
        }
      ]
    }
  ],
  "packagePresets": [
    {
      "name": "release",
      "configurePreset": "release",
      "generators": [
        "TGZ"
      ]
    },
    {
      "name": "wasm",
      "configurePreset": "js-wasm-release",
      "generators": [
        "TGZ"
      ]
    }
  ]
}