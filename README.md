# C++ Service Template

This is a template for C++ SaaS Services. It's designed not for "microservices" but for either
monoliths or normal services. The goal is to make long-term, durable, and maintainable services
with high performance and a plethora of development features (debug tools, test servers, etc.)
rather than to make something "small" and "replaceable".

## License

Copyright 2023 Matthew Tolman

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
