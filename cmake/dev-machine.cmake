message(STATUS "Checking dev machine setup")
include(${CMAKE_SOURCE_DIR}/cmake/deps.cmake)

set(FORMAT_COUNTER 0)

if(${CMAKE_SYSTEM_NAME} MATCHES "Darwin")
    set(MACOSX ON)
    set(EMSCRIPTEN OFF)
elseif(${CMAKE_SYSTEM_NAME} MATCHES "Emscripten")
    set(MACOSX OFF)
    set(EMSCRIPTEN ON)
else()
    set(EMSCRIPTEN OFF)
    set(MACOSX OFF)
endif()

# Check requirement: GIT

find_program(GIT git)

if(NOT GIT)
    message(
            FATAL_ERROR
            "You must install git on your machine. See https://git-scm.com/book/en/v2/Getting-Started-Installing-Git for instructions"
    )
endif()

if(NOT CMAKE_CXX_COMPILER_ID)
    message(
            FATAL_ERROR
            "You must install a C++ compiler on your machine. For Windows, see https://visualstudio.microsoft.com/. For Mac, see https://www.freecodecamp.org/news/install-xcode-command-line-tools/. For Linux, see https://gcc.gnu.org/install/"
    )
endif()

include(${CMAKE_SOURCE_DIR}/cmake/deps.cmake)

# Check requirement: BUN (PREFERRED) OR NODE/NPM/NPX (these are installed together)

find_program(BUN bun)

if (NOT BUN)
    message(WARNING "Could not find bun! Bun helps speed up some build scripts! See https://bun.sh/")
endif ()

find_program(NODE_EXE node nodejs)
find_program(NPM_EXE npm)
find_program(NPX npx)

if(NOT NODE_EXE
        OR NOT NPM_EXE
        OR NOT NPX)
    find_program(NVM nvm)
    if(NVM)
        execute_process(COMMAND ${NVM} install latest COMMAND ${NVM} use latest)
    else()
        if(WIN32)
            message(FATAL_ERROR "You must install NodeJS. See https://nodejs.org/")
        else()
            find_program(CURL curl)
            find_program(WGET wget)
            find_program(BASH bash)
            if(CURL AND BASH)
                execute_process(
                        ${CURL} -o-
                        https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.5/install.sh |
                        ${BASH})
            elseif(WGET AND BASH)
                execute_process(
                        ${WGET} -qO-
                        https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.5/install.sh |
                        ${BASH})
            else()
                message(FATAL_ERROR "You must install NodeJS. See https://nodejs.org/")
            endif()
            find_program(NVM nvm)
            if(NVM)
                execute_process(COMMAND ${NVM} install latest COMMAND ${NVM} use latest)
            else()
                message(FATAL_ERROR "You must install NodeJS. See https://nodejs.org/")
            endif()
        endif()
    endif()
endif()

# Check requirement: CSpell

execute_process(COMMAND ${NPM_EXE} list -g cspell OUTPUT_VARIABLE CSPELL_OUT)

if(CSPELL_OUT MATCHES "(empty)")
    message("Installing cspell")
    find_program(NPM_BUN bun npm REQUIRED)
    execute_process(COMMAND ${NPM_BUN} install -g cspell)
endif()

# Check requirement: Graphviz/Dot

find_program(DOT dot)

if(NOT DOT)
    message(
            FATAL_ERROR
            "You must install Graphviz on your machine. See https://graphviz.org/download/"
    )
endif()

# Check requirement: clang-tidy, clang-format

find_program(CLANG_TIDY clang-tidy)
find_program(CLANG_FORMAT clang-format)

if((NOT CLANG_TIDY
        AND NOT MACOSX
        AND NOT EMSCRIPTEN)
        OR NOT CLANG_FORMAT)
    message("MACOSX: ${CMAKE_SYSTEM_NAME}")
    message(
            FATAL_ERROR
            "You must install LLVM tools (clang-tidy, clang-format, etc). For Windows, see https://community.chocolatey.org/packages/llvm. For Mac, see https://formulae.brew.sh/formula/clang-format#default. For linux, see https://apt.llvm.org/"
    )
endif()

# Check requirement: cppcheck

find_program(CPPCHECK cppcheck)

if(NOT CPPCHECK)
    message(FATAL_ERROR "You must install cppcheck. See http://cppcheck.net/")
endif()

# Check requirement: iwyu

find_program(IWYU_EXE include-what-you-use)

if(NOT IWYU_EXE)
    message(
            WARNING
            "Missing include-what-you-use (iwyu). This tool is not required but may be helpful. For Mac, see https://formulae.brew.sh/formula/include-what-you-use. For Linuxs, install 'iwyu'"
    )
    set(IWYU_EXE
            OFF
            CACHE BOOL "" FORCE)
endif()

# Check requirement: Doxygen

find_package(Doxygen)

if(NOT DOXYGEN_FOUND)
    message("Grabbing Doxygen")
    FetchContent_Declare(
            doxygen
            GIT_REPOSITORY https://github.com/doxygen/doxygen.git
            GIT_TAG Release_1_9_8
            OVERRIDE_FIND_PACKAGE)
    MakeAvailable(doxygen)
endif()

# Check requirement: cmake-format

find_program(CMAKE_FORMAT cmake-format)

if(NOT CMAKE_FORMAT)
    message(
            FATAL_ERROR
            "You must install cmake-format. See https://pypi.org/project/cmake-format/"
    )
endif()

if(NOT EMSCRIPTEN)
    if(WITH_INTEGRATION_TESTS AND NOT EXCLUDE_PODMAN_TESTS)
        # Check requirement: Podman
        find_program(PODMAN podman)

        if(NOT PODMAN)
            message(FATAL_ERROR "Missing podman. See https://podman.io/")
        endif()
    endif()

    # Check server requirement: libpq
    find_package(PostgreSQL)

    if(NOT PostgreSQL_FOUND)
        message(
                FATAL_ERROR
                "You must install postgres/libpq. For Mac, see https://www.postgresql.org/download/macosx/. For Linux, see https://www.postgresql.org/download/linux/ubuntu/. For Windows, see https://www.postgresql.org/download/windows/"
        )
    endif()

    if(WITH_INTEGRATION_TESTS AND NOT EXCLUDE_LIQUIBASE_TESTS)
        # Check server requirement: liquibase
        find_program(LIQUIBASE liquibase)

        if(NOT LIQUIBASE)
            message(
                    FATAL_ERROR
                    "You must install liquibase. See https://www.liquibase.com/download")
        endif()
    endif()
endif()

FetchContent_Declare(
        cmake-scripts
        GIT_REPOSITORY https://github.com/StableCoder/cmake-scripts.git
        GIT_TAG 23.06)

FetchContent_GetProperties(cmake-scripts)
if(NOT cmake-scripts_POPULATED)
    Populate(cmake-scripts)
endif()

include(${cmake-scripts_SOURCE_DIR}/code-coverage.cmake)
include(${cmake-scripts_SOURCE_DIR}/dependency-graph.cmake)
include(${cmake-scripts_SOURCE_DIR}/formatting.cmake)
include(${cmake-scripts_SOURCE_DIR}/tools.cmake)

macro(format_current_cmake)
    find_program(CMAKE_FORMAT_EXE "cmake-format")

    if(CMAKE_FORMAT_EXE)
        string(REPLACE "/" "_" LIST_FILE ${CMAKE_CURRENT_LIST_FILE})
        string(REPLACE "\\" "_" LIST_FILE ${LIST_FILE})
        string(REPLACE "." "_" LIST_FILE ${LIST_FILE})
        set(FORMAT_TARGET_NAME cmake_format_${LIST_FILE})
        cmake_format(${FORMAT_TARGET_NAME} ${CMAKE_CURRENT_LIST_FILE})
        add_dependencies(cmake_format_all ${FORMAT_TARGET_NAME})
    endif()
endmacro()

format_current_cmake()
