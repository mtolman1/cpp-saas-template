include(${CMAKE_SOURCE_DIR}/cmake/deps.cmake)

find_package(Doxygen)

if (NOT TARGET doc)
    add_custom_target(doc ALL)

    include(FetchContent)
    FetchContent_Declare(
            doxygen-styles
            GIT_REPOSITORY https://github.com/jothepro/doxygen-awesome-css.git
            GIT_TAG v2.2.1
    )
    FetchContent_GetProperties(doxygen-styles)
    if (NOT doxygen-styles_POPULATED)
        Populate(doxygen-styles)
    endif ()
    add_custom_target(
            css-doc-copy
            COMMAND ${CMAKE_COMMAND} -E copy_if_different "${doxygen-styles_SOURCE_DIR}/doxygen-awesome.css"
            ${CMAKE_SOURCE_DIR}/docs/doxygen-awesome.css)
    add_dependencies(doc css-doc-copy)
endif ()

function(make_docs)
    set(DOXYFILE ${CMAKE_CURRENT_SOURCE_DIR}/Doxyfile)
    set(TARGET_NAME doc-${PROJECT_NAME})
    message(STATUS "Adding doc target ${TARGET_NAME}")

    add_custom_target(
            ${TARGET_NAME}
            COMMAND ${DOXYGEN_EXECUTABLE} ${DOXYFILE}
            WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
            VERBATIM)

    add_dependencies(doc ${TARGET_NAME})
    add_dependencies(doc-${PROJECT_NAME} css-doc-copy)
endfunction()