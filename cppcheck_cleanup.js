const fs = require('fs')

checkArgs()

const fileToCheck = process.argv[2]
const excludePrefix = process.argv[3]

fs.writeFileSync(
    fileToCheck,
    JSON.stringify(
        JSON.parse(fs.readFileSync(fileToCheck, 'utf-8').toString())
        .filter(entry => !(entry.file || '').startsWith(excludePrefix))
        .map(entry => {
            return {
                ...entry, command: (entry.command || '')
                    .split(' ')
                    .filter(s => !s.startsWith('-I' + excludePrefix))
                    .join(' ')
            }
        })
    )
)

function printUsage() {
    console.error('Usage: node cppcheck_cleanup.js <json_file_path>  <excludeDir>')
}

function checkArgs() {

    if (process.argv.length < 3) {
        printUsage()
        process.exit(1)
    }

    if (process.argv[2] === '--help') {
        console.error('Usage: node cppcheck_cleanup.js [json_file_path]')
        process.exit(0)
    }

    if (process.argv.length < 4) {
        printUsage()
        process.exit(1)
    }

    if (process.argv[3] === '--help') {
        console.error('Usage: node cppcheck_cleanup.js [json_file_path]')
        process.exit(0)
    }
}