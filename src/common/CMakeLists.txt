project(${SERVICE_NAME}-common)

include(${CMAKE_SOURCE_DIR}/cmake/deps.cmake)

add_subdirectory(core)
add_subdirectory(testing)

format_current_cmake()

if (FORMAT_CODE)
    message(STATUS "Adding format code for common")
    file(
            GLOB_RECURSE
            SOURCE_FILES
            *.cpp
            *.h
            *.c
            *.cxx
            *.hpp)

    clang_format(cpp_format_common ${SOURCE_FILES})
    add_dependencies(main_build cpp_format_common)
endif ()
