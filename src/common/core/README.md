# common::core Module

This module is for core code that can be used for both server and client code (e.g. C++ servers,
WASM, mobile apps, etc). 

For now, I included some demo code in core.h, and I added some common patterns I use in my code
(an implementation of defer, and on-the-fly visitor::overload pattern).

## Library export

The library name for this module is `common::core`, so if you have a CMake target that needs to
link to this library you can add something like `target_link_libraries(${TARGET_NAME} PUBLIC common::core)`.

## Documentation

Doxygen documentation is automatically created for the project. The CMake targets which build the docs include:
* main_build (does formatting, executable building, etc)
* doc (builds all docs)
* doc-saas-template-common-core (the name changes if you change the root CMake's PROJECT_NAME)

Documentation is stored under you're output folder in docs/code/common/core. We output dockbook
templates, HTML code, latex templates, and man pages.

## Current Dependencies (remove, update, or move this section as desired) 

The example code in this module can (and should) be replaced. Additionally, feel free to unlink
any example libraries that are linked. The libraries linked by default are:

### cxx_calendar

https://gitlab.com/mtolman/calendars

This is a calendar library I wrote which converts between calendar systems (e.g. Gregorian <-> Julian), and it
calculates holidays (US Independence Day, US Thanksgiving, Easter, etc). I added this since I plan on using it for my own
code. However, feel free to remove it from your system. Something like [HowardHinnet/date](https://github.com/HowardHinnant/date)
or [Boost.Date_Time](https://www.boost.org/doc/libs/1_83_0/doc/html/date_time.html) will
probably be better for your needs.

Just an FYI, quite a bit of the example code does depend on this library, so you will need to remove that code as part
of removing this library.

### ztd.text

https://github.com/soasis/text

This is a text encoding, decoding, and transcoding library. Handles a lot of common text encodings.

### SI

https://github.com/bernedom/SI

This is a units library (meters, feet, seconds, etc) that handles SI units (but not quantities).

For my needs, I only needed SI units for time related units, such as seconds, minutes, etc.
Many projects don't necessarily *need* a units library, but I have had enough issues where
not specifying time units can waste a lot of developer time and can cause subtle bugs. This
mostly comes from code where some functions return seconds, others take in milliseconds, and
some only work with nanoseconds or minutes. By having a unit library as the default, I hope
to reduce (if not eliminate) a class of bugs related to functions dealing with units of
measurements.

If you don't want it or want to replace it, fell free to do so. Some of the example
code uses this library, but that is generally confined to the common::core module.

Alternative unit libraries include [mp-units](https://github.com/mpusz/mp-units) (which is a proposal for C++29 and
handles quantities), [nholthaus/units](https://github.com/nholthaus/units), and
[LLNL/units](https://github.com/LLNL/units).

## Project Example Usage

ADD EXAMPLES OF YOUR COMMON CODE HERE
