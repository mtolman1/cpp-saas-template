# Defer

Location: common/core/defer.h

Defer statement that will defer execution until the scope is left
Execution will happen regardless of whether there was an error

Example:
```cpp
{
     auto fp = fopen("file.txt", "r");
     if (!fp) {
         throw std::runtime_error("could not open file");
     }
     // Guarantees we close our file pointer, even if we later throw or have multiple return statements
     defer { fclose(fp); };

     // some logic here

     if (some_condition) {
         // No need to call fclose, it will be called automatically
         throw std::runtime_exception("sample exception");
     }

     // No need to call fclose, it will be called automatically
     return some_value;
}
```

Based on https://www.modernescpp.com/index.php/visiting-a-std-variant-with-the-overload-pattern/

For an alternative defer, see
https://www.gingerbill.org/article/2015/08/19/defer-in-cpp/#:~:text=The%20defer%20statement%20pushes%20a%20function%20call%20onto,is%20similar%20to%20how%20D%20has%20scope%20%28exit%29.

And yet another alternative: http://the-witness.net/news/2012/11/scopeexit-in-c11/

I prefer this pattern since it works better with code that has a comma inside.
For the Ginger Bill implementation, you could do something like the following to (maybe) make it handle commas better (I haven't checked)

#define defer(...)   auto DEFER_3(_defer_) = defer_func([&](){_VA_ARGS;})

That said, the Ginger Bill implementation works perfect for small, one off statements (which is 98% of defer use cases),
whereas this defer is more verbose and clunky. Also, with Ginger Bill's version you don't need a semi-colon afterwards.

If you want to change the defer statement to use a different variation, feel free to do so.
You will need to change the places that it is used, but finding them all shouldn't be too difficult