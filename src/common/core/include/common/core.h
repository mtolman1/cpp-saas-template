#pragma once

#include "core/defer.h"
#include "core/visitor.h"
#include <calendars/cxx/calendars/holidays.h>
#include <calendars/cxx/calendars/unix_timestamp.h>

/**
 * Core functionality that can be used on the server or on the frontend (via wasm)
 */
namespace common::core {
    /**
     * Example method
     * todo: replace this with your code
     * @return
     */
    int num();
}  // namespace common::core
