#pragma once
#pragma clang diagnostic push
#pragma ide diagnostic   ignored "cppcoreguidelines-special-member-functions"

/** @cond */
namespace common::defer::impl {
    // Struct that will be used for operator overloading
    // we're using our own struct so that we don't have to worry about overloading collisions with other code
    struct DeferrerBuilder {};

    // The deferrer struct uses RAII to delay calling a callable
    // We intentionally don't define any custom constructors or make them explicit
    template<class F>
    struct Deferrer {
        F f;

        ~Deferrer() { f(); }
    };

    // We define a custom operator that takes a builder type and our function callable type so we can generate a deferrable
    //  without the use of parenthesis
    // When we combine this with our macro, we will be able to create a block of code which turns into a deferred lambda
    template<class F>
    Deferrer<F> operator+(DeferrerBuilder, F f) {
        return {f};
    }
}  // namespace common::defer::impl

#define DEFER_(LINE) zz_defer##LINE
#define DEFER(LINE)  DEFER_(LINE)
/** @endcond */

/**
 * Defer statement that will defer execution until the scope is left, at which point the code will run
 * Execution will happen regardless of whether there was an error
 */
#define defer auto DEFER(__LINE__) = common::defer::impl::DeferrerBuilder{} + [ & ]()

#pragma clang diagnostic pop