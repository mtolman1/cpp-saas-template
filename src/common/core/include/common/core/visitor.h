#pragma once

/**
 * Holds overload variant visitor pattern for lambdas
 *
 * Based on https://dev.to/tmr232/that-overloaded-trick-overloading-lambdas-in-c17
 *
 * Alternate implementation: https://arne-mertz.de/2018/05/overload-build-a-variant-visitor-on-the-fly/
 *
 * Standard proposal (P0051): https://www.open-std.org/jtc1/sc22/wg21/docs/papers/2015/p0051r0.pdf
 * Alternate Revised proposal (P1772R1): https://www.open-std.org/jtc1/sc22/wg21/docs/papers/2019/p1772r1.pdf
 */
namespace common::core {
    /**
     * Creates a callable overload that can be used to visit a variant
     * This allows using inline-lambdas for visiting each specialized type
     *
     * Note: This should work for more than just lambdas. Any callable should work.
     *
     * Example:
     * ```cpp
     *   std::variant<int, double, float, std::string> value = get_value();
     *
     *   std::visit(
     *        common::core::overload{
     *            [](const std::string& s) {
     *                std::cout << "Found a string!\n";
     *            },
     *            [](int i) {
     *                std::cout << "Found an integer!\n";
     *            },
     *            [](auto v) { // The "auto" case is the default case for anything not specialized
     *                std::cout << "Found a float or a double\n";
     *            },
     *        }
     *        , value
     *   )
     * ```
     * @tparam Ts
     */
    template<class... Ts>
    struct overload : Ts... {
        using Ts::operator()...;
    };
    template<class... Ts>
    overload(Ts...) -> overload<Ts...>;
}  // namespace common::core
