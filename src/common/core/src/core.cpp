#include "common/core.h"

#include <SI/time.h>

int common::core::num() {
    // Example showing how to use units
    using namespace SI::literals;
    return (5_min).as<SI::seconds_t<int>>().value();
}
