#include <common/core.h>
#include <emscripten/bind.h>

#include <sstream>

namespace wrapper {
    using namespace calendars::holidays;

    constexpr double multiplier = 1000;

    template<typename T>
    auto to_js_timestamp(const T& v) {
        return v.to_rd_date().template to<calendars::unix_timestamp::Date>().timestamp() * multiplier;
    }

    [[nodiscard]] static auto independence_day(double year) -> double { return to_js_timestamp(usa::independence_day(static_cast<int64_t>(year))); }
    [[nodiscard]] static auto veterans_day(double year) -> double { return to_js_timestamp(usa::veterans_day(static_cast<int64_t>(year))); }
    [[nodiscard]] static auto indigenous_peoples_day(double year) -> double {
        return to_js_timestamp(usa::indigenous_peoples_day(static_cast<int64_t>(year)));
    }
    [[nodiscard]] static auto presidents_day(double year) -> double { return to_js_timestamp(usa::presidents_day(static_cast<int64_t>(year))); }
    [[nodiscard]] static auto new_years_eve(double year) -> double { return to_js_timestamp(usa::new_years_eve(static_cast<int64_t>(year))); }
    [[nodiscard]] static auto new_years(double year) -> double { return to_js_timestamp(usa::new_years(static_cast<int64_t>(year))); }
}  // namespace wrapper

EMSCRIPTEN_BINDINGS(core) {
    emscripten::function("independenceDay", wrapper::independence_day);
    emscripten::function("veteransDay", wrapper::veterans_day);
    emscripten::function("indigenousPeoplesDay", wrapper::indigenous_peoples_day);
    emscripten::function("presidentsDay", wrapper::presidents_day);
    emscripten::function("newYearsEve", wrapper::new_years_eve);
    emscripten::function("newYears", wrapper::new_years);
}
