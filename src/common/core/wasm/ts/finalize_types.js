const fs = require('fs')
const path = require('path')

let fileContents = fs.readFileSync(path.join(__dirname, 'core.d.ts'), 'utf-8').toString()

fileContents = fileContents.replace(/export\s*interface\s*MainModule\s*\{((?:\n|.)*)\}(\s|\n)*$/g, '$1').trim()

let res = ''
let inExport = false
for (let line of fileContents.split(/\r?\n/)) {
    line = line.trim()
    if (!inExport && !line.startsWith('export')) {
        line = 'export function ' + line.replaceAll('(_0: number)', '(year: number)')
    }
    else if (!inExport) {
        inExport = line.endsWith('{')
    }
    else if (line.indexOf('}') >= 0) {
        inExport = false
    }
    res += '\n' + line
}

fs.writeFileSync(path.join(__dirname, 'index.d.ts'), res + '\n')