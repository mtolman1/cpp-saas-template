import {independenceDay, veteransDay, indigenousPeoplesDay, presidentsDay, newYearsEve, newYears} from "./index";


// Note: For JS, months are 0-index based while everything else is 1-index based
describe('Holidays', () => {
    it('Independence day', () => {
        expect(new Date(independenceDay(2024))).toEqual(new Date(Date.UTC(2024, 6, 4)))
    })

    it('Veterans day', () => {
        expect(new Date(veteransDay(2024))).toEqual(new Date(Date.UTC(2024, 10, 11)))
    })

    it('Indigenous Peoples Day', () => {
        expect(new Date(indigenousPeoplesDay(2024))).toEqual(new Date(Date.UTC(2024, 9, 14)))
    })

    it('Presidents Day', () => {
        expect(new Date(presidentsDay(2024))).toEqual(new Date(Date.UTC(2024, 1, 19)))
    })

    it('New Years Eve', () => {
        expect(new Date(newYearsEve(2024))).toEqual(new Date(Date.UTC(2024, 11, 31)))
    })

    it('New Years', () => {
        expect(new Date(newYears(2024))).toEqual(new Date(Date.UTC(2024, 0, 1)))
    })
});