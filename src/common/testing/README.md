# common::testing Module

This module is for core code that can be used for both server and client code (e.g. C++ servers,
WASM, mobile apps, etc).

Any logic you would like to have shared between the testing systems would go here.

## Library export

The library name for this module is `common::testing`, so if you have a CMake target that needs to
link to this library you can add something like `target_link_libraries(${TARGET_NAME} PUBLIC common::core)`.

## Documentation

Doxygen documentation is automatically created for the project. The CMake targets which build the docs include:
* main_build (does formatting, executable building, etc)
* doc (builds all docs)
* doc-saas-template-common-core (the name changes if you change the root CMake's PROJECT_NAME)

Documentation is stored under you're output folder in docs/code/common/core. We output dockbook
templates, HTML code, latex templates, and man pages.

## Current Dependencies (remove, update, or move this section as desired) 

The example code in this module can (and should) be replaced. Additionally, feel free to unlink
any example libraries that are linked. The libraries linked by default are:

### doctest

https://github.com/doctest/doctest

Unit testing framework.

### rapidcheck

https://github.com/emil-e/rapidcheck

Property testing framework.

### faker

https://gitlab.com/mtolman/faker

Fake data generation library. Also has module for RapidCheck.
`faker::faker` is fake data library. `faker::rc` RapidCheck integration. 

## Project Example Usage

ADD EXAMPLES OF YOUR COMMON CODE HERE
