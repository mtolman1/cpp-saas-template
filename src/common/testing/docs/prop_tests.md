# Property testing

Property based testing is provided to allow better testing of source code. Macros have been defined
to make defining property tests much easier. These property tests are also integrated with DocTest
so that they can be ran using the same DocTest commandline arguments as simple unit tests, and they
can be included in the same DocTest executables.

Property testing is based on [rapidcheck](https://github.com/emil-e/rapidcheck).

## Example

```cpp
#include <common/testing.h>

// Defines a property test suite; this will appear as a DocTest TEST_CASE
PROP_SUITE("TestSuite") {
    // Defines a property test. This will integrate with DocTest's CHECK
    //  so that DocTest will know if it worked, while also allowing for
    //  rapidcheck to do test shrinkage
    // We do need to define our input so that property test can generate it
    PROP_TEST("Test Case") (const std::vector<int> & initialList) {
        // We now define our test body
        auto reversedList = initialList;
        std::reverse(begin(reversedList), end(reversedList));
        std::reverse(begin(reversedList), end(reversedList));
        
        // Do a property assert, this is required for rapidcheck's input shrinkage
        PROP_ASSERT_EQ(initialList, reversedList);
    };
}
```
