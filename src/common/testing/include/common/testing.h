#pragma once

#include <doctest/doctest.h>
#include <rapidcheck.h>
#include <string>
#include <iostream>
#include <faker-rc.h>

namespace common::testing {
    namespace impl {
        struct PropTest {
            std::string description;
        };
        struct DocTest {
            std::string description;
        };
    }
}

template<typename T>
auto operator/(const common::testing::impl::PropTest& d, const T& f) {
    return rc::check(d.description, f);
}

inline auto operator+(const common::testing::impl::DocTest& d, const bool& f) {
    CHECK_MESSAGE(f, ("Property test '" + d.description + "' failed!"));
}

#define PROP_SUITE(SuiteName) TEST_CASE(SuiteName)
#define PROP_TEST(CheckName) common::testing::impl::DocTest{CheckName} + common::testing::impl::PropTest{CheckName} / []
#define PROP_ASSERT(CASE) RC_ASSERT(CASE)
#define PROP_ASSERT_EQ(LEFT, RIGHT) RC_ASSERT(LEFT == RIGHT)
#define PROP_ASSERT_NE(LEFT, RIGHT) RC_ASSERT(LEFT != RIGHT)
#define PROP_ASSERT_LT(LEFT, RIGHT) RC_ASSERT(LEFT < RIGHT)
#define PROP_ASSERT_LE(LEFT, RIGHT) RC_ASSERT(LEFT <= RIGHT)
#define PROP_ASSERT_GT(LEFT, RIGHT) RC_ASSERT(LEFT > RIGHT)
#define PROP_ASSERT_GE(LEFT, RIGHT) RC_ASSERT(LEFT >= RIGHT)
