#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include <common/testing.h>

PROP_SUITE("TestSuite") {
    PROP_TEST("Test Case") (const std::vector<int> &l0){
        auto l1 = l0;
        std::reverse(begin(l1), end(l1));
        std::reverse(begin(l1), end(l1));
        PROP_ASSERT_EQ(l0, l1);
    };
}
