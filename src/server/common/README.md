# server::core Module

This module is for core server code that can only (or should only) be used for the server. Additionally, this is code
NOT meant to receive bindings to other languages.

Any logic you would like to have shared server modules (and only server modules) would go here.

For now, I included some demo code in core.h. I also set it up to provide some common, useful libraries
to any code which imports it ([nlhomman/json](https://github.com/nlohmann/json), [gabime/spdlog](https://github.com/gabime/spdlog),
the server::common module).

## Library export

The library name for this module is `server::common`, so if you have a CMake target that needs to
link to this library you can add something like `target_link_libraries(${TARGET_NAME} PUBLIC server::common)`.

## Documentation

Doxygen documentation is automatically created for the project. The CMake targets which build the docs include:
* main_build (does formatting, executable building, etc)
* doc (builds all docs)
* doc-saas-template-server-common (the name changes if you change the root CMake's PROJECT_NAME)

Documentation is stored under you're output folder in docs/code/server/common. We output dockbook
templates, HTML code, latex templates, and man pages.

## Current Dependencies (remove, update, or move this section as desired)

The example code in this module can (and should) be replaced. Additionally, feel free to unlink
any example libraries that are linked. The libraries linked by default are:

### core::common

This is the core::common module (src/core/common).

### nlhomman/json

https://github.com/nlohmann/json

This is a JSON library. It's meant primarily for ease of use rather than speed (that doesn't mean it is slow, but
that it does not advertise speed as a main selling point). It also supports binary data formats, such as BSON, CBOR,
MessagePack, UBJSON, and BJData.

The inclusion of this library is more of a quick starter for during initial development and discovery work
(e.g. experimenting with JSON vs binary data formats for service-to-service.service-to-client communication).
After the requirements and some core decisions are made, it could be switched for another library specialized
for performance. For instance, if the server is primarily injesting JSON with simple plain/text responses, then
[simdjson](https://github.com/simdjson/simdjson) might be all you need.

Alternative multi-format libraries include:
- [jsoncons](https://github.com/danielaparker/jsoncons) (JSON, BSON, CBOR, MessagePack, CSV, UBJSON, JSONPath, JMESPath)

Alternative JSON-only libraries include:
- [simdjson](https://github.com/simdjson/simdjson)
- [rapidjson](https://rapidjson.org/)
- [jsoncpp](https://github.com/open-source-parsers/jsoncpp)

Alternative BSON-only libraries include:
- [libbson](https://github.com/mongodb/mongo-c-driver/tree/master/src/libbson) (part of the mongodb driver)
- [bson-cxx](https://github.com/dwight/bson-cxx)

Alternative CBOR-only libraries include:
- [libcbor](https://github.com/PJK/libcbor) (C)
- [tinycbor](https://github.com/intel/tinycbor)
- [cbor-cpp](https://github.com/naphaso/cbor-cpp)
- [cbor11](https://github.com/jakobvarmose/cbor11)
- [RantyDave/cppbor](https://github.com/RantyDave/cppbor)
  
Alternative MessagePack-only libraries include:
- [camgunz/cmp](https://github.com/camgunz/cmp)
- [mpack](https://github.com/ludocode/mpack)
- [msgpack-c](https://github.com/msgpack/msgpack-c)
- [cppack](https://github.com/mikeloomisgg/cppack)
- [msgpack11](https://github.com/ar90n/msgpack11)

Alternative UBJSON-only libraries include:
- [ubj](https://github.com/Steve132/ubj)
- [UbjsonCpp](https://github.com/WhiZTiM/UbjsonCpp)

### spdlog

https://github.com/gabime/spdlog

A fast logging library. Alternatives include:
- [Boost.log](https://github.com/boostorg/log)
- [EasyLogging++](https://github.com/abumq/easyloggingpp)

### xsimd

https://github.com/xtensor-stack/xsimd

A SIMD intrinsics library.

Alternatives include:
- [simde](https://github.com/simd-everywhere/simde/tree/master)
- [Google Highway](https://github.com/google/highway)

The default SIMD architecture is AVX2. You can change this with the CMAKE cache variable `SERVER_SIMD`.

## Project Example Usage

ADD EXAMPLES OF YOUR COMMON CODE HERE
