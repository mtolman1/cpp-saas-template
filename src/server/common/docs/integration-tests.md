# Integration Tests

Integration tests are based off the [JetBrains HTTP client file format](https://www.jetbrains.com/help/idea/http-client-in-product-code-editor.html).

## Request Format

The files are a simplified version of HTTP requests. For example:

```http request
### Request name here
POST http://example.com/holidays/us/veteransDay
Authorization: Auth Header Here
Content-Type: application/json

{
  "content": "body"
}
```

The three hashtags (`###`) indicates the start of a new request. Following the hashtags is the
request name.

The next line is the HTTP request declaration with a method (`GET`, `POST`, etc) and a URL.
Do note that for this format we don't specify the HTTP version. All requests are assumed to be
HTTP/1.1 (HTTP/2 and HTTP/3 are binary formats, so something else will need to be done for them).

After the declaration comes the headers. After the headers is a blank line followed by the body.

This forms a basic request which will ensure that the server returns with a 200 status.

## Test Assertions

To do more complicated assertions we can use JavaScript code. We can declare a JS codeblock
with the tags `> {%` and end it with `%}`. The `client` and `response` objects will be in scope.

### client object

The `client` object allows writing tests with `client.test`, running assertions with
`client.assert`, accessing and saving global date (isolated to a test file) with `client.global`,
and logging file with `client.log`.

> **Important:** JetBrains client object has an `exit` method, however our custom test runner
> does NOT support `client.exit`!

#### Global data

Global data allows requests to set and retrieve global state. This is important for requests which
depend on a previous request. For instance, a request getting the current user information depends
on the login request. By setting global state, requests can "communicate" with each other.

The `client.global` object is a key-value store. Values are inserted by a key and retrieved by a
key. The global object only works with string keys and string values. 

> Tip: Use `JSON.stringify` and `JSON.parse` to convert non strings to and from string values

The global object has the following properties:
* `set(key: string, value: string) : void`
    * Sets a value at a given key. The value will be coerced into a string if it was not a string previously 
* `get(key: string) : string`
    * Retrieves the string value stored at a key. If the key does not exist, will return `undefined`
* `clear(key: string) : void`
    * Clears the value stored at a key (if it is present)
* `isEmpty() : bool`
  * Returns whether the key/value store is empty
* `clearAll() : void`
  * Removes all entries from the key/value store

> Tip: To know if a value is set, use `if(typeof client.global.get(key) !== 'undefined')`

> Tip: To have a default value for get, use `client.global.get(key) || defaultValue`

## Environments

Test environments allow substituting values into the request objects. This substitution allows
the same request file to be used for multiple servers.

In the request file, values wrapped in double curly braces (e.g. `{{name}}`) declare a named
substitution point. When an environment is loaded, values will be substituted to the corresponding
substitution point.

Environments are stored the `http-client.env.json` file. The file is structured as follows:

```json
{
  "environment-name-1": {
    "env-var-1": "env value",
    "env-var-2": "env value"
  },
  "environment-name-2": {
    "env-var-1": "diff value",
    "env-var-2": "diff value"
  }
}
```

The first JSON level are the available environments (e.g. `environment-name-1`). These environments
then have a JSON object of key/value pairs where the key is the substitution name and the value
is the value to substitute (e.g. `"env-var-1": "env value"`).

We'll now look at a full example. Here is our request file:

```http request
### Home
GET {{url}}/index.html
Authorization: {{auth}}
```

Here is our environment file:

```json
{
  "local-dev": {
    "url": "http://127.0.0.1:18080",
    "auth": ""
  },
  "staging": {
    "url": "https://service-staging.example.com",
    "auth": "Bearer stagingKey"
  }
}
```

If we run with the `local-dev` environment, our request becomes the following:

```http request
GET http://127.0.0.1:18080/index.html
Authorization: 
```

If we run with the `staging` environment, our request becomes the following:

```http request
GET https://service-staging.example.com
Authorization: Bearer stagingKey
```

## CLI Tool

Our CLI tool runs using [Node](https://nodejs.org/en/download). CMake will handle checking for
node and installing any dependencies. If you need to reinstall dependencies, rerun CMake with
the option `FORCE_NPM_UPDATE` set to `ON`, or delete the `node_modules` folder and rerun CMake.

> Note for Bun users: There is currently an issue with Bun's bundled zlib in some environments
> where it is unable to decode the gzip compression of the test servers. If you run into issues
> with Bun, please switch to Node.

To manually run integration tests, run the following:

```bash
node $PATH_TO_PROJECT/src/server/common/integration-tester/test.js \
  --test-directory $PATH_TO_TEST_DIR \
  --env-file=$PATH_TO_PROJECT/http-client.env.json \
  --env $HTTP_ENV \
  --log-level debug
```

For instance, if our working directory was the same as our project directory, and we wanted to
run our data module tests against our local test environment, we would run the following:

```bash
PATH_TO_PROJECT=.; \
PATH_TO_TEST_DIR=$PATH_TO_PROJECT/src/server/data/test-server/requests; \
HTTP_ENV=local-dev; \
node $PATH_TO_PROJECT/src/server/common/integration-tester/test.js \
  --test-directory $PATH_TO_TEST_DIR \
  --env-file=$PATH_TO_PROJECT/http-client.env.json \
  --env $HTTP_ENV \
  --log-level debug
```

### Test Servers and integration tests

> Note: This step is usually not required to run manually. CMake will add test server
> integration tests to the CTest target automatically

All the test servers using the `server::unsafe` module will have CLI access by default to run
the CLI integration tool automatically (this is why they are considered test servers and not
production servers). Once a server is done running integration tests, it will log the results
and exit.

To have a test server run integration tests, run something like the following:

```bash
test-server-command \
  --log-level=debug \ 
  --test-node=$NODE_PATH \
  --test-script=$PATH_TO_PROJECT/src/server/common/integration-tester/test.js \
  --test-directory=$PATH_TO_TEST_DIR \
  --test-env-file=$PATH_TO_PROJECT/http-client.env.json \
  --test-env=$HTTP_ENV
```

For instance, to run our `server::compute` module tests, we would have something like the
following:

```bash
NODE_PATH=/usr/local/bin/node; \
PATH_TO_PROJECT=.; \
PATH_TO_TEST_DIR=$PATH_TO_PROJECT/src/server/compute/test-server/requests; \
HTTP_ENV=local-dev; \
saas-template-compute-test-server \
  --log-level=debug \ 
  --test-node=$NODE_PATH \
  --test-script=$PATH_TO_PROJECT/src/server/common/integration-tester/test.js \
  --test-directory=$PATH_TO_TEST_DIR \
  --test-env-file=$PATH_TO_PROJECT/http-client.env.json \
  --test-env=$HTTP_ENV
```
