const http = require('http')
const fs = require('fs')
const path = require('path')
const { HTTPParser } = require('http-parser-js');
const url = require('url')
const utils = require('util')
const uuid = require('uuid')
const zlib = require('zlib')

/**
 *
 * @param {Logger} logger
 * @param {object} env
 * @returns {{runFiles: ((function(*): Promise<void>)|*)}}
 */
module.exports = (logger, env) => {

    async function runFiles(testFiles) {
        const tid = uuid.v4()
        logger = logger.child({tid: tid})
        logger.info(`STARTING RUN`)
        const results = await Promise.all(testFiles.map(runFile(tid, logger, env)))
        const passed = results.filter(([pass, ]) => !pass).length === 0
        const totals = results.map(([_, res]) => res).reduce((acc, cur) => {
            return {
                pass: acc.pass + cur.pass,
                error: acc.error + cur.error,
                fail: acc.fail + cur.fail,
                timings: acc.timings.concat(cur.timings)
            }
        }, {pass: 0, fail: 0, error: 0, timings: []})

        const {pass, fail, error} = totals

        const msg = `RUN TOTALS | PASS ${pass} | FAIL ${fail} | ERROR ${error}`
        if (passed) {
            logger.info(msg)
        }
        else {
            logger.error(msg)
        }
        return [passed, totals]
    }

    return {
        runFiles
    }
}

function runFile(tid, logger, env) {
    return async (httpFilePath) => {
        const httpFile = fs.readFileSync(httpFilePath, 'utf-8').toString()
        const suiteName = path.basename(httpFilePath)
        logger = logger.child({suite: suiteName})


        logger.info(`RUNNING TEST SUITE: ${suiteName}`)
        const result = await executeTests(tid, logger, httpFile, env)
        const {fail, pass, error} = result

        logger.info(`SUITE TOTALS | PASS ${pass} | FAIL ${fail} | ERROR ${error}`)
        if (fail > 0 || error > 0) {
            logger.error("SUITE FAILED")
            return [false, result]
        } else {
            logger.info("SUITE PASSED")
            return [true, result]
        }
    }
}

/**
 *
 * @param {UrlWithStringQuery} parsedUrl
 * @param httpRequest
 * @param callback
 */
function makeRequest(logger, parsedUrl, httpRequest, callback) {
    const opts = {
        host: parsedUrl.hostname,
        port: parsedUrl.port,
        path: parsedUrl.path,
        protocol: parsedUrl.protocol,
        headers: httpRequest.headers,
        method: httpRequest.method
    }
    const req = http.request(opts, res => {
        let resBody = ''
        res.setEncoding('utf8');
        res.on('data', (chunk) => {
            resBody += chunk
        });
        res.on('end', () => {
            res.status = res.statusCode
            res.headers.valueOf = (headerName) => res.headers[headerName.toLowerCase()];
            res.headers.valuesOf = (headerName) => [res.headers[headerName.toLowerCase()]].filter(x => x);
            res.contentType = {
                mimeType: (res.headers.valueOf('content-type') || '').split(';')[0].trim().toLowerCase(),
                charset: (((res.headers.valueOf('content-type') || '').split(';')[1] || '').trim().split('=')[1] || '').toLowerCase()
            }

            res.body = resBody
            console.log(res.body)
            if (res.contentType.mimeType.toLowerCase() === 'application/json') {
                res.body = JSON.parse(res.body)
            }
            callback(null, res)
        });
        res.on('error', (err) => {
            callback(err, null)
        })
    })

    req.on('error', (e) => {
        logger.error(`Problem with request ${httpRequest.method} ${parsedUrl.protocol}//${parsedUrl.host}${parsedUrl.path}. Error: ${e.message}`)
        callback(e, null)
    });

    req.write(httpRequest.bodyChunks.map(b => b.toString('utf8')).join(''));
    req.end();
}

function extractCheckCode(body = '') {
    const re = /> {%((?:.|\n)*?)%}/g
    const copy = body

    let code = ''

    for (const match of copy.matchAll(re)) {
        body = body.replace(match[0], '')
        code += match[1]
    }

    return {
        body,
        code
    };
}

async function executeTests(tid, logger, file, args) {
    const commandSplit = /^### /gm;

    let pass = 0
    let error = 0
    let fail = 0
    let timings = []

    let globals = {
        runId: tid
    }

    const requestRunner = runRequest(tid, file, args, globals)

    const promises =
        file.split(commandSplit)
            .map(l => l.trimStart('\n'))
            .filter(l => l)
            .map(replaceVars(args))
            .map(async ([requestName, request]) => {
                const start = timer()
                try {
                    if (await requestRunner(logger.child({request: requestName}), request)) {
                        ++pass
                    } else {
                        ++fail
                    }
                } catch (e) {
                    logger.error(`Error when trying to run test. ${e}`)
                    ++error
                }
                const end = start()
                timings.push(end)
            })

    await Promise.all(promises)
    return {pass, fail, error, timings}
}

function timer(){
    const start = process.hrtime()
    return () => {
        return process.hrtime(start)
    }
}

function runRequest(tid, file, args, globals) {
    return async (logger, request = '') => {
        const top = request.split(/\r?\n\r?\n/, 1)
        top.push(request.substring(top[0].length).replace(/^\r?\n\r?\n/, ''))
        top[0] = top[0].replaceAll(/\r?\n/g, '\r\n')

        const lines = top[0].split('\r\n')
        const [method, fullUrl, httpVersion] = lines[0].split(' ')

        /** @type UrlWithStringQuery */
        const parsedUrl = url.parse(fullUrl)
        lines[0] = `${method} ${parsedUrl.path} ${httpVersion || 'HTTP/1.1'}\r\nHost: ${parsedUrl.host}`

        top[0] = lines.join('\r\n')

        const bodyPieces = extractCheckCode(top[1])
        top[1] = bodyPieces.body
        const toRun = bodyPieces.code

        if (top[1].length) {
            request = top[0] + '\r\nContent-Length: ' + top[1].length + '\r\n\r\n' + (top[1] || '')
        } else {
            request = top[0] + '\r\n\r\n'
        }

        const httpRequest = await (utils.promisify(parse)(logger, request))
        logger.debug(`MAKING REQUEST ${httpRequest.method} ${fullUrl}`)
        const response = await (async () => {
            let lastError = null
            for (let i = 0; i < 3; ++i) {
                try {
                    return await (utils.promisify(makeRequest)(logger, parsedUrl, httpRequest));
                }
                catch (e) {
                    lastError = e
                }
            }
            throw lastError
        })()

        logger.debug(`FINISHED REQUEST ${httpRequest.method} ${fullUrl}; STATUS: ${response.status}`)

        const __re = /<anonymous>:(?<line>\d+):(?<col>\d+)/g
        let __passed = true
        {
            let childLogger = logger
            const client = {
                test: function (testName, testFunc) {
                    childLogger = logger.child({test: testName})
                    childLogger.info(`STARTING TEST`)
                    try {
                        testFunc()
                        childLogger.info(`TEST PASSED`)
                    } catch (error) {
                        childLogger.error(`TEST FAILED`)
                        childLogger.error(`ERROR: ${error}`)
                        childLogger.error(`RESPONSE: ${JSON.stringify({
                            status: response.status,
                            body: typeof response.body === 'string' ? response.body : JSON.stringify(response.body),
                            headers: response.headers
                        })}`)
                        throw error
                    }
                },
                isCpp: true,
                log: msg => childLogger.info(`client.log: ${msg}`),
                assert: function (passed, errMessage = null) {
                    if (!passed) {
                        __passed = false
                        var err = new Error((errMessage || 'ASSERTION FAILED') + ' at ');
                        var callerLine = err.stack.split("\n")[2];
                        const line = callerLine.split(':').slice(-2)[0]
                        const index = (+line) - 1
                        const lines = toRun.split(/\r?\n/)
                        err.message += `'${lines[index]}` || err.stack
                        throw err
                    }
                },
                exit: () => {
                    childLogger.error('Exit is not supported!')
                    throw new Error('Exit is not supported in the C++ version')
                },
                global: {
                    set(varName, varValue) {
                        globals[varName] = `${varValue}`
                    },
                    get(varName) {
                        return globals[varName]
                    },
                    clear(varName) {
                        if (varName in globals) {
                            delete globals[varName]
                        }
                    },
                    isEmpty() {
                        return Object.values(globals).length === 0
                    },
                    clearAll() {
                        const keys = Object.keys(globals)
                        for (const key of keys) {
                            delete globals[key]
                        }
                        globals['runId'] = tid
                    }
                }
            }
            eval(toRun)
        }
        return __passed
    }
}

function replaceVars(args = {}) {
    const terminalRegex = /^(\w+)}}/
    return function (content = '') {
        const pieces = content.split(/\n/)
        const title = pieces[0].trim()
        content = pieces.slice(1).join('\n')
        let index
        let lastIndex = 0

        const replacements = {}

        while ((index = content.indexOf('{{', lastIndex)) >= 0) {
            const start = index
            let end = index + 2
            let match = content.substring(index + 2).match(terminalRegex)
            if (match) {
                const varName = match[1]
                if (!(varName in replacements)) {
                    if (!(varName) in args) {
                        replacements[varName] = ''
                    } else {
                        replacements[varName] = args[varName] || ''
                    }
                }

                const newEnd = start + replacements[varName].length
                end += match[0].length
                content = content.slice(0, start) + replacements[varName] + (content.slice(end) || '')
                end = newEnd
            }
            lastIndex = end
        }
        return [title, content]
    }
}

function parse(logger, request, callback) {
    const parser = new HTTPParser(HTTPParser.REQUEST)
    let shouldKeepAlive;
    let upgrade;
    let method;
    let url;
    let versionMajor;
    let versionMinor;
    let headers = {};
    let trailers = [];
    let bodyChunks = [];
    let complete = false
    let httpRequest = {}

    parser[HTTPParser.kOnHeadersComplete] = function (req) {
        shouldKeepAlive = req.shouldKeepAlive;
        upgrade = req.upgrade;
        method = HTTPParser.methods[req.method];
        url = req.url;
        versionMajor = req.versionMajor;
        versionMinor = req.versionMinor;
        headers = Object.fromEntries(chunk(2, req.headers.map(s => s.toLowerCase())));
    };

    parser[HTTPParser.kOnBody] = function (chunk, offset, length) {
        bodyChunks.push(chunk.slice(offset, offset + length));
    };

    // This is actually the event for trailers, go figure.
    parser[HTTPParser.kOnHeaders] = function (t) {
        trailers = t;
    };

    parser[HTTPParser.kOnMessageComplete] = function () {
        httpRequest = {
            shouldKeepAlive,
            upgrade,
            method,
            url,
            versionMajor,
            versionMinor,
            headers,
            trailers,
            bodyChunks,
        }
        complete = true
    };

    // Since we are sending the entire Buffer at once here all callbacks above happen synchronously.
    // The parser does not do _anything_ asynchronous.
    // However, you can of course call execute() multiple times with multiple chunks, e.g. from a stream.
    // But then you have to refactor the entire logic to be async (e.g. resolve a Promise in kOnMessageComplete and add timeout logic).
    const res = parser.execute(Buffer.from(request, 'utf8'));
    if (typeof res !== 'number') {
        logger.error(`Could not parse request [${request.split(/\r?\n/g)[0]}] Error: ${res}`)
        callback(res, null)
    }
    parser.finish();

    if (!complete) {
        logger.error(`Could not parse request [${request.split(/\r?\n/g)[0]}] Error: Did not finish parsing. Last state: ${parser.state}`)
        return callback(res, null)
    }

    callback(undefined, httpRequest)
}

function* chunk(size, iterable) {
    const iter = iterable || [];
    let chunks = [];
    for (const elem of iter) {
        if (chunks.length >= size) {
            yield chunks;
            chunks = [];
        }
        chunks.push(elem);
    }
    if (chunks.length) {
        yield chunks;
    }
}