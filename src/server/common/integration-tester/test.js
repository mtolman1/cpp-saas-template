const fs = require('fs')
const path = require('path')
const winston = require('winston')
const {program} = require('commander')
const percentile = require('just-percentile')
const mean = require('just-mean')
const median = require('just-median')
const standardDeviation = require('just-standard-deviation')
const variance = require('just-variance')

program
    .option('--with-timestamped-logs', 'Will add the current date and time to log file names')
    .option('--silent', 'Will turn off output to the console')
    .option('--no-log', 'Will turn off generating a log file for the run')
    .option('--log-level <level>', 'Log level to use', 'info')
    .option('--env-file <env_file>', 'Path to env file to use for populating the environment')
    .option('--env <env>', 'Name of the environment to use from the environment file. Use if there are multiple environments in a file')
    .option('--test-directory <directory>', 'Directory containing the HTTP files to use for testing', process.cwd())
    .option('--num-users <num_users>', 'Number of simultaneous users to run with', 1)
    .option('--suites <suites>', 'Comma separated list of suites to run. Each .http file is a separate suite', '')
    .showHelpAfterError(true)
    .showSuggestionAfterError(true)

program.parse()

const programOptions = program.opts()

const logFile = path.join(programOptions.testDirectory, programOptions.withTimestampedLogs ? 'test_run' + (new Date()).toISOString() + '.log' : 'test_run.log')

const format = winston.format.combine(
    winston.format.timestamp({
        format: 'YYYY-MM-DD hh:mm:ss.SSS A',
    }),
    winston.format.align(),
    winston.format.printf((info) => `[${info.timestamp}] [log_level=${info.level}] [run=${info.tid || '<none>'}] [suite=${info.suite || '<none>'}] [request=${info.request || '<none>'}] [test=${info.test || '<none>'}] \t${info.message}`)
)

const winstonOptions = {
    format,
    transports: [],
}

let logLevel = 'info'
let understoodLogLevel = true;
if (programOptions.logLevel.length > 0) {
    const logLevelLowercase = programOptions.logLevel.toLowerCase()
    if (winston.config.syslog.levels.hasOwnProperty(logLevelLowercase)) {
        logLevel = logLevelLowercase
    }
    else {
        understoodLogLevel = false
    }
}

if (!programOptions.silent) {
    winstonOptions.transports.push(
        new winston.transports.Console({
            level: logLevel,
            format: winston.format.combine(
                winston.format.colorize({ message: false, level: true }),
                format
            )
        })
    )
}

if (!programOptions.noLog) {
    winstonOptions.transports.push(
        new winston.transports.File({
            filename: logFile,
            level: logLevel,
            options: {flags: 'w'},
        })
    )
}

const logger = winston.createLogger(winstonOptions);

if (!understoodLogLevel) {
    logger.warn(`Unknown log level ${programOptions.logLevel}, using default log level`)
}

if (!path.resolve(programOptions.testDirectory) || !fs.existsSync(programOptions.testDirectory) || !fs.lstatSync(programOptions.testDirectory).isDirectory()) {
    logger.error('Must provide a valid test directory!')
    program.help({error: true})
}

const testFiles = findExtension('.http', path.resolve(programOptions.testDirectory))

let env = programOptions.envFile ? loadEnv(programOptions.envFile, programOptions.env) : {}

logger.debug('Using env: ' + JSON.stringify(env))

const {runFiles} = require('./impl')(logger, env)

async function runTestSuites(numClients) {
    logger.info(`Beginning run with ${numClients} client(s)`)

    let filteredFiles = testFiles

    if (programOptions.suites) {
        const suitesToRun = new Set(programOptions.suites.split(',').map(s => (s.endsWith('.http')) ? s : `${s}.http`))
        const basePath = path.resolve(programOptions.testDirectory) + '/'
        filteredFiles = testFiles
            .map((f = '') => {
                return f.replace(basePath, '', 1)
            })
            .filter(f => suitesToRun.has(f))
    }

    const results = await Promise.all((new Array(numClients)).fill(1).map(async x => await runFiles(filteredFiles)))
    const summary = results.reduce((acc, [passed, cur]) => {
        return {
            allPassed: acc.allPassed && passed,
            pass: acc.pass + cur.pass,
            error: acc.error + cur.error,
            fail: acc.fail + cur.fail,
            timings: acc.timings.concat(cur.timings)
        }
    }, {allPassed: true, pass: 0, error: 0, fail: 0, timings: []})

    const timings = summary.timings.map(([seconds, nanoseconds]) => seconds * 1000 + nanoseconds * 0.0000001)

    if (timings.length) {
        const p90 = percentile(timings, 0.9)
        const p95 = percentile(timings, 0.95)
        const p99 = percentile(timings, 0.99)

        const v = variance(timings)
        const mn = mean(timings)
        const md = median(timings)
        const sd = standardDeviation(timings)

        logger.info(`TIMINGS (ms) | USERS ${numClients} | REQUESTS ${timings.length} | MAX ${Math.max(...timings)} | MIN ${Math.min(...timings)} | MEAN ${mn} | MEDIAN ${md} | VARIANCE ${v} | STANDARD DEVIATION ${sd} | P90 ${p90} | P95 ${p95} | P99 ${p99}`)
        const msg = `SUMMARY | ${summary.allPassed ? 'SUCCESS' : 'FAILURE'} | PASS ${summary.pass} | FAIL ${summary.fail} | ERROR ${summary.error}`

        if (!summary.allPassed) {
            logger.error(msg)
            process.exit(1)
        } else {
            logger.info(msg)
        }
    }
    else {
        logger.error("No tests were ran")
        process.exit(1)
    }
}

runTestSuites(+programOptions.numUsers)

function loadEnv(envFilePath, envName = null) {
    const envFile = JSON.parse(fs.readFileSync(envFilePath).toString())
    return (envName && envFile[envName]) || Object.values(envFile)[0]
}


function findExtension(ext, dir) {
    return fs.readdirSync(dir)
        .flatMap(file => {
            const filePath = path.join(dir, file)
            if (!fs.lstatSync(filePath).isDirectory()) {
                return path.extname(file) === ext ? [filePath] : []
            }
            return findExtension(ext, filePath)
        })
}