const fs = require('fs')

const contents = `const char* ${process.argv[2]} = ${JSON.stringify(fs.readFileSync(process.argv[3], 'utf-8').toString())};\n`

const outFile = process.argv[4]

if (!fs.existsSync(outFile) || contents !== fs.readFileSync(outFile, 'utf-8').toString()) {
    fs.writeFileSync(
        outFile,
        contents
    )
}
