macro(defsimd SIMD_NAME MSVCFLAG GCCFLAG)
    add_library(simd_${SIMD_NAME} INTERFACE)
    if(MSVC)
        target_compile_options(simd_${SIMD_NAME} INTERFACE /arch:${MSVCFLAG})
    else()
        target_compile_options(simd_${SIMD_NAME} INTERFACE -m${GCCFLAG} -ftree-vectorize)
    endif()
    add_library(simd::${SIMD_NAME} ALIAS simd_${SIMD_NAME})
endmacro()

defsimd(sse SSE sse)
defsimd(sse2 SSE2 sse2)
defsimd(sse4_1 SSE4.1 sse4.1)
defsimd(sse4_2 SSE4.2 sse4.2)
defsimd(avx AVX avx)
defsimd(avx2 AVX2 avx2)
defsimd(avx512 AVX512 avx512f)
