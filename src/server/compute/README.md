# server::compute Module

This module is for server-side logic. It is meant to perform business logic, computations, etc. and to be both
*stateless* and *side-effect free*. That means this module doesn't talk to a database, publish messages, receive
requests (outside of the test server), check cookies, etc.

The goal is to put all the needed business logic into an isolated module where it can be easily tested and verified.
Then, once the code is verified, it can be imported into any other modules.

Additionally, a test server is provided to allow teams to easily expose the module's methods to non-developers for
verification, such as a QA team, subject-matter-expert (SME), etc. That way developers can get quick and immediate feedback
about system correctness early and often (e.g. before a database schema is finalized and before each deploy).

For now, there is some demo code for calculating holiday dates in various ways. There is also an example test server
to show how to expose code through a (development only) API. Do note that the provided test server is meant for
development use only! It should never be used in a production environment!

The reason it shouldn't be used is that the current test server provides the following features:

* Performing integration tests on itself by running CLI commands
* It does need to be activated by providing a CLI argument though
* Not providing HTTPS support
    * Currently, the CMake build does not link to OpenSSL or other crypto libraries. There is some infrastructure in
      place to allow turning on OpenSSL (such as the code conditionally turning on CLI args), but without changing CMake
      files HTTPS is not supported.
* Providing an (extensible) interactive shell which can control the server for anyone with access to stdin
    * It does need to be activated by providing a CLI argument though

However, those features are useful for testing and debugging purposes. For testing purposes, realistic but not real,
test data is used. A lack of encryption-by-default does not compromise real user data since it is not in use.
Additionally, the server will either be running locally or on an internal network where the risk of man-in-the-middle
attacks is much lower. Additionally, many development environments use self-signed certificates and both developers
and non-developers using those environments are trained to ignore the safety warnings about an insecure certificate.
This poses a problem since it both negates the benefits of HTTPS preventing man-in-the-middle attacks, and it trains
staff to ignore blatant security issues which could cause risk exposure when browsing public websites.

Performing integration tests with a few CLI options makes running a full integration test suite as part of development
much easier for developers. It also allows running integration tests as part of the CMake build (which is enabled by
default). However, due to how the integration framework is currently written, it does require CLI access, and program
arguments are passed to the CLI command. Neither of those situations are desirable for a production system.

## Current Dependencies (remove, update, or move this section as desired)

The example code in this module can (and should) be replaced. Additionally, feel free to unlink
any example libraries that are linked. The libraries linked by default are:

### server::common

The server::common module.

## Library export

The library name for this module is `server::compute`, so if you have a CMake target that needs to
link to this library you can add something like `target_link_libraries(${TARGET_NAME} PUBLIC server::compute)`.

## Documentation

Doxygen documentation is automatically created for the project. The CMake targets which build the docs include:

* main_build (does formatting, executable building, etc)
* doc (builds all docs)
* doc-saas-template-common-core (the name changes if you change the root CMake's PROJECT_NAME)

Documentation is stored under you're output folder in docs/code/common/core. We output dockbook
templates, HTML code, latex templates, and man pages.

## Project Example Usage

ADD EXAMPLES OF YOUR COMMON CODE HERE

## Why Stateless

This module is meant to be stateless. Meaning that it does not depend on any form of global state or memory. This could
be global variables, thread-local variables, external databases, external cache, file system, network, time clock, etc.

It is meant to take input and return output. Additionally, if given the same input parameters, the output should be the
same every time.

Statelessness provides lots of benefits, including ease of testing, re-use of business logic for multiple user
contexts and features, ease of change, and providing more options to better scale and grow an application.

For an example, let's write a function that should report whether we should charge a late fee on a loan. For simplicity,
we'll make the following assumptions:

* We won't worry about duplicate late fees (other code will handle that check)
* We want to charge if the borrower is behind on their payments (paid late, paid too little, etc)
* We'll give a grace period where we won't charge the borrower if they're behind
* We aren't going to worry about the amount of the late fee, just whether we should charge one

Let's also assume we aren't starting from scratch. We have a function that someone else wrote, and our job is to verify
whether it works as intended and to fix any issues we find with it. There are no tests for the method. Additionally,
the software product has no automated way to create test data, and all the manual controls match the customer experience
exactly (have to enter in new loans, loans only check late fee charges nightly, etc.).

We'll also assume any changes we make to the code (including function signature) are easy to do (not always a realistic
assumption, but we'll make it for this article).

Below is the code we're given:

```cpp
bool should_charge_late_fee_stateful(const LoanId& loanId) {
    auto loan = LoanDbRepository::query_by_id(loanId);
    const auto currentDate = Date::from(
        std::chrono::time_point{
            std::chrono::system_clock::now()
        }
    );
    
    return currentDate - PaymentService::query_by_id(loan.lastPaymentId).date > loan.gracePeriodInDays;
}
```

So, how do we verify this? Well, let's look at our options. We could create a loan manually, but that won't run the
above code until sometime in the night, and having to wait a full day to test isn't exactly fast (though it is needed
and we will do it later). However, if we could write some tests, we could start testing the method immediately.

If we look at the code, there is a lot of stateful calls being made. We're grabbing state from a loan database, a
payment
from a payment service, and we're grabbing the current time from the system clock. That means to test the function we
need to mock out the following:

* LoanDbRepository::query_by_id
* PaymentService::query_by_id
* std::chrono::system_clock::now

Great, so we know what we need to mock. Now let's look at the data structures we're dealing with. For simplicity, I'm
going to show the data with JSON, just imagine there was a class defined with a serializer/deserializer defined as well.

Here is the data for an example loan:

```json
{
  "id": "14938020394",
  "initialPrincipal": 1000.00,
  "paymentPeriodType": "MONTHLY",
  "firstPeriod": "2024-01-01",
  "paymentPeriods": 36,
  "interestRate": 0.06,
  "periodPayment": 30.42,
  "gracePeriodInDays": 5,
  "lateFeeAmount": 20.50,
  "lastPaymentId": "0"
}
```

This loan is for a $1000.00 loan with 36 monthly payments (3 years) starting on the 1st of the month every January.
There
is a 5 day grace period before a late fee is charged for missing a payment. And yes, I'm aware that most loans are way
more complex than this, but I'm keeping it simple.

Now let's look at the payment history. I'm going to omit how much goes towards principal, interest, fees, escrow, etc.

```json
[
  {
    "id": "94829",
    "date": "2024-01-01",
    "amount": "30.42"
  },
  {
    "id": "94848",
    "date": "2024-02-01",
    "amount": "20.42"
  },
  {
    "id": "94862",
    "date": "2024-03-01",
    "amount": "30.42"
  },
  {
    "id": "94896",
    "date": "2024-05-01",
    "amount": "30.42"
  }
]
```

Great! So we now understand what the data looks like. Let's start writing a test:

```cpp
// I'm using a hypothetical mocking library here
DEF_TEST("Negative Case") {
    auto testLoanId = LoanId{"1234"};
    auto testPaymentId = PaymentId("123");

    auto loanMock = Mocker::CreateMockObject<Loan>()
        ->create_instance()
        ->set_property<&Loan::gracePeriodInDays>(5)
        ->set_property<&Loan::getClosestDueDate>(Date::from("2020-01-01"));

    auto paymentMock = Mocker::CreateMockObject<Payment>()
        ->create_instance()
        ->set_property<&Payment::dateInDays>(Date::from("2020-01-02"));

    auto loanDbMock = Mocker::CreateMockObject<LoanDbRepository>()
        ->mock_static<&LoanDbRepository::query_by_id>()
        ->when_called_with(testLoanId)
        ->will_return(mockLoan);

    auto paymentDbMock = Mocker::CreateMockObject<PaymentService>()
        ->mockStatic<&PaymentService::query_by_id>()
        ->when_called_with(testPaymentId)
        ->will_return(paymentMock);

    auto chronoMock = Mocker::CreateMockFunction<std::chrono::system_clock:now>()
        ->when_called_with()
        ->will_return(Date::from("2020-01-03"));

    ASSERT_FALSE(should_charge_late_fee_stateful(testLoanId, testPaymentId));
}

DEF_TEST("Positive Case") {
    auto testLoanId = LoanId{"1234"};
    auto testPaymentId = PaymentId("123");

    auto loanMock = Mocker::CreateMockObject<Loan>()
        ->create_instance()
        ->set_property<&Loan::gracePeriodInDays>(5)
        ->set_property<&Loan::getClosestDueDate>(Date::from("2020-03-01"));

    auto paymentMock = Mocker::CreateMockObject<Payment>()
        ->create_instance() 
        ->set_property<&Payment::dateInDays>(Date::from("2020-01-01"));

    auto loanDbMock = Mocker::CreateMockObject<LoanDbRepository>()
        ->mock_static<&LoanDbRepository::query_by_id>()
        ->when_called_with(testLoanId)
        ->will_return(mockLoan);

    auto paymentDbMock = Mocker::CreateMockObject<PaymentService>()
        ->mockStatic<&PaymentService::query_by_id>()
        ->when_called_with(testPaymentId)
        ->will_return(paymentMock);

    // Have the date be past the due date (~3 months)
    auto chronoMock = Mocker::CreateMockFunction<std::chrono::system_clock:now>()
        ->when_called_with()
        ->will_return(Date::from("2020-06-01").to_chrono());

    ASSERT_TRUE(should_charge_late_fee_stateful(testLoanId, testPaymentId));
}
```

Great, so we've set up a test case. Now it's time to test this in the actual codebase. Which will take time. A lot of
time. Since we have a function on time, to fully test everything we need to run through multiple scenarios, and then
give the system enough time to uncover weird bugs. For now, let's assume there's a QA department who'll handle the
testing. We give it to them.

A few months later, we get back a list of bug reports. They have a table of how what the table evaluated to, as well as
a statement of what went wrong.

Here's the table of what our function evaluates to over the course of a few months:

| Days since<br/>1st of month | January | February | March | April | May   |
|-----------------------------|---------|----------|-------|-------|-------|
| 0                           | false   | false    | false | true  | false |
| 1                           | false   | false    | false | true  | false |
| 2                           | false   | false    | false | true  | false |
| 3                           | false   | false    | false | true  | false |
| 4                           | false   | false    | false | true  | false |
| 5                           | false   | false    | false | true  | false |
| 6                           | true    | true     | true  | true  | true  |

Here's the payment and loan data they were using too:

```json
{
  "id": "14938020394",
  "initialPrincipal": 1000.00,
  "paymentPeriodType": "MONTHLY",
  "firstPeriod": "2024-01-01",
  "paymentPeriods": 36,
  "interestRate": 0.06,
  "periodPayment": 30.42,
  "gracePeriodInDays": 5,
  "lateFeeAmount": 20.50,
  "lastPaymentId": "94896",
  "payments": [
    {
      "id": "94829",
      "date": "2024-01-01",
      "amount": "30.42"
    },
    {
      "id": "94848",
      "date": "2024-02-01",
      "amount": "20.42" // Didn't pay the full amounts, late fee should be charged
    },
    {
      "id": "94862",
      "date": "2024-03-01",
      "amount": "30.42"
    }, // No april payment, late fee should be charged.
    {
      "id": "94896",
      "date": "2024-05-01",
      "amount": "60.82" // Back to normal
    }
  ]
}
```

QA is reporting that the missed payment is charging correctly, but everything else is wrong. We're always charging after
the grace period, even when we shouldn't.
Let's take another look at the code:

```cpp
bool should_charge_late_fee_stateful(const LoanId& loanId) {
    auto loan = LoanDbRepository::query_by_id(loanId);
    const auto currentDate = Date::from(
        std::chrono::time_point{
            std::chrono::system_clock::now()
        }
    );
    
    return currentDate - PaymentService::query_by_id(loan.lastPaymentId).date > loan.gracePeriodInDays; // not using the due date
}
```

The fix would be to get the last due date and use that in our calculation. Something like the following maybe:

```cpp
bool should_charge_late_fee_stateful(const LoanId& loanId, const PaymentId& lastPaymentId) {
    auto loan = LoanDbRepository::query_by_id(loanId);
    auto lastPayment = PaymentService::query_by_id(lastPaymentId);
    const auto currentDate = Date::from(
        std::chrono::time_point{
            std::chrono::system_clock::now()
        }
    );
    
    const auto paymentDate = PaymentService::query_by_id(loan.lastPaymentId).date;
    const auto dueDate = loan.getClosestDueDate();
    const auto inGracePeriod = currentDate < dueDate + loan.gracePeriodDays;
    return !inGracePeriod && paymentDate < dueDate;
}
```

Great! Well, we got a report from QA, and it has a loan id, and it was the loan that the bug was reported with. So,
let's try using that loan to see if we can fix the issue. We'll load it up from the DB and change our system clock to
the days that QA used.

Let's run that code for January, February, March, April, and May.
Below would be the results we get

| Days since<br/>1st of month | January | February | March | April | May   |
|-----------------------------|---------|----------|-------|-------|-------|
| 0                           | false   | false    | false | false | false |
| 1                           | false   | false    | false | false | false |
| 2                           | false   | false    | false | false | false |
| 3                           | false   | false    | false | false | false |
| 4                           | false   | false    | false | false | false |
| 5                           | false   | false    | false | false | false |
| 6                           | false   | false    | false | false | false |

Hm. That isn't right. Now it's not charging a late fee, even though it really should. We even changed the date, so why
isn't it working?

Well, the answer is simple. Whenever a new payment was added to the system, it mutated the database state, which means
that the data we're using now is not the data that QA used - even though it's the "same loan" that we're using.

To show what I mean, here's the part that's causing the problem.

```cpp
PaymentService::query_by_id(loan.lastPaymentId)
```

We are querying the last payment id on the loan and using that for our comparison. When QA originally ran the tests,
they didn't have payments for future dates. However, when we're running the tests, we do have payments for "future" dates,
we always have payments through May 1st! That means that in April, we're calculating that we did pay, even though the
payment wouldn't come in until May.

Before we go start changing the loan data, let's look at another potential issue.

```cpp
loan.getClosestDueDate()
```

What does this section of code do? Does it search the list of due dates and compares them to our system time? Does it
run a SELECT statement on a database? Does it hit a microservice? Does it compare due dates to payment dates? An answer
of "it depends" isn't sufficient in this case. We had changed our local system clock to try to "go back in time", but
if it is using the system clock of another machine (say a database or microservice), then our attempt to "change state"
wouldn't impact this method at all.

It's quite possible that if we didn't start addressing the bug report until mid-June, our test would have returned the
following table instead:

| Days since<br/>1st of month | January | February | March | April | May  |
|-----------------------------|---------|----------|-------|-------|------|
| 0                           | true    | true     | true  | true  | true |
| 1                           | true    | true     | true  | true  | true |
| 2                           | true    | true     | true  | true  | true |
| 3                           | true    | true     | true  | true  | true |
| 4                           | true    | true     | true  | true  | true |
| 5                           | true    | true     | true  | true  | true |
| 6                           | true    | true     | true  | true  | true |

It all depends on if our test used a date where the account was had a recent payment (May) or a date where there hadn't
been a new payment logged in a while (June).

This highlights one of the major problems with stateful systems, our data can *age* out of relevance. This is also why
it's so important to mock time related functions in unit tests; if we don't "freeze" time, eventually our test data will
age and our tests will break (usually after the end of a month or a year). This aging effect isn't only restricted to
tests though. It happens in production systems too.

I worked at a company where we had lots of state related to A/B testing. However, the database that held this data
put an expiration date on most of this state. When a new year rolled around, a ton of data "aged out" of our database,
and it took down many different production systems that were using A/B testing. Similar issues can happen with security
certificates (they all have expiration dates), credentials (especially when password rotation is forced), and more.
Data sitting around in a stateful system can age out of relevance and cause breakages.

Back to our late fee example. We can't use QA's data anymore since it aged out, and we don't want to have to spend
multiple months testing that our code works, so how do we verify it? Well, we could write better tests. This time, we'll
want to mock out time changes to some of the boundaries we care about, such as before and after a grace period.

```cpp
DEF_TEST("Grace window") {
    // test that we get "false" during the grace period
        
    auto loanMock = Mocker::CreateMockObject<Loan>()
        ->create_instance()
        ->set_property<&Loan::gracePeriodInDays>(5)
        // make sure our due date is well past the payment date
        ->set_property<&Loan::getClosestDueDate>(Date::from("2020-03-01"));
    
    auto loanDbMock = Mocker::CreateMockObject<LoanDbRepository>()
            ->mock_static<&LoanDbRepository::query_by_id>()
            ->when_called_with(testLoanId)
            ->will_return(mockLoan);

    auto paymentMock = Mocker::CreateMockObject<Payment>()
            ->create_instance()
            ->set_property<&Payment::dateInDays>(Date::from("2020-01-01"));
    auto paymentDbMock = Mocker::CreateMockObject<PaymentService>()
            ->mockStatic<&PaymentService::query_by_id>()
            ->when_called_with(testPaymentId)
            ->will_return(paymentMock);
        
    for (size_t i = 0; i < 5; ++i) {
        // Test current date up to end of grace period
        auto chronoMock = Mocker::CreateMockFunction<std::chrono::system_clock:now>()
            ->when_called_with()
            ->will_return((Date::from("2020-03-01") + i).to_chrono());
    
        auto testLoanId = LoanId{"1234"};
        auto testPaymentId = PaymentId("123");
        
        ASSERT_FALSE(should_charge_late_fee_stateful(testLoanId, testPaymentId));
    }
    
    // Now test past the grace period
    auto chronoMock = Mocker::CreateMockFunction<std::chrono::system_clock:now>()
        ->when_called_with()
        ->will_return((Date::from("2020-03-01") + 6).to_chrono());

    auto testLoanId = LoanId{"1234"};
    auto testPaymentId = PaymentId("123");
    
    ASSERT_TRUE(should_charge_late_fee_stateful(testLoanId, testPaymentId));
}
```

There we go. No bugs right?

Well, QA doesn't quite agree. A few months later we receive a new table:

| Days since<br/>1st of month | June  | July   | August | September | October |
|-----------------------------|-------|--------|--------|-----------|---------|
| 0                           | false | false  | false  | true      | false   |
| 1                           | false | false  | false  | true      | false   |
| 2                           | false | false  | false  | true      | false   |
| 3                           | false | false  | false  | true      | false   |
| 4                           | false | false  | false  | true      | false   |
| 5                           | false | false  | false  | true      | false   |
| 6                           | false | false  | false  | true      | false   |

And here's the new test data (pretty close to the previous set, just with dates moved).


```json
{
  "id": "16321102332",
  "initialPrincipal": 1000.00,
  "paymentPeriodType": "MONTHLY",
  "firstPeriod": "2024-06-01",
  "paymentPeriods": 36,
  "interestRate": 0.06,
  "periodPayment": 30.42,
  "gracePeriodInDays": 5,
  "lateFeeAmount": 20.50,
  "lastPaymentId": "94896",
  "payments": [
    {
      "id": "94829",
      "date": "2024-06-01",
      "amount": "30.42"
    },
    {
      "id": "94848",
      "date": "2024-07-01",
      "amount": "20.42" // Didn't pay the full amounts, late fee should be charged
    },
    {
      "id": "94862",
      "date": "2024-08-01",
      "amount": "30.42"
    },
    // No september payment, late fee should be charged.
    {
      "id": "94896",
      "date": "2024-10-01",
      "amount": "60.82" // Back to normal
    }
  ]
}
```

The issue this time is July. Apparently, the outstanding balance hadn't been fully paid in July. So, now we need
to get the current balance, and the outstanding balance, and add them to our check. Also, when we talked to them about
basing it all off the payment and due dates, they just looked at us funny and said "What? it should all be based on the
balance and the grace period!".

Also, QA is getting tired of having to spend literal months making these tables, and management is asking questions
about why it's been almost a year and this one feature still isn't working, and the other developers are getting upset
because the bugs in this function are causing issues for their features, so they're now getting blocked and their
managers are starting to ask questions and point fingers. Oh, and the DBAs are also getting mad since our method is
called multiple times in the same request and we keep sending duplicate queries - though that doesn't matter since all
devs know they'll just add more read replicas anyway (this is both a joke and yet painfully true for a lot of software).

> Side note: It doesn't always get this bad, but it does suck a lot when testing processes are slowed down a ton because
new  testdata has to get generated every time. And it does get even worse when management does start asking questions
about why it takes so long to get something fixed or built or tested. And it also sucks if you do overload a database
instance with queries and take it down. They all suck, and they don't all happen because of coupling state with business
logic, but I do believe that the coupling of business logic and state does help contribute to these issues.

So, what's an alternative? What can we do to fix these state-related issues? Well, we can remove the state from our
business logic. Before we start "fixing" code though, we should at least convert what we have to be stateless.
Below is the stateless code, bugs and all:

```cpp
bool should_charge_late_fee_stateful(const Loan& loan, const Payment& lastPayment, const Date& currentDate) {
    const auto paymentDate = lastPayment.date;
    const auto dueDate = loan.getClosestDueDateTo(currentDate); // It's important we pass in the date
    const auto inGracePeriod = currentDate < dueDate + loan.gracePeriodDays;
    return !inGracePeriod && paymentDate < dueDate;
}
```

One of the things you'll notice, is that our code is no longer concerned with grabbing state. It doesn't know about the
database, or microservices, or even the system clock! That's fantastic!

Many devs will complain "where does the data come from then? If the code doesn't grab the data, how does it know
what the data is?" The data comes from the other code, the stateful part of the app (see the server::data module).

The business logic doesn't need to care about the source of the data. For all the business cares, the data could come
from a microservice, a database, a file upload, an event stream, a cache, or a form filled out by the user. Businesses,
users, and managers don't care about all the technical stuff of where data comes from, where it goes, or how it's
stored. They only care about if the end result is correct, and how many ways the same computation can be monetized.

Some devs will complain "well, how does the function know the data is correct?" It doesn't have to. That's the job of
the code which grabs the data. This method just processes what it's given. It doesn't care if the payment belongs to
the loan or not. It doesn't care if the date is in the current century or past millennium. It doesn't have to care. And
that's the beauty of it - because it doesn't have to care about where the data comes from or if it's "correct", we can
pass in whatever we want! Including any test data that we want!

So, we have now removed the state. But, how do we fix the actual bug? Well, we need to accept more state. Specifically,
we need to be able to calculate the outstanding balance. There are a few ways we could do that.

First, we could get all the payments and balances on a loan before the current date and then find the difference of their
sums. We could add the payments and charges to the loan object itself. That would give us something like the following:

```cpp
bool should_charge_late_fee_stateless(const Loan& loan, const Date& currentDate) {
    const auto dueDate = *std::find_first_of(
        // This iterates in reverse order, so "find_first_of" is really "find_last_of" 
        loan.charges.rbegin(),
        loan.charges.rend(),
        // We're checking grace period, so we want the last date less than or equal to the current date
        [currentDate](const auto& charge) {
            return charge.date <= currentDate;
        }
    );
    
    const auto inGracePeriod = currentDate < dueDate + loan.gracePeriodDays;
    if (inGracePeriod) {
        return false;
    }
    
    // See if we've fallen behind
    
    // We haven't missed a date, but we could be behind
    const totalChargeToDate = std::transform_reduce(
            loan.charges.begin(), loan.charges.end(),
            std::plus{},
            [&currentDate](const ChargeDate& charge) {
                // Filter out any charge dates before the current date
                if (charge.date <= currentDate) {
                    return charge.amount;
                }
                return 0;
            }
    );
    
    const totalPaidToDate = std::transform_reduce(
            loan.payments.begin(), loan.payments.end(),
            std::plus{},
            [](const Payment& payment) {
                // We could remove this if statement
                // Doing so would mean that future payments would apply to today
                // That generally doesn't happen except when doing testing on a loan that's been used for tests before
                // For this example we'll filter out payments instead of allowing time travel
                if (payment.date <= currentDate) {
                    return payment.amount;
                }
                return 0;
            }
    );
    
    return totalPaidToDate < totalChargeToDate;
}
```

We could then use this to test the QA loans. Code like the following would let us retest our first loan:

```cpp
const auto loan = LoanDbRepository::get_by_id({"14938020394"});
loan.fetch_payments();
loan.calculate_charges();

std::string date;
std::cout << "Enter a date to test for: \n";
std::in >> date;
std::cout << "Result: " << (should_charge_late_fee_stateless(loan, days_from_date(date)) ? "true" : "false") << "\n";
```

That's a lot of progress. We can now test what QA is seeing and compare against the expected results from QA. We can
now quickly tweak our code until we get the exact output that QA wants. However, that still doesn't solve the issue that
QA will still take months to verify our change, and management is still breathing down our backs. So, what could we do
to help QA?

Well, one thing we could do is to make this tool available for QA in some form. They could load up any loan and "scrub"
through the history of the loan to see what the function outputs. We could also replicate production data and test with
the replicated data.

We can also write unit tests a lot easier. Let's take all the test cases we've written and fix them up, and let's also
add in the tests for the new cases we know about.

```cpp
auto create_test_loan() -> Loan;

DEF_TEST("stateless") {
    auto loan = create_test_loan();
    
    SUBCASE("Everything on time and in full - no late fees") {
      // Test our first period, no charges should be made the entire first period
      auto jan1 = Date::from("2024-01-01");
      auto jan31 = Date::from("2024-01-31");
      for (size_t date = jan1; date <= jan31; ++date) {
          ASSERT_FALSE(should_charge_late_fee_stateless(loan, date));
      }
    }
    
    // Test our second period
    SUBCASE("On time but not in full - yes late fees") {
      // no charges should be made until the grace period expires
      auto feb1 = Date::from("2024-02-01");
      auto endOfGrace = feb1 + loan.gracePeriodDays;
      for (size_t date = feb1; date <= endOfGrace; ++date) {
          ASSERT_FALSE(should_charge_late_fee_stateless(loan, date));
      }
      
      // We should then charge from the end of the grace period to when the balance is restored
      auto paymentsRestored = loan.payments[2].date;
      for (size_t date = endOfGrace; date < paymentsRestored; ++date) {
          ASSERT_TRUE(should_charge_late_fee_stateless(loan, date));
      }
      
      // We shouldn't charge for the rest of February
      // using march1 as the exclusive end so we don't worry about leap years
      auto mar1 = Date::from("2024-03-01");
      for (size_t date = paymentsRestored; date < mar1; ++date) {
          ASSERT_FALSE(should_charge_late_fee_stateless(loan, date));
      }
    }
    
    SUBCASE("No payment made") {
      // Test our thrid period, no charges should be made the entire third period
      auto apr1 = Date::from("2024-04-01");
      auto apr30 = Date::from("2024-04-30");
      auto endOfGrace = apr1 + loan.gracePeriodDays;
      for (size_t date = apr1; date <= endOfGrace; ++date) {
          ASSERT_FALSE(should_charge_late_fee_stateless(loan, date));
      }
      for (size_t date = endOfGrace; date <= apr30; ++date) {
          ASSERT_TRUE(should_charge_late_fee_stateless(loan, date));
      }
    }
}

auto create_test_loan() -> Loan {
    auto loan = Loan{
      .id = LoanId{"14938020394"},
      .initialPrincipal = 1000.00,
      .paymentPeriodType = PAYMENT_PERIOD_TYPE::MONTHLY,
      .firstPeriod = Date::from("2024-01-01"),
      .paymentPeriods = 36,
      .interestRate = 0.06,
      .periodPayment = 30.42,
      .gracePeriodInDays = 5,
      .lateFeeAmount = 20.50,
      .payments = {
        {
          .id = PaymentId{"94829"},
          .date = Date::from("2024-01-01"),
          .amount = 30.42
        },
        {
          .id = PaymentId{"94848"},
          .date = Date::from("2024-02-01"),
          .amount = 20.42 // Didn't pay the full amounts, late fee should be charged
        },
        {
          .id = PaymentId{"94848"},
          .date = Date::from("2024-02-20"),
          .amount = 10.00 // Finished paying the full amount (resets for next period)
        },
        {
          .id = PaymentId{"94862"},
          .date = Date::from("2024-03-01"),
          .amount = 30.42
        },
        // No april payment, late fee should be charged.
        {
          .id = PaymentId{"94896"},
          .date = Date::from("2024-05-01"),
          .amount = 60.84 // Account comes back to current
        }
      }
    };
    
    // We'll calculate the charge dates out so that we don't have to write them by hand
    // If we wanted to test the charge date calculations, we'd do that in another set of tests
    loan.calculate_charge_dates();
    return loan;
}
```

These tests are very easy to read and understand for the most part. However, we still have an issue. We wrote our code
to require a full loan object. Additionally, it does actually matter what the loan object looks like since it is the
primary item we're testing with. It also means that we can only ever use our code for loans. We can't use it for loan
applications, credit cards, revolving debt, buy now pay later, etcetera since we've tied the logic to a loan. Let's take
another look at the code and see if we really do need to tie it all to a loan, or if we can remove that coupling.

```cpp
bool should_charge_late_fee_stateless(const Loan& loan, const std::chrono::days& currentDate) {
    // We want a due date
    const auto dueDate = *std::find_first_of( 
        loan.charges.rbegin(),
        loan.charges.rend(),
        [currentDate](const auto& charge) {
            return charge.date <= currentDate;
        }
    );

    // We want a grace period length
    const auto inGracePeriod = currentDate < dueDate + loan.gracePeriodDays;
    if (inGracePeriod) {
        return false;
    }
    
    // We want the total amount charged
    const totalChargeToDate = std::transform_reduce(
            loan.charges.begin(), loan.charges.end(),
            std::plus{},
            [&currentDate](const ChargeDate& charge) {
                // Filter out any charge dates before the current date
                if (charge.date <= currentDate) {
                    return charge.amount;
                }
                return 0;
            }
    );
    
    // We want the total amount paid
    const totalPaidToDate = std::transform_reduce(
            loan.payments.begin(), loan.payments.end(),
            std::plus{},
            [](const Payment& payment) {
                // We could remove this if statement
                // Doing so would mean that future payments would apply to today
                // That generally doesn't happen except when doing testing on a loan that's been used for tests before
                // For this example we'll filter out payments instead of allowing time travel
                if (payment.date <= currentDate) {
                    return payment.amount;
                }
                return 0;
            }
    );
    
    return totalPaidToDate < totalChargeToDate;
}
```

If we look at the code, we only really depend on the following items:
* The current date
* The previous due date
* The grace period
* The total amount charged
* The total amount paid

Let's create a more simplified version where we pass in those values instead of calculate them. Something like the
following should work:

```cpp
bool should_charge_late_fee_explicit_args(
        const Date& currentDate,
        const Date& previousDate,
        const Date& gracePeriod,
        double amountCharged,
        double amountPaid
) {
    // We want a grace period length
    const auto inGracePeriod = currentDate < dueDate + loan.gracePeriodDays;
    return !inGracePeriod && totalPaidToDate < totalChargeToDate;
}
```

We now have a completely isolated method which doesn't even depend on loans. Some might say "there are too many
parameters", except we have far fewer parameters we're passing in. When we were passing in a loan, we were passing in
every field on that loan as a parameter. Generally, those fields were unused, but whenever we called the method we
still had to provide them. Our tests made this dependency pretty obvious since we either had to use a test database and
fetch the fields, mock the fields we didn't use, or provide fake data for the fields we didn't use. However, now we only
have to provide the fields which we do use.

Now that we can see the fields which we do actually use, we can see if there are any fields that could be combined. For
instance, we have the fields amountCharged and amountPaid, but we could combine them into a balance (`amountCharged - amountPaid`).
Additionally, we have a separate due date and grace period, but we could combine them into a "past due date" or "fee
charge date" (`dueDate + gracePeriodDays`). These two combinations do help simplify the code more, and they reduce the
number of parameters we use.

```cpp
bool should_charge_late_fee_simplified(const Date& feeChargeDate, double balance, const Date& currentDate) {
    // We want a grace period length
    return currentDate < feeChargeDate && balance > 0;
}
```

With this new method, we can redefine our tests:

```cpp
DEF_TEST("simplified stateless") {
  const auto gracePeriodDays = 5;
  auto feb1 = Date::from("2024-02-01");
  auto mar1 = Date::from("2024-03-01");
  
  // no charges should be made until the grace period expires
  auto endOfGrace = feb1 + loan.gracePeriodDays;
  for (size_t date = feb1; date <= endOfGrace; ++date) {
      // during grace period, it shouldn't matter if we're past due or not
      ASSERT_FALSE(should_charge_late_fee_simplified(endOfGrace, 10, date));
      ASSERT_FALSE(should_charge_late_fee_simplified(endOfGrace, 0, date));
  }
    
  // past grace period
  for (size_t date = endOfGrace; date < mar1; ++date) {
      // We should only charge when there is an outstanding balance
      ASSERT_TRUE(should_charge_late_fee_simplified(endOfGrace, 10, date));
      ASSERT_FALSE(should_charge_late_fee_simplified(endOfGrace, 0, date));
  }
}
```

If you look closely, you'll notice that we have fewer tests, yet we still cover all the branches in our code. When we
were relying on the full loan object, we had to calculate a lot of our compound values in addition to performing our business
logic. This meant that we had to test how we calculated our values in the same test suite as our business logic.

We haven't removed the need to calculate compound values. However, we have decoupled that calculation from our business
logic. Somewhere else in the code we will still have that calculation, and we still need to make sure it's correct. But,
we can do that separately. For instance, we could add "getLateFeeChargeDate" and "getBalance" methods to our Loan object.

The other added benefit is that we've decoupled ourselves from our data model. We can now use this code with an SQL
database, NoSQL database (e.g. Mongo, Cassandra), in-memory cache (e.g. Memcached, Redis), event system (e.g. Kafka, RabbitMQ),
uploaded files (e.g. CSV), client storage (e.g. LocalStorage), or even no datastore at all (e.g. API or a web form).
This allows us to reuse our code to solve multiple business domain issues.

For instance, on one project I worked on we managed contracts that were finalized. Once a contract was finalized, it rarely changed
so our business logic only needed to be run infrequently. This meant we could get away with excessive amounts of
database calls in our business logic since it rarely mattered if we ran an extra 10 database calls per client per week.

At one point, we tried to venture into the contract draft process. During contract drafting, many different drafts
were made which needed to go through a subset of our existing business logic (e.g. logic which checks the numbers in the
contract are correct). These drafts also were generally short-lived and changed frequently, and each change required an
additional rerun through our business logic. At this point, we went from an extra 10 calls per client per week to an extra 10 calls
per client per minute. The huge jump in load overwhelmed our database and caused issues across the product. We did get
things resolved (mostly by indexing things that should have been indexed).

But, there were still a lot of issues left over. For one, draft contracts and finalized contracts were stored in the same
database table, even though they were treated very differently from our system. Additionally, the table they were stored
in was already huge with just the finalized contracts, so adding in a bunch of temporary contracts grew the table tremendously.
Furthermore, draft contracts were often never revisited after 24-hours, but they were created much faster than the finalized
contracts. Our solution to keep the table small was to have a separate process which scanned the table (yes, scan, the
devs who made it wanted the "when to delete" logic "in code") and deleted any draft contract over a few days. This
caused a massive amount of traffic as well as table and row locks, which slowed down quite a few other processes. Fortunately,
they scheduled this to happen in the middle of night, so it only impacted our longest running and most critical nightly
maintenance processes. Customers only saw impact if our nightly maintenance processes didn't finish before they logged in,
which did start to happen but only for really early morning people.

Of course, anyone reading the above paragraph might be wondering why we didn't use Redis with a TTL and persistence mode
turned on. It would have been perfect for our use case. The answer is that our business logic was tied to an SQL table,
and that decoupling it wasn't something on our roadmap (especially since the devs who wrote the core logic no longer worked
there, and no other devs knew what was going on or how to verify changes). Ideally, the core business logic would not have been
reliant on the database, that way future devs could adapt it to any data storage (or lack thereof) that was needed.

