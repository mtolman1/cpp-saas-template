#pragma once

#include <common/core.h>
#include <map>
#include <string>
#include <vector>

namespace server::compute {
  enum class US_FEDERAL_RESERVE_HOLIDAYS {
    NEW_YEARS_DAY,
    MARTIN_LUTHER_KING_JR_DAY,
    PRESIDENTS_DAY,
    MEMORIAL_DAY,
    JUNETEENTH,
    INDEPENDENCE_DAY,
    LABOR_DAY,
    COLUMBUS_DAY,
    VETERANS_DAY,
    THANKSGIVING_DAY,
    CHRISTMAS_DAY
  };

  enum class US_HOLIDAY_OBSERVATION {
    ON_DATE,
    NEAREST_WEEKDAY,
    PRECEDING_FRIDAY,
    FOLLOWING_MONDAY
  };

  auto us_federal_reserve_holidays(int64_t year) -> std::map<US_FEDERAL_RESERVE_HOLIDAYS, calendars::gregorian::Date>;
  auto us_holidays_adjust_to_observed(std::vector<calendars::gregorian::Date> holidays, US_HOLIDAY_OBSERVATION observations)
      -> std::vector<calendars::gregorian::Date>;
  auto us_holiday_adjust_to_observed(const calendars::gregorian::Date& holidays, US_HOLIDAY_OBSERVATION observations)
    -> calendars::gregorian::Date;
  auto us_federal_reserve_holidays_with_observation(int64_t year, US_HOLIDAY_OBSERVATION observations)
      -> std::map<US_FEDERAL_RESERVE_HOLIDAYS, calendars::gregorian::Date>;

}  // namespace server::compute
