#include "compute/holidays.h"

auto server::compute::us_federal_reserve_holidays(int64_t year) -> std::map<US_FEDERAL_RESERVE_HOLIDAYS, calendars::gregorian::Date> {
  using namespace calendars::holidays::usa;
  using namespace calendars::holidays::christian;
  return {
      {US_FEDERAL_RESERVE_HOLIDAYS::NEW_YEARS_DAY, new_years(year)},
      {US_FEDERAL_RESERVE_HOLIDAYS::MARTIN_LUTHER_KING_JR_DAY, martin_luther_king_jr_day(year)},
      {US_FEDERAL_RESERVE_HOLIDAYS::PRESIDENTS_DAY, presidents_day(year)},
      {US_FEDERAL_RESERVE_HOLIDAYS::MEMORIAL_DAY, memorial_day(year)},
      {US_FEDERAL_RESERVE_HOLIDAYS::JUNETEENTH, juneteenth(year)},
      {US_FEDERAL_RESERVE_HOLIDAYS::INDEPENDENCE_DAY, independence_day(year)},
      {US_FEDERAL_RESERVE_HOLIDAYS::LABOR_DAY, labor_day(year)},
      {US_FEDERAL_RESERVE_HOLIDAYS::COLUMBUS_DAY, columbus_day(year)},
      {US_FEDERAL_RESERVE_HOLIDAYS::VETERANS_DAY, veterans_day(year)},
      {US_FEDERAL_RESERVE_HOLIDAYS::THANKSGIVING_DAY, thanksgiving(year)},
      {US_FEDERAL_RESERVE_HOLIDAYS::CHRISTMAS_DAY, christmas(year)},
  };
}

static auto adjust_holidays_to_following_monday(std::map<server::compute::US_FEDERAL_RESERVE_HOLIDAYS, calendars::gregorian::Date> holidays)
    -> std::map<server::compute::US_FEDERAL_RESERVE_HOLIDAYS, calendars::gregorian::Date>;
static auto adjust_holidays_to_preceding_friday(std::map<server::compute::US_FEDERAL_RESERVE_HOLIDAYS, calendars::gregorian::Date> holidays)
    -> std::map<server::compute::US_FEDERAL_RESERVE_HOLIDAYS, calendars::gregorian::Date>;
static auto adjust_holidays_to_nearest_weekday(std::map<server::compute::US_FEDERAL_RESERVE_HOLIDAYS, calendars::gregorian::Date> holidays)
    -> std::map<server::compute::US_FEDERAL_RESERVE_HOLIDAYS, calendars::gregorian::Date>;
static auto adjust_holidays_to_following_monday(std::vector<calendars::gregorian::Date> holidays) -> std::vector<calendars::gregorian::Date>;
static auto adjust_holidays_to_preceding_friday(std::vector<calendars::gregorian::Date> holidays) -> std::vector<calendars::gregorian::Date>;
static auto adjust_holidays_to_nearest_weekday(std::vector<calendars::gregorian::Date> holidays) -> std::vector<calendars::gregorian::Date>;
static auto adjust_holiday_to_following_monday(const calendars::gregorian::Date& holiday) -> calendars::gregorian::Date;
static auto adjust_holiday_to_preceding_friday(const calendars::gregorian::Date& holiday) -> calendars::gregorian::Date;
static auto adjust_holiday_to_nearest_weekday(const calendars::gregorian::Date& holiday) -> calendars::gregorian::Date;

auto server::compute::us_federal_reserve_holidays_with_observation(int64_t year, server::compute::US_HOLIDAY_OBSERVATION observations)
    -> std::map<US_FEDERAL_RESERVE_HOLIDAYS, calendars::gregorian::Date> {
  auto holidays = us_federal_reserve_holidays(year);
  switch (observations) {
    case US_HOLIDAY_OBSERVATION::FOLLOWING_MONDAY:
      return adjust_holidays_to_following_monday(holidays);
    case US_HOLIDAY_OBSERVATION::NEAREST_WEEKDAY:
      return adjust_holidays_to_nearest_weekday(holidays);
    case US_HOLIDAY_OBSERVATION::PRECEDING_FRIDAY:
      return adjust_holidays_to_preceding_friday(holidays);
    case US_HOLIDAY_OBSERVATION::ON_DATE:
    default:
      return holidays;
  }
}

auto server::compute::us_holiday_adjust_to_observed(const calendars::gregorian::Date& d, server::compute::US_HOLIDAY_OBSERVATION observations) -> calendars::gregorian::Date {
    switch (observations) {
        case US_HOLIDAY_OBSERVATION::NEAREST_WEEKDAY:
            return adjust_holiday_to_nearest_weekday(d);
        case US_HOLIDAY_OBSERVATION::PRECEDING_FRIDAY:
            return adjust_holiday_to_preceding_friday(d);
        case US_HOLIDAY_OBSERVATION::FOLLOWING_MONDAY:
            return adjust_holiday_to_following_monday(d);
        case US_HOLIDAY_OBSERVATION::ON_DATE:
        default:
            return d;
    }
}

auto server::compute::us_holidays_adjust_to_observed(std::vector<calendars::gregorian::Date> holidays, server::compute::US_HOLIDAY_OBSERVATION observations)
    -> std::vector<calendars::gregorian::Date> {
  switch (observations) {
    case US_HOLIDAY_OBSERVATION::FOLLOWING_MONDAY:
      return adjust_holidays_to_following_monday(holidays);
    case US_HOLIDAY_OBSERVATION::NEAREST_WEEKDAY:
      return adjust_holidays_to_nearest_weekday(holidays);
    case US_HOLIDAY_OBSERVATION::PRECEDING_FRIDAY:
      return adjust_holidays_to_preceding_friday(holidays);
    case US_HOLIDAY_OBSERVATION::ON_DATE:
    default:
      return holidays;
  }
}

auto adjust_holidays_to_following_monday(std::vector<calendars::gregorian::Date> holidays) -> std::vector<calendars::gregorian::Date> {
  for (auto& holiday : holidays) {
    holiday = adjust_holiday_to_following_monday(holiday);
  }
  return holidays;
}

auto adjust_holidays_to_preceding_friday(std::vector<calendars::gregorian::Date> holidays) -> std::vector<calendars::gregorian::Date> {
  for (auto& holiday : holidays) {
    holiday = adjust_holiday_to_preceding_friday(holiday);
  }
  return holidays;
}

auto adjust_holidays_to_nearest_weekday(std::vector<calendars::gregorian::Date> holidays) -> std::vector<calendars::gregorian::Date> {
  for (auto& holiday : holidays) {
    holiday = adjust_holiday_to_nearest_weekday(holiday);
  }
  return holidays;
}

auto adjust_holiday_to_following_monday(const calendars::gregorian::Date& holiday) -> calendars::gregorian::Date {
  auto rdDate    = holiday.to_rd_date();
  auto dayOfWeek = rdDate.day_of_week();
  if (dayOfWeek == calendars::DAY_OF_WEEK::SUNDAY || dayOfWeek == calendars::DAY_OF_WEEK::SATURDAY) {
      return rdDate.day_of_week_on_or_after(calendars::DAY_OF_WEEK::MONDAY).to<calendars::gregorian::Date>();
  }
  return holiday;
}

auto adjust_holiday_to_preceding_friday(const calendars::gregorian::Date& holiday) -> calendars::gregorian::Date {
  auto rdDate    = holiday.to_rd_date();
  auto dayOfWeek = rdDate.day_of_week();
  if (dayOfWeek == calendars::DAY_OF_WEEK::SUNDAY || dayOfWeek == calendars::DAY_OF_WEEK::SATURDAY) {
      return rdDate.day_of_week_on_or_before(calendars::DAY_OF_WEEK::FRIDAY).to<calendars::gregorian::Date>();
  }
  return holiday;
}

auto adjust_holiday_to_nearest_weekday(const calendars::gregorian::Date& holiday) -> calendars::gregorian::Date {
  auto rdDate    = holiday.to_rd_date();
  auto dayOfWeek = rdDate.day_of_week();
  if (dayOfWeek == calendars::DAY_OF_WEEK::SUNDAY) {
      return rdDate.day_of_week_on_or_after(calendars::DAY_OF_WEEK::MONDAY).to<calendars::gregorian::Date>();
  }
  else if (dayOfWeek == calendars::DAY_OF_WEEK::SATURDAY) {
      return rdDate.day_of_week_on_or_before(calendars::DAY_OF_WEEK::FRIDAY).to<calendars::gregorian::Date>();
  }
  return holiday;
}

auto adjust_holidays_to_following_monday(std::map<server::compute::US_FEDERAL_RESERVE_HOLIDAYS, calendars::gregorian::Date> holidays)
    -> std::map<server::compute::US_FEDERAL_RESERVE_HOLIDAYS, calendars::gregorian::Date> {
  for (auto& holiday : holidays) {
    holiday.second = adjust_holiday_to_following_monday(holiday.second);
  }
  return holidays;
}

auto adjust_holidays_to_preceding_friday(std::map<server::compute::US_FEDERAL_RESERVE_HOLIDAYS, calendars::gregorian::Date> holidays)
    -> std::map<server::compute::US_FEDERAL_RESERVE_HOLIDAYS, calendars::gregorian::Date> {
  for (auto& holiday : holidays) {
    holiday.second = adjust_holiday_to_preceding_friday(holiday.second);
  }
  return holidays;
}

auto adjust_holidays_to_nearest_weekday(std::map<server::compute::US_FEDERAL_RESERVE_HOLIDAYS, calendars::gregorian::Date> holidays)
    -> std::map<server::compute::US_FEDERAL_RESERVE_HOLIDAYS, calendars::gregorian::Date> {
  for (auto& holiday : holidays) {
    holiday.second = adjust_holiday_to_nearest_weekday(holiday.second);
  }
  return holidays;
}
