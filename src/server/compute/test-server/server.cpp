#pragma clang diagnostic push
#pragma ide diagnostic ignored "cert-err58-cpp"
#include <common/core.h>

#define DEFINE_SERVER_MAIN
#define APP_INFO "Compute Server - Dev Build"
#include "server/unsafe/test-server.h"

#include <iostream>

static constexpr auto date_two_digit_str(int num) -> const char * {
  switch (num) {
    case 0:
      return "00";
    case 1:
      return "01";
    case 2:
      return "02";
    case 3:
      return "03";
    case 4:
      return "04";
    case 5:
      return "05";
    case 6:
      return "06";
    case 7:
      return "07";
    case 8:
      return "08";
    case 9:
      return "09";
    case 10:
      return "10";
    case 11:
      return "11";
    case 12:
      return "12";
    case 13:
      return "13";
    case 14:
      return "14";
    case 15:
      return "15";
    case 16:
      return "16";
    case 17:
      return "17";
    case 18:
      return "18";
    case 19:
      return "19";
    case 20:
      return "20";
    case 21:
      return "21";
    case 22:
      return "22";
    case 23:
      return "23";
    case 24:
      return "24";
    case 25:
      return "25";
    case 26:
      return "26";
    case 27:
      return "27";
    case 28:
      return "28";
    case 29:
      return "29";
    case 30:
      return "30";
    case 31:
      return "31";
    default:
      return "??";
  }
}

/**
 * GET /holidays/us/independenceDay/{year}
 * @summary US Independence Day
 * @description Returns the US independence day for a given year
 * @response 200 - Date of US Independence Day
 * @response 400 - Invalid request
 * @pathParam {int64} year - Gregorian year
 * @responseContent {string} 200.text/plain
 * @responseContent {string} 400.text/plain
 * @responseExample {YmdDate} 200.text/plain.YmdDate
 */
DEF_ROUTE("/holidays/us/independenceDay/:year", HTTP_GET)
([](const Req &req, Res &res) {
  std::stringstream ss{};
  auto              yearStr = req.path_params.at("year");
  int64_t           year;
  try {
    year = std::stoll(yearStr, nullptr, 10);
  }
  catch (...) {
    invalid_request(res, "year", "not_an_integer");
    res.status = 400;
    return;
  }

  auto date = calendars::holidays::usa::independence_day(year);
  ss << date.year() << "-" << date_two_digit_str(static_cast<int>(date.month())) << "-" << date_two_digit_str(date.day());

  res.set_content(ss.str(), mimeText);
});

/**
 * POST /holidays/us/veteransDay
 * @summary US Veterans Day
 * @response 200 - Date of US Veterans Day
 * @response 400 - Invalid Request
 * @description Returns the US veterans day for a given year
 * @bodyRequired
 * @bodyComponent {YearBody}
 * @responseContent {GregorianDate} 200.application/json
 * @responseContent {InvalidRequest} 400.application/json
 * @responseExample {GregorianDateResponse} 200.application/json.GregorianDateResponse
 */
DEF_ROUTE("/holidays/us/veteransDay", HTTP_POST)
([](const Req &req, Res &res) {
  nlohmann::json input;
  try {
    input = nlohmann::json::parse(req.body);
  }
  catch (...) {
    return invalid_request(res, "*", "invalid_json");
  }
  nlohmann::json val{};

  if (!input.contains("year")) {
    return invalid_request(res, "year", "missing");
  }
  else if (!input.at("year").is_number_integer()) {
    return invalid_request(res, "year", "invalid_integer");
  }

  auto veteransDay = calendars::holidays::usa::veterans_day(input.at("year"));

  res.set_content(nlohmann::to_string(nlohmann::json{{"year", static_cast<int>(veteransDay.year())},
                                                     {"month", static_cast<int>(veteransDay.month())},
                                                     {"day", static_cast<int>(veteransDay.day())},
                                                     {"calendar", "GREGORIAN"}}),
                  mimeJson);
});

#pragma clang diagnostic pop