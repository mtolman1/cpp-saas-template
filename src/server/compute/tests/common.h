#pragma once

#include <common/testing.h>
#include "compute/holidays.h"
#include <limits>

namespace rc {

    template<>
    struct Arbitrary<calendars::gregorian::Date> {
        static constexpr auto bounds = std::numeric_limits<int64_t>::max() >> 21; // NOLINT(hicpp-signed-bitwise)
        static Gen<calendars::gregorian::Date> arbitrary() {
            return gen::map(gen::construct<calendars::gregorian::Date>(
                    gen::construct<int64_t>(gen::inRange(-bounds, bounds)),
                    gen::construct<calendars::gregorian::MONTH>(gen::inRange(1, 12)),
                    gen::construct<int16_t>(gen::inRange(static_cast<int16_t>(0), static_cast<int16_t>(31)))
            ), [](const calendars::gregorian::Date& d) {
                return d.nearest_valid();
            });
        }
    };

} // namespace rc

