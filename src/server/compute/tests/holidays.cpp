#include "common.h"
#include <vector>

static auto is_weekday(const calendars::gregorian::Date& d) -> bool {
    auto dayOfWeek = d.to_rd_date().day_of_week();
    return dayOfWeek >= calendars::DAY_OF_WEEK::MONDAY && dayOfWeek <= calendars::DAY_OF_WEEK::FRIDAY;
}
#define CHECK_CAT_IMPL(s1, s2) s1##s2
#define CHECK_CAT(s1, s2)      CHECK_CAT_IMPL(s1, s2)
#ifdef __COUNTER__  // not standard and may be missing for some compilers
#define CHECK_ANONYMOUS(x) CHECK_CAT(x, __COUNTER__)
#else  // __COUNTER__
#define CHECK_ANONYMOUS(x) CHECK_CAT(x, __LINE__)
#endif  // __COUNTER__

#define NEAREST_CHECK(INPUT, RESULT) { \
    PROP_ASSERT(is_weekday(RESULT)); \
    PROP_ASSERT_LE(std::abs((INPUT).to_rd_date().day_difference((RESULT).to_rd_date())), 1); \
\
    if (is_weekday(INPUT)) { \
        PROP_ASSERT_EQ((RESULT), (INPUT)); \
    } \
}

#define PRECEEDING_CHECK(INPUT, RESULT) { \
    PROP_ASSERT(is_weekday(RESULT)); \
    PROP_ASSERT_LE(std::abs((INPUT).to_rd_date().day_difference((RESULT).to_rd_date())), 2); \
\
    if (is_weekday(INPUT)) { \
        PROP_ASSERT_EQ((RESULT), (INPUT)); \
    } else {                      \
        PROP_ASSERT_EQ(calendars::DAY_OF_WEEK::FRIDAY, (RESULT).to_rd_date().day_of_week()); \
    } \
}

#define FOLLOWING_CHECK(INPUT, RESULT) { \
    PROP_ASSERT(is_weekday(RESULT)); \
    PROP_ASSERT_LE(std::abs((INPUT).to_rd_date().day_difference((RESULT).to_rd_date())), 2); \
\
    if (is_weekday(INPUT)) { \
        PROP_ASSERT_EQ((RESULT), (INPUT)); \
    } else {                      \
        PROP_ASSERT_EQ(calendars::DAY_OF_WEEK::MONDAY, (RESULT).to_rd_date().day_of_week()); \
    } \
}

PROP_SUITE("Holiday Adjustments") {
    using namespace server::compute;

    PROP_TEST("Nearest Weekday") (const calendars::gregorian::Date& inputDate){
        auto res = us_holiday_adjust_to_observed(inputDate, US_HOLIDAY_OBSERVATION::NEAREST_WEEKDAY);
        NEAREST_CHECK(inputDate, res);
    };

    PROP_TEST("Nearest Weekdays") (const std::vector<calendars::gregorian::Date>& inputDates){
        const auto resDates = us_holidays_adjust_to_observed(inputDates, server::compute::US_HOLIDAY_OBSERVATION::NEAREST_WEEKDAY);
        PROP_ASSERT_EQ(resDates.size(), inputDates.size());
        for (auto inputIt = inputDates.begin(), resIt = resDates.end(); inputIt != inputDates.end() && resIt != resDates.end(); ++inputIt, ++resIt)
            NEAREST_CHECK(*inputIt, *resIt)
    };

    PROP_TEST("Preceding Friday") (const calendars::gregorian::Date& inputDate){
        auto res = us_holiday_adjust_to_observed(inputDate, US_HOLIDAY_OBSERVATION::PRECEDING_FRIDAY);
        PRECEEDING_CHECK(inputDate, res);
    };

    PROP_TEST("Preceding Fridays") (const std::vector<calendars::gregorian::Date>& inputDates) {
        const auto resDates = us_holidays_adjust_to_observed(inputDates, server::compute::US_HOLIDAY_OBSERVATION::PRECEDING_FRIDAY);
        PROP_ASSERT_EQ(resDates.size(), inputDates.size());
        for (auto inputIt = inputDates.begin(), resIt = resDates.end(); inputIt != inputDates.end() && resIt != resDates.end(); ++inputIt, ++resIt)
            PRECEEDING_CHECK(*inputIt, *resIt)
    };

    PROP_TEST("Following Monday") (const calendars::gregorian::Date& inputDate){
        auto res = us_holiday_adjust_to_observed(inputDate, US_HOLIDAY_OBSERVATION::FOLLOWING_MONDAY);
        FOLLOWING_CHECK(inputDate, res);
    };

    PROP_TEST("Following Mondays") (const std::vector<calendars::gregorian::Date>& inputDates){
        const auto resDates = us_holidays_adjust_to_observed(inputDates, server::compute::US_HOLIDAY_OBSERVATION::FOLLOWING_MONDAY);
        PROP_ASSERT_EQ(resDates.size(), inputDates.size());
        for (auto inputIt = inputDates.begin(), resIt = resDates.end(); inputIt != inputDates.end() && resIt != resDates.end(); ++inputIt, ++resIt)
            NEAREST_CHECK(*inputIt, *resIt)
    };

    PROP_TEST("On Date") (const calendars::gregorian::Date& inputDate){
        auto res = us_holiday_adjust_to_observed(inputDate, US_HOLIDAY_OBSERVATION::ON_DATE);
        PROP_ASSERT_EQ(res, inputDate);
    };


    PROP_TEST("On Dates") (const std::vector<calendars::gregorian::Date>& inputDates){
        const auto resDates = us_holidays_adjust_to_observed(inputDates, server::compute::US_HOLIDAY_OBSERVATION::ON_DATE);
        PROP_ASSERT_EQ(resDates.size(), inputDates.size());
        for (auto inputIt = inputDates.begin(), resIt = resDates.end(); inputIt != inputDates.end() && resIt != resDates.end(); ++inputIt, ++resIt)
            PROP_ASSERT_EQ(*inputIt, *resIt);
    };
}
