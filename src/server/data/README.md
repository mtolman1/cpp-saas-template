# server::data Module

This module is for server-side data fetching and saving. It manages communicating with stateful
systems (e.g. a database).

This library is currently setup for Postgres databases.

## Current Dependencies

### Liquibase

https://www.liquibase.org/

Used for SQL schema migration. A PRO license is **NOT** needed! The community edition is supported
with the base template.

If you do not wish to install liquibase locally or to have liquibase be ran by CMake, set the CMake
variable `EXCLUDE_LIQUIBASE_TESTS` to `ON`.

### server::common

The server::common module.

### libpq

https://www.postgresql.org/docs/current/static/libpq.html

The Postgres C driver

### PGFE

https://github.com/dmitigr/pgfe

A C++ wrapper for libpq

## Project Example Usage

ADD EXAMPLES OF YOUR COMMON CODE HERE
