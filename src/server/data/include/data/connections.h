#pragma once

#include <string>
#include <optional>

namespace server::data::connections {
    class DB {
    public:
        DB(const std::string& host, int port, std::string user, std::string password, std::optional<int> poolSize);

    private:

    };
}
