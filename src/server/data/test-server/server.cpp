#pragma clang diagnostic push
#pragma ide diagnostic ignored "cppcoreguidelines-avoid-non-const-global-variables"
#pragma ide diagnostic ignored "cert-err58-cpp"

#define DEFINE_SERVER_MAIN
#define APP_INFO "Data Server - Dev Build"

#include "server/unsafe/test-server.h"
#include <cstdlib>
#include <regex>
#include <iostream>
#include <chrono>
#include <thread>
#include <fstream>
#include <common/core/defer.h>

#include "server/unsafe/process.h"

DEF_CLI_OPTION(std::string, dbHost, "--db-host",
               "Postgresql database host address. Can also set with environment variable DB_HOST. Defaults to 127.0.0.1")
DEF_CLI_OPTION(int, dbPort, "--db-port",
               "Postgresql database port. Can also set with environment variable DB_PORT. Defaults to 5432")
DEF_CLI_OPTION(int, dbPoolSize, "--db-pool-size",
               "Postgresql database port. Can also set with environment variable DB_POOL_SIZE. Defaults to number of CPU cores")
DEF_CLI_OPTION(std::string, dbUser, "--db-user",
               "Postgresql database user. Can also set with environment variable DB_USER")
DEF_CLI_OPTION(std::string, dbPassword, "--db-pwd",
               "Postgresql database user password. Can also set with environment variable DB_PWD")
DEF_CLI_OPTION(std::string, dbDatabase, "--db-database",
               "Postgresql database to connect to. Can also set with environment variable DB_DATABASE. Defaults to the database user")
DEF_CLI_OPTION(std::string, podmanCmd, "--podman-cmd",
               "Podman command. Used to start the DB via podman. If provided, it will try to start the database with podman")
DEF_CLI_OPTION_DEFAULT(bool, podmanCleanMachine, false, "--podman-clean-machine",
                       "Whether should clean the podman machine (before and after run)")
DEF_CLI_OPTION_DEFAULT(bool, initPodmanDb, false, "--db-podman-setup",
                       "Sets up a podman DB.\nRequires --podman-cmd to be present, otherwise it will be ignored.\nNote: this does rebuild the DB container.")
DEF_CLI_OPTION_DEFAULT(std::string, podmanDbImage, "postgres:15.4-alpine", "--db-podman-image",
                       "Podman image to use for the container. Defaults to postgres:15.4")
DEF_CLI_OPTION_DEFAULT(std::string, podmanDbContainer, "test_postgres", "--db-podman-container",
                       "Podman container name to use. Defaults to test_postgres")
DEF_CLI_OPTION(std::string, liquibaseCmd, "--liquibase-cmd", "Liquibase command to use for database migrations")
DEF_CLI_OPTION(std::string, liquibasePropFile, "--liquibase-prop-file", "Where to write the liquibase property file")
DEF_CLI_OPTION(std::string, liquibaseChangelogFile, "--liquibase-changelog-file", "The change log file to read from")
DEF_CLI_OPTION(std::string, liquibaseJarFile, "--liquibase-jar-file",
               "The JAR file to the postgres driver for liquibase to use")

std::shared_ptr<server::unsafe::process::podman::Machine> machine;
std::unique_ptr<server::unsafe::process::podman::Container> container;

PRE_SERVER_START {
     const std::regex cliSafeRegex("^[A-Za-z0-9\\-_]+$");
     if (dbHost.empty()) {
         GET_ENV_OR_DEFAULT(dbHost, "DB_HOST", "127.0.0.1")
     }
     if (dbPort <= 0) {
         std::string envPort;
         GET_ENV_OR_DEFAULT(envPort, "DB_PORT", "5432")
         try {
             dbPort = std::stoi(envPort);
         }
         catch (...) {
             spdlog::error(
                     "Could not parse DB_PORT env variable; value '{}' is not a valid integer. Defaulting to 5432",
                     envPort);
             dbPort = 5432;
         }
     }

     if (dbPoolSize <= 0) {
         std::string envPool;
         GET_ENV_OR_DEFAULT(envPool, "DB_POOL_SIZE",
                            std::to_string(std::thread::hardware_concurrency()))
         try {
             dbPoolSize = std::stoi(envPool);
         }
         catch (...) {
             spdlog::error(
                     "Could not parse DB_POOL_SIZE env variable; value '{}' is not a valid integer. Defaulting to {}",
                     envPool, std::thread::hardware_concurrency());
             dbPoolSize = std::thread::hardware_concurrency();
         }
     }

     if (dbUser.empty()) {
         GET_ENV(dbUser, "DB_USER")
     }

     if (dbDatabase.empty()) {
         GET_ENV_OR_DEFAULT(dbDatabase, "DB_DATABASE", dbUser);
     }

     if (dbPassword.empty()) {
         GET_ENV(dbPassword, "DB_PWD")
     }

     if (!podmanDbContainer.empty()) {
         if (!std::regex_match(podmanDbContainer, cliSafeRegex)) {
             spdlog::critical(
                     "Invalid podman DB container name provided! Only valid characters are AlphaNumeric, Underscore, and dash");
             exit(1);
         }
     } else if (initPodmanDb) {
         spdlog::warn("No podman container name provided. Will not initialize podman db.");
         initPodmanDb = false;
     }

     if (!podmanDbImage.empty()) {
         if (podmanDbImage.contains("'")) {
             spdlog::critical("Invalid podman DB image provided! Cannot contain apostrophes");
             exit(1);
         }
     } else if (initPodmanDb) {
         spdlog::warn("No podman image provided. Will not initialize podman db.");
         initPodmanDb = false;
     }

     if (!podmanCmd.empty() && initPodmanDb) {
         if (dbHost != "127.0.0.1") {
             spdlog::warn("Setting DB Host to 127.0.0.1 to talk to podman container!");
             dbHost = "127.0.0.1";
         }
         if (!std::regex_match(dbPassword, cliSafeRegex)) {
             if (dbPassword.empty()) {
                 spdlog::critical(
                         "Invalid podman DB container password provided! Password may not be empty");
             } else {
                 spdlog::critical(
                         "Invalid podman DB container password provided! Only valid characters are AlphaNumeric, Underscore, and dash (for server created containers)");
             }
             exit(1);
         }

         if (!std::regex_match(dbUser, cliSafeRegex)) {
             if (dbPassword.empty()) {
                 spdlog::critical("Invalid podman DB container user provided! User may not be empty");
             } else {
                 spdlog::critical(
                         "Invalid podman DB container user provided! Only valid characters are AlphaNumeric, Underscore, and dash (for server created containers)");
             }
             exit(1);
         }

         if (!std::regex_match(dbDatabase, cliSafeRegex)) {
             if (dbPassword.empty()) {
                 spdlog::critical(
                         "Invalid podman DB container database provided! Database may not be empty");
             } else {
                 spdlog::critical(
                         "Invalid podman DB container database provided! Only valid characters are AlphaNumeric, Underscore, and dash (for server created containers)");
             }
             spdlog::critical(
                     "Invalid podman DB container database provided! Only valid characters are AlphaNumeric, Underscore, and dash (for server created containers)");
             exit(1);
         }

         unsigned int flags = 0;
         if (podmanCleanMachine) {
             flags |= server::unsafe::process::podman::Machine::CLEAN_AFTER |
                      server::unsafe::process::podman::Machine::CLEAN_BEFORE;
         }

         machine = server::unsafe::process::podman::Machine::create_machine(podmanCmd, flags).get();
         container = server::unsafe::process::podman::Container::create_container(
                 {
                      .name = podmanDbContainer,
                      .image = podmanDbImage,
                      .flags =
                      server::unsafe::process::podman::ContainerOptions::CLEAN_BEFORE |
                      server::unsafe::process::podman::ContainerOptions::CLEAN_AFTER,
                      .detach = true,
                      .portMapping = {
                              {5432, 5432}},
                      .environmentVariables = {
                              {"POSTGRES_PASSWORD", dbPassword},
                              {"POSTGRES_USER",     dbUser},
                              {"POSTGRES_DB",       dbDatabase},
                      }
                 },
                 machine
         ).get();

         spdlog::info("Waiting 5 seconds for DB to spin up.");
         std::this_thread::sleep_for(std::chrono::seconds(5));
     }

     if (!liquibasePropFile.empty()) {
         if (liquibaseChangelogFile.empty()) {
             spdlog::critical("Must provide a changelog file in order to create a property file!");
             exit(1);
         }

         auto liquibaseChangelogFilePath = std::filesystem::absolute(
                 std::filesystem::path(liquibaseChangelogFile));
         auto liquibasePath = liquibaseChangelogFilePath.parent_path() /
                              std::filesystem::path(liquibasePropFile).filename();
         spdlog::info("Writing Liquibase property file to {}", liquibasePath.string());

         {
             auto liquibaseJarFilePath = std::filesystem::absolute(
                     std::filesystem::path(liquibaseJarFile));
             std::ofstream propFile{liquibasePath};
             defer { propFile.close(); };
             propFile << "changeLogFile:" << liquibaseChangelogFilePath.filename().string()
                      << "\nurl: jdbc:postgresql://" << dbHost << ":" << dbPort << "/" << dbDatabase
                      << "\nusername: " << dbUser
                      << "\npassword: " << dbPassword
                      << "\nclasspath: " << liquibaseJarFilePath.string()
                      << std::endl;
         }

         if (!liquibaseCmd.empty()) {
             auto [exitCode, output] = server::unsafe::process::exec::Command::run(
                     "cd " + liquibaseChangelogFilePath.parent_path().string() + " && " + liquibaseCmd +
                     " --defaults-file=" + liquibasePath.string() + " update").get();
             if (exitCode) {
                 spdlog::critical("Could not run liquibase!");
                 spdlog::error(output);
                 exit(1);
             }
         }
     } else if (!liquibaseCmd.empty()) {
         auto [exitCode, output] = server::unsafe::process::exec::Command::run(liquibaseCmd + " update").get();
         if (exitCode) {
             spdlog::critical("Could not run liquibase!");
             spdlog::error(output);
             exit(1);
         }
     }
 };

POST_SERVER_STOP {
     container = nullptr;
     machine = nullptr;
 };

/**
 * GET /users
 * @summary List of users
 * @description Returns a list of all users
 * @response 200 - List of users
 * @response 400 - Invalid request
 * @responseContent {string} 200.application/json
 * @responseContent {string} 400.text/plain
 */
DEF_ROUTE("/users", HTTP_GET)
([](const Req &req, Res &res) {
    res.set_content("[]", mimeJson);
});

DEF_INTERACTIVE_COMMAND("info") [](const std::string &inputLine, bool &processedLine, bool &shouldQuit) {
    if (inputLine == "info server") {
        processedLine = true;
        std::cout << "SaaS Data Server - Prototype\n";
    } else if (inputLine == "info author") {
        processedLine = true;
        std::cout << "mtolman\n";
    } else if (inputLine == "info help") {
        processedLine = true;
        std::cout << "Usage: info [server|author]\n";
    } else {
        if (inputLine.size() > 6) {
            std::cerr << "Unknown info command " << (inputLine.substr(6))
                      << ", expected one of \"server\" or \"author\"\n";
        }
        else {
            std::cerr << "No info command provided, expected one of \"server\" or \"author\"\n";
        }
        std::cout << "Usage: info [server|author]\n";
    }
};

// This will only run when the previous handler didn't mark "processedLine"
DEF_INTERACTIVE_COMMAND("info2") [](const std::string &inputLine, bool &processedLine, bool &shouldQuit) {
    if (inputLine == "info2 author") {
        processedLine = true;
        std::cout << "mtolman\n";
    }
};

// This will only run when the previous handler didn't mark "processedLine"
DEF_INTERACTIVE_COMMAND("info2") [](const std::string &inputLine, bool &processedLine, bool &shouldQuit) {
    if (inputLine == "info2 help") {
        processedLine = true;
        std::cout << "Usage: info2 [server|author]\n";
    }
};

// This is the last default case
DEF_INTERACTIVE_COMMAND("info2") [](const std::string &inputLine, bool &processedLine, bool &shouldQuit) {
    if (inputLine.size() > 6) {
        std::cerr << "Unknown info command " << (inputLine.substr(6))
                  << ", expected one of \"server\" or \"author\"\n";
    }
    else {
        std::cerr << "No info command provided, expected one of \"server\" or \"author\"\n";
    }
    std::cout << "Usage: info [server|author]\n";
};

#pragma clang diagnostic pop