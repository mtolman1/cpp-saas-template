project(${SERVICE_NAME}-server-unsafe)

include(${CMAKE_SOURCE_DIR}/cmake/deps.cmake)
include(ExternalProject)

option(HTTPS "When on, https is used" OFF)

find_program(NODE bun node nodejs REQUIRED)

add_library(${PROJECT_NAME} STATIC src/test-server.cpp)
add_dependencies(main_build ${PROJECT_NAME})
target_include_directories(${PROJECT_NAME}
                           PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/include)
clang_tidy(${PROJECT_NAME})

if(HTTPS)
  target_compile_definitions(${PROJECT_NAME} PUBLIC SERVER_USE_HTTPS=1
                                                    CPPHTTPLIB_OPENSSL_SUPPORT)
else()
  target_compile_definitions(${PROJECT_NAME} PUBLIC SERVER_USE_HTTPS=0)
endif()

target_link_libraries(${PROJECT_NAME} PUBLIC httplib::httplib server::common
        cuid::thread_safe common::core)

add_executable(${PROJECT_NAME}-test tests/main.cpp)
target_link_libraries(${PROJECT_NAME}-test PRIVATE ${PROJECT_NAME} common::testing)
add_test(NAME ${PROJECT_NAME}-unit-tests COMMAND ${PROJECT_NAME}-test)

add_library(server::unsafe ALIAS ${PROJECT_NAME})

format_current_cmake()

make_docs()

find_program(CMAKE_FORMAT_EXE "cmake-format")

if(CMAKE_FORMAT_EXE)
  string(REPLACE "\\" "_" LIST_FILE ${LIST_FILE})
  string(REPLACE "." "_" LIST_FILE ${LIST_FILE})
  set(FORMAT_TARGET_NAME manual_server_unsafe_server_cmake_format)
  cmake_format(${FORMAT_TARGET_NAME}
               ${CMAKE_CURRENT_SOURCE_DIR}/test-server.cmake)
  add_dependencies(${PROJECT_NAME} ${FORMAT_TARGET_NAME})
endif()
