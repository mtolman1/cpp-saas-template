# server::unsafe Module

This module is for TESTING and DEVELOPMENT environments ONLY!

**DO NOT USE THIS IN PRODUCTION SYSTEMS!**

This module provides helpers for creating test servers which can help with manually testing small
segments and logic points directly, rather than only being able to test in a large system.

## Current Dependencies (remove, update, or move this section as desired)

The example code in this module can (and should) be replaced. Additionally, feel free to unlink
any example libraries that are linked. The libraries linked by default are:

### httplib

https://github.com/yhirose/cpp-httplib

Basic HTTP server/client library for C++.

Alternatives include:
* [drogon](https://github.com/drogonframework/drogon)
* [userver](https://userver.tech/)
* [pistache](https://pistacheio.github.io/pistache/)
* [oatpp](https://oatpp.io/)
* [crow](https://github.com/CrowCpp/Crow)
* [facil.io](http://facil.io/)
* [kore](https://kore.io/)
* [libonion](https://www.coralbits.com/libonion/)
* 
### common::core

The common core module.

### server::common

The server common module.

### cuid

https://gitlab.com/mtolman/cuid

A port of [cuid](https://www.npmjs.com/package/cuid) (V1) with a few custom extensions. Used for linking logs to the same request (assigns each request a cuid
and then logs the request).

> The port was made before CUID V2 came out. CUID V1 is deprecated since it's possible to predict
> future or past values, and that ability to predict future or past values could lead to security
> issues in some instances where a user being able to predict future ids could lead to information
> leakage (e.g. knowing how many accounts a client has, bypassing insufficient access controls,
> etc).
> 
> For this project, we're just using CUID for log association and correlation, which is not at high
> risk for id prediction. Additionally, we don't have to have extremely high collision resistance
> (i.e. we don't have to worry about new ids colliding with old ids from 10 years ago). Generally,
> any ofthe id generation methodologies listed here should work for this purpose.
> 
> The CUID is returned in an HTTP header so developers can quickly find the logs associated with a
> request.
> 
> **IMPORTANT:** While CUID V1 is acceptable for just logging, don't blindly use it for other use
> cases (e.g. database ids)! Additionally, if you are needing something secure, make sure you use
> a secure random number generator (the default random number generator for cuid is not secure)!

Alternatives include:

* [stduuid](https://github.com/mariusbancila/stduuid) ([UUID](https://en.wikipedia.org/wiki/Universally_unique_identifier), [C++ proposal](https://github.com/mariusbancila/stduuid/blob/master/P0959.md))
* [nanoid_cpp](https://github.com/mcmikecreations/nanoid_cpp) ([Nanoid](https://github.com/ai/nanoid))
* [ulid](https://github.com/suyash/ulid) ([Ulid](https://github.com/ulid/spec))
* [snowflake-cpp](https://github.com/sniper00/snowflake-cpp) ([Snowflake IDs](https://en.wikipedia.org/wiki/Snowflake_ID))
* [KsuidCPP](https://github.com/medamine101/KsuidCPP) ([KSUID](https://github.com/segmentio/ksuid))

## Creating a Test Server

**DO NOT USE TEST SERVERS IN PRODUCTION SYSTEMS!**

> Test servers do have CLI access to perform some functionality (e.g. run the node-based integration
> test suite on startup, run Liquibase database migrations, run podman, etc). Furthermore, these
> systems turn off HTTPS automatically (and many don't even compile with HTTPS). Also, they all come
> with an extendable interactive mode for developers to use for debugging. Do NOT use these in
> production! It's meant for testing and development purposes, not for production systems!

To create a test server, first create a `test-server` directory in the same directory as your CMakeLists.txt
file (e.g. if CMakeLists.txt is in `src/server/compute`, then create the directory `src/server/compute/test-server`).

Next, inside your `test-server` directory create a `requests` folder. This folder will hold automatic
integration tests that can be run against your test server. All the test files in this folder will
end with the `.http` extension. These files can also be run directly inside a modern version of a
JetBrains IDE (e.g. CLion). Below is an example test file:

```http request
# File: test.http

### Home
GET {{url}}
Authorization: {{auth}}

> {%
client.test("Home", function() {
client.global.set('value', client.global.get('runId'))
});
%}

### 2024 Independence Day
GET {{url}}/holidays/us/independenceDay/2024
Authorization: {{auth}}

> {%
client.test("date is correct", function() {
client.assert(response.body === '2024-07-04');
});
%}
```

> Do note that all test environment definitions are currently defined inside the `http-client.env.json`
> file in the root project. This is due to a weird quirk with the JetBrains IDE integrations where it
> doesn't handle multiple environment files being defined in different directories. Consequently, to
> maintain compatibility with JetBrains IDEs, all test environment variables are defined in a single
> file. Do keep this in mind when deciding on standards to use for defining testing environment variables.

Next, create a `yaml` directory under your `test-server` directory. This directory will be used to
generate OpenApi documentation for your project (which will also be used to automatically create 
interactive documentation so that devs can quickly use your project).

Inside your `yaml` directory, create an `openapi.yaml` file. This file will follow the
[OpenApi](https://swagger.io/specification/) specification (version 3.0.0).

> Note: You do not have to define operations manually inside the yaml file if you don't want to.
> The test-server will use a [JSDoc OpenApi Comment Parser](https://github.com/bee-travels/openapi-comment-parser)
> to parse the operations from your test server's cpp file (we'll create that in a moment).
> 
> This is why we use JSDoc style comments (/** @param ... */) rather than Microsoft's XML style
> commonts (//// <param> ... </param>)

Finally, create a `server.cpp` file under your `test-server` directory. This is the only cpp file
that will be compiled by the test server project. In this file, you will need to do the following
* `#define DEFINE_SERVER_MAIN`
  * This will generate the main function for the test server, and it will do the needed bootstrapping of the test server
* `#define APP_INFO "YOUR APP INFO HERE"`
  * Make sure your string literal describes your test server. This is used for CLI help
* `#include "server/unsafe/test-server.h"`
  * This includes the test server framework
* Define routes with `DEF_ROUTE`. Comments can be used to describe the OpenAPI spec

```cpp
/**
 * POST /holidays/us/veteransDay
 * @summary US Veterans Day
 * @response 200 - Date of US Veterans Day
 * @response 400 - Invalid Request
 * @description Returns the US veterans day for a given year
 * @bodyRequired
 * @bodyComponent {YearBody}
 * @responseContent {GregorianDate} 200.application/json
 * @responseContent {InvalidRequest} 400.application/json
 * @responseExample {GregorianDateResponse} 200.application/json.GregorianDateResponse
 */
DEF_ROUTE("/holidays/us/veteransDay", HTTP_POST)
([](const Req &req, Res &res) {
  nlohmann::json input;
  try {
    input = nlohmann::json::parse(req.body);
    nlohmann::json val{};
  
    if (!input.contains("year") || !input.at("year").is_number_integer()) {
      return invalid_request(res, "year", "invalid_integer");
    }
  
    auto veteransDay = calendars::holidays::usa::veterans_day(input.at("year"));
  
    res.set_content(nlohmann::to_string(nlohmann::json{{"year", static_cast<int>(veteransDay.year())},
                                                       {"month", static_cast<int>(veteransDay.month())},
                                                       {"day", static_cast<int>(veteransDay.day())},
                                                       {"calendar", "GREGORIAN"}}),
                    mimeJson);
  }
  catch (...) {
    return invalid_request(res, "*", "unable_to_parse");
  }
});
```

* In your `CMakeLists.txt` file, include `test-server.cmake` and then call `add_test_server()`. 
* To add integration tests for your test server to CMake, add the following to your `CMakeLists.txt` file

```cmake
# Check that our test server is being built and that integration tests are running
if(WITH_INTEGRATION_TESTS AND NOT EXCLUDE_TEST_SERVERS)
  # We need to pass node to our test server
  # Note: As of writing, Bun's zlib decompression doesn't work properly on Mac, so using node instead
  find_program(NODE_EXE node nodejs REQUIRED)

  add_test(
    NAME ${PROJECT_NAME}-integration-tests
    COMMAND
      ${PROJECT_NAME}-test-server
      --log-level=debug
      --test-node=${NODE_EXE}
      --test-script=${CMAKE_CURRENT_SOURCE_DIR}/../common/integration-tester/test.js
      --test-directory=${CMAKE_CURRENT_SOURCE_DIR}/test-server/requests
      --test-env-file=${CMAKE_SOURCE_DIR}/http-client.env.json
      # Change this to the environment name you want to run the HTTP requests using
      --test-env=local-dev
      # Set this to how many clients you want to run concurrently
      --test-client-count=10
      # Add any additional CLI options here (e.g. DB host, credentials, etc)
  )
endif()
```

And you're done! You've created a test server which you can use manually, and you've also built
integration tests and added them to your CMake build as well.

### Adding CLI options

To add additional CLI options, use `DEF_CLI_OPTION` for an option without a default value (useful
for falling back to environment variables if not set), or use `DEF_CLI_OPTION_DEFAULT` for an option
with a default value.

`DEF_CLI_OPTION` takes an option type, name for a global variable that will be set to that option,
the flag for the option, and a description of the option.

```cpp
DEF_CLI_OPTION(std::string, dbHost, "--db-host",
               "Postgresql database host address. Can also set with environment variable DB_HOST. Defaults to 127.0.0.1")
```


`DEF_CLI_OPTION` takes an option type, name for a global variable that will be set to that option,
the default value of the option, the flag for the option, and a description of the option.

```cpp
DEF_CLI_OPTION_DEFAULT(bool, podmanCleanMachine, false, "--podman-clean-machine",
                       "Whether should clean the podman machine (before and after run)")
```

### Adding pre startup code

Sometimes environment initialization or CLI option verification code needs to run before a server
is started. To do this, use the `PRE_SERVER_START` macro

```cpp
PRE_SERVER_START {
    spdlog::info("Initializing server!");
    run_initialization();
};
```

### Adding post shutdown code

Sometimes environment cleanup is needed once a server is stopped. To do this, use the `POST_SERVER_STOP`
macro.

```cpp
POST_SERVER_STOP {
  run_cleanup();
}
```

## Project Example Usage

ADD EXAMPLES OF YOUR COMMON CODE HERE
