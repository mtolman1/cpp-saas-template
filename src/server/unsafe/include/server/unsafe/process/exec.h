#pragma once

#ifndef SERVER_DEFAULT_PORT
#define SERVER_DEFAULT_PORT 18080
#endif

#include <spdlog/spdlog.h>

#include <array>
#include <future>
#include <string>
#include <tuple>

#ifdef SERVER_WITH_CLI_ACCESS
#warning "Compiling with server CLI access! THIS IS NOT MEANT FOR PRODUCTION!"
#endif

namespace server::unsafe::process::exec {
    struct Command {
        [[nodiscard]] static auto run(const std::string &cmd) -> std::future<std::tuple<int, std::string>> {
#ifdef SERVER_WITH_CLI_ACCESS
            spdlog::warn("CLI access enabled! Should not be running in production!");
            return std::async(
                std::launch::async,
                [](const std::string &cmd) -> std::tuple<int, std::string> {
                    std::array<char, 128> buffer{};
                    std::string           result{};
                    auto                  newCmd = cmd + " 2>&1";
                    spdlog::debug("Running CLI cmd: {}", newCmd);
                    auto *pipe = popen(newCmd.c_str(), "r");
                    if (pipe == nullptr) {
                        throw std::runtime_error("popen() failed [run]!");
                    }
                    while (fgets(buffer.data(), buffer.size(), pipe) != nullptr) {
                        result += buffer.data();
                        spdlog::trace("{}", buffer.data());
                    }
                    auto retCode = pclose(pipe);
                    return {retCode, result};
                },
                cmd);
#else
            return std::async(
                std::launch::any,
                [](const std::string &cmd) -> std::tuple<int, std::string> {
                    spdlog::info("Not running command {}, no CLI access provided");
                    return std::tuple<int, std::string>{0, ""};
                },
                cmd);
#endif
        }

        [[nodiscard]] static auto run_no_output(const std::string &cmd) -> std::future<int> {
#ifdef SERVER_WITH_CLI_ACCESS
            spdlog::warn("CLI access enabled! Should not be running in production!");
            return std::async(
                std::launch::async,
                [](const std::string &cmd) -> int {
                    std::array<char, 128> buffer{};
                    spdlog::debug("Running CLI cmd: {}", cmd);
                    auto *pipe = popen(cmd.c_str(), "r");
                    if (pipe == nullptr) {
                        throw std::runtime_error("popen() failed [run_no_output]!");
                    }
                    while (fgets(buffer.data(), buffer.size(), pipe) != nullptr) {
                        spdlog::trace("{}", buffer.data());
                    }
                    return pclose(pipe);
                },
                cmd);
#else
            return std::async(
                std::launch::any,
                [](const std::string &cmd) -> int {
                    spdlog::info("Not running command {}, no CLI access provided");
                    return 0;
                },
                cmd);
#endif
        }
    };
}  // namespace server::unsafe::process::exec
