#pragma once

#ifdef SERVER_WITH_PODMAN_CALLS
#warning "Compiling with server Podman access! THIS IS NOT MEANT FOR PRODUCTION!"
#ifndef SERVER_WITH_CLI_ACCESS
// We need CLI access,
#define SERVER_WITH_CLI_ACCESS
#endif
#endif

#include <spdlog/spdlog.h>

#include <exception>
#include <filesystem>
#include <future>
#include <map>
#include <optional>
#include <sstream>
#include <string>
#include <vector>

#include "exec.h"

namespace server::unsafe::process::podman {
    enum class ActionResult {
        CHANGE_MADE,
        NO_CHANGE_NEEDED,
        CHANGE_FAILED,
    };

    class Container;

    enum class AttachOptions {
        STDIN,
        STDOUT,
        STDERR
    };

    struct ContainerOptions {
        enum FLAGS : unsigned int {
            CLEAN_BEFORE = 0x1u << 0,
            CLEAN_AFTER  = 0x1u << 1,
        };

        std::string                        name;
        std::string                        image;
        unsigned int                       flags                = 0x0;
        bool                               detach               = false;
        std::map<int, int>                 portMapping          = {};
        std::map<std::string, std::string> environmentVariables = {};
        ;

        std::optional<std::filesystem::path> pidFile     = std::nullopt;
        std::map<std::string, std::string>   annotations = {};
    };

    class Machine {
       public:
        enum FLAGS : unsigned int {
            CLEAN_BEFORE              = 0x1u << 0u,
            CLEAN_AFTER               = 0x1u << 1u,
            STOP_AFTER                = 0x1u << 2u,
            ASSUME_EXTERNALLY_MANAGED = 0x1u << 3u,
        };

        Machine(const Machine &) = delete;

        auto operator=(const Machine &) -> Machine & = delete;

        Machine(Machine &&other) noexcept {
            std::swap(flags, other.flags);
            std::swap(podmanCmd, other.podmanCmd);
        }

        auto operator=(Machine &&other) noexcept -> Machine & {
            std::swap(flags, other.flags);
            std::swap(podmanCmd, other.podmanCmd);
            return *this;
        }

        ~Machine() {
            if (!podmanCmd.empty()) {
                if ((flags & STOP_AFTER) != 0u) {
                    stop_machine();
                }

                if ((flags & CLEAN_AFTER) != 0u) {
                    clean_machine();
                }
                podmanCmd.clear();
                flags = 0;
            }
        }

        static auto create_machine(const std::string &podmanCmd, unsigned int flags = 0x0) -> std::future<std::shared_ptr<Machine>> {
            return std::async(
                std::launch::async,
                [](const std::string &podmanCmd, unsigned int flags) -> std::shared_ptr<Machine> {
                    auto machine = std::make_shared<Machine>(Machine{podmanCmd, flags});
                    if ((flags & ASSUME_EXTERNALLY_MANAGED) != 0u) {
                        return machine;
                    }

                    if ((flags & CLEAN_BEFORE) != 0u) {
                        machine->clean_machine();
                    }
                    machine->init_machine();
                    machine->start_machine();
                    return machine;
                },
                podmanCmd,
                flags);
        }

        [[nodiscard]] auto command() const -> std::string { return podmanCmd; }

       private:
        Machine(const std::string &podmanCmd, unsigned int flags = 0x0) : flags(flags), podmanCmd(podmanCmd) {}

        auto clean_machine() -> void {
#ifdef SERVER_WITH_PODMAN_CALLS
            spdlog::warn("Podman access enabled! Should not be running in production!");
            spdlog::info("Checking podman machine (for cleanup)...");
            auto [ exitCode, output ] = server::unsafe::process::exec::Command::run("podman machine list").get();
            if (!output.ends_with("DISK SIZE\n")) {
                spdlog::info("Podman machine detected. Removing it...");
                auto [ rmExitCode, rmOutput ] = server::unsafe::process::exec::Command::run("podman machine rm -f").get();
                if (rmExitCode != 0) {
                    spdlog::critical("Could not clean up podman machine!");
                    spdlog::error(rmOutput);
                    throw std::runtime_error("unable to clean up podman");
                }
                else {
                    spdlog::info("Podman machine removed.");
                }
            }
            else {
                spdlog::info("No podman machine detected");
            }
#else
            spdlog::info("Not cleaning podman machine. Reason: podman command are not enabled");
#endif
        }

        auto init_machine() -> void {
#ifdef SERVER_WITH_PODMAN_CALLS
            spdlog::warn("Podman access enabled! Should not be running in production!");
            spdlog::info("Checking podman machine (for initialization)...");
            auto [ exitCode, output ] = server::unsafe::process::exec::Command::run("podman machine list").get();
            if (output.ends_with("DISK SIZE\n")) {
                spdlog::info("No podman machine detected. Initializing podman machine...");
                auto [ initExitCode, initOutput ] = server::unsafe::process::exec::Command::run("podman machine init").get();
                if (initExitCode != 0) {
                    spdlog::critical("Could not initialize podman machine! Please manually initialize!");
                    spdlog::error(initOutput);
                    throw std::runtime_error("could not initialize podman machine");
                }
                else {
                    spdlog::info("Podman machine initialized");
                }
            }
            else {
                spdlog::info("Podman machine detected");
            }
#else
            spdlog::info("Not initializing podman machine. Reason: podman command are not enabled");
#endif
        }

        auto start_machine() -> void {
#ifdef SERVER_WITH_PODMAN_CALLS
            spdlog::warn("Podman access enabled! Should not be running in production!");
            spdlog::info("Starting podman machine...");
            auto [ exitCode, output ] = server::unsafe::process::exec::Command::run("podman machine start").get();
            if (exitCode != 0) {
                if (output.contains("already running")) {
                    spdlog::info("Podman machine already running");
                }
                else {
                    spdlog::critical("Could not start podman machine! Please manually start!");
                    spdlog::error(output);
                    throw std::runtime_error("could not start podman machine");
                }
            }
            else {
                spdlog::info("Podman machine started.");
            }
#else
            spdlog::info("Not starting podman machine. Reason: podman command are not enabled");
#endif
        }

        auto stop_machine() -> void {
#ifdef SERVER_WITH_PODMAN_CALLS
            spdlog::warn("Podman access enabled! Should not be running in production!");
            spdlog::info("Stopping podman machine...");
            auto [ exitCode, output ] = server::unsafe::process::exec::Command::run("podman machine stop").get();
            if (exitCode != 0) {
                if (output.contains("VM does not exist")) {
                    spdlog::info("Could not find podman machine. Considering it to be already stopped");
                }
                else {
                    spdlog::critical("Could not stop podman machine!");
                    spdlog::error(output);
                    throw std::runtime_error("could not stop podman machine");
                }
            }
            else {
                spdlog::info("Podman machine stopped.");
            }
#else
            spdlog::info("Not stopping podman machine. Reason: podman command are not enabled");
#endif
        }

        unsigned int flags = 0x0u;
        std::string  podmanCmd{};

        friend std::shared_ptr<Machine> std::make_shared<Machine>(Machine &&);
    };

    class Container {
       public:
        ~Container() {
            if (machine != nullptr) {
                if ((options.flags & ContainerOptions::CLEAN_AFTER) != 0u) {
                    clean();
                }
                else {
                    stop();
                }
            }
        }

        Container(const Container &) = delete;

        auto operator=(const Container &) -> Container & = delete;

        Container(Container &&other) noexcept {
            std::swap(options, other.options);
            std::swap(machine, other.machine);
        }

        auto operator=(Container &&other) noexcept -> Container & {
            std::swap(options, other.options);
            std::swap(machine, other.machine);
            return *this;
        }

        static auto create_container(const ContainerOptions &options, const std::shared_ptr<Machine> &machine)
            -> std::future<std::unique_ptr<Container>> {
            return std::async(
                std::launch::async,
                [](const ContainerOptions &options, const std::shared_ptr<Machine> &machine) -> std::unique_ptr<Container> {
                    auto container = std::make_unique<Container>(Container{options, machine});

                    if ((options.flags & ContainerOptions::CLEAN_BEFORE) != 0u) {
                        container->clean();
                    }

                    container->run();

                    return container;
                },
                options,
                machine);
        }

       private:
        Container(const ContainerOptions &options, const std::shared_ptr<Machine> &machine) : machine(machine), options(options) {}

        std::shared_ptr<Machine> machine;
        ContainerOptions         options;

        auto clean() -> void {
#ifdef SERVER_WITH_PODMAN_CALLS
            spdlog::warn("Podman access enabled! Should not be running in production!");
            spdlog::warn("Searching for previous DB containers to remove. Searching for name {}...", options.name);
            auto [ exitCode, output ] = server::unsafe::process::exec::Command::run("podman ps -a -f name='^" + options.name + "$'").get();
            if (exitCode) {
                spdlog::critical("Could not fetch current container info");
                spdlog::error(output);
                throw std::runtime_error("could not get podman container info");
            }

            if (!output.ends_with("NAMES\n")) {
                spdlog::warn("Removing existing podman container {}", options.name);
                auto [ cleanExitCode, cleanOutput ] = server::unsafe::process::exec::Command::run("podman rm -f " + options.name).get();
                if (cleanExitCode) {
                    spdlog::critical("Could not remove container {}", options.name);
                    spdlog::error(cleanOutput);
                    throw std::runtime_error("could not clean podman container");
                }
                spdlog::warn("Podman container {} removed successfully!", options.name);
            }
#else
            spdlog::info("Not cleaning podman container. Reason: podman command are not enabled");
#endif
        }

        auto run() -> void {
#ifdef SERVER_WITH_PODMAN_CALLS
            spdlog::warn("Podman access enabled! Should not be running in production!");
            bool existingContainer;
            {
                spdlog::info("Searching for previous DB containers to start. Searching for name {}...", options.name);
                auto [ exitCode, output ] = server::unsafe::process::exec::Command::run("podman ps -a -f name='^" + options.name + "$'").get();
                if (exitCode) {
                    spdlog::critical("Could not fetch current container info. Going to try using the run command");
                    spdlog::error(output);
                }

                existingContainer = !output.ends_with("NAMES\n");
            }

            std::stringstream command{};
            if (!existingContainer) {
                command << machine->command() << " container run ";

                if (options.detach) {
                    command << "-d ";
                }

                for (const auto &ports : options.portMapping) {
                    command << "-p " << ports.first << ":" << ports.second << " ";
                }

                for (const auto &envVar : options.environmentVariables) {
                    command << "-e " << envVar.first << "=" << envVar.second << " ";
                }

                if (options.pidFile) {
                    command << "--pidfile=" << std::filesystem::absolute(options.pidFile.value()).string() << " ";
                }

                for (const auto &annotation : options.annotations) {
                    command << "--annotation " << annotation.first << "=" << annotation.second << " ";
                }

                command << options.image;
            }
            else {
                command << machine->command() << " container start " << options.name;
            }

            spdlog::info("Starting container {}", options.name);
            auto [ exitCode, output ] = server::unsafe::process::exec::Command::run(command.str()).get();
            if (exitCode) {
                spdlog::critical("Could not start container {}", options.name);
                spdlog::error(output);
                throw std::runtime_error("unable to start container");
            }
#else
            spdlog::info("Not starting podman container. Reason: podman command are not enabled");
#endif
        }

        auto stop() -> void {
#ifdef SERVER_WITH_PODMAN_CALLS
            spdlog::warn("Podman access enabled! Should not be running in production!");
            bool existingContainer;
            {
                spdlog::info("Searching for previous DB containers to start. Searching for name {}...", options.name);
                auto [ exitCode, output ] = server::unsafe::process::exec::Command::run("podman ps -a -f name='^" + options.name + "$'").get();
                if (exitCode) {
                    spdlog::critical("Could not fetch current container info. Going to try using the run command");
                    spdlog::error(output);
                }

                existingContainer = !output.ends_with("NAMES\n");
            }

            if (existingContainer) {
                auto [ exitCode, output ] = server::unsafe::process::exec::Command::run(machine->command() + " container stop " + options.name).get();
                if (exitCode) {
                    spdlog::critical("Could not stop container {}", options.name);
                    spdlog::error(output);
                    throw std::runtime_error("unable to stop container");
                }
            }
#else
            spdlog::info("Not stopping podman container. Reason: podman command are not enabled");
#endif
        }

        friend std::unique_ptr<Container> std::make_unique<Container>(Container &&);
    };
}  // namespace server::unsafe::process::podman
