#pragma once

/*
 * This file is for test-servers only.
 * These test servers are meant for internal use only and may not be secure, free of bugs, scalable, etc.
 * It is meant to provide a foundation for creating internal testing tools for QA, and it also
 *   serves as a jumping point for remotely managed integration testing
 *
 * DO NOT USE THIS SERVER CODE IN PRODUCTION
 */

#warning "THIS IS A TEST SERVER! DO NOT USE IN PRODUCTION!"

#ifndef SERVER_DEFAULT_PORT
#define SERVER_DEFAULT_PORT 18080
#endif

#include <common/core.h>
#include <server/unsafe/process.h>
#include <spdlog/spdlog.h>

#include <array>
#include <cstdio>
#include <filesystem>
#include <future>
#include <iostream>
#include <nlohmann/json.hpp>
#include <server/common/CLI11.hpp>
#include <stdexcept>
#include <string>
#include <thread>
#include <tuple>
#include <variant>

#include "httplib.h"

/**
 * Convenience alias for httplib::Request
 */
using Req = httplib::Request;

/**
 * Convenience alias for httplib::Response
 */
using Res = httplib::Response;

/**
 * MIME type for HTML results
 */
constexpr const char *mimeHtml = "text/html";

/**
 * MIME type for JSON results
 */
constexpr const char *mimeJson = "application/json";

/**
 * MIME type for text results
 */
constexpr const char *mimeText = "text/plain";

/** @cond **/
namespace impl {
    inline auto invalid_request(nlohmann::json & /*unused*/) {}

    template<typename Key, typename Value, typename... StrParams>
    auto invalid_request(nlohmann::json &res, Key key, Value value, StrParams... params) {
        static_assert(sizeof...(params) % 2 == 0, "Must provide pairs of fieldName/value to invalid_request");
        res.push_back({{"field", key}, {"desc", value}});
        invalid_request(res, params...);
    }
}  // namespace impl
/** @endcond **/

/**
 * Returns a 400 "Bad Request" with details on why the request was bad
 * This is meant as a quick way of telling testers what went wrong and how to fix it
 *
 * Example:
 * ```cpp
 *  // No fields
 *  invalid_request(response);
 *
 *  // One fields
 *  invalid_request(response, "year", "missing");
 *
 *  // Many fields
 *  invalid_request(response, "year", "missing", "password", "too short");
 * ```
 *
 * @tparam FieldNamesErrorDesc Series of fieldName/values. The fieldNames are the field name with the issue, and the value is a description of the
 * issue
 * @param response The response object to set
 * @param params A list of fieldName/values describing the issues. These are not pairs so don't wrap them in curly braces
 */
template<typename... FieldNamesErrorDesc>
auto invalid_request(Res &response, FieldNamesErrorDesc... params) -> void {
    static_assert(sizeof...(params) % 2 == 0, "Must provide pairs of fieldName/value to invalid_request");
    nlohmann::json value{};
    impl::invalid_request(value, params...);
    response.set_content(nlohmann::to_string(value), mimeJson);
}

#if defined CPPHTTPLIB_OPENSSL_SUPPORT && SERVER_USE_HTTPS
using Server = std::variant<std::shared_ptr<httplib::SSLServer>, std::shared_ptr<httplib::Server>>;
#else
using Server = std::variant<std::shared_ptr<httplib::Server>>;
#endif

/** @cond */
namespace impl {
    using server_pre_callback       = std::function<void()>;
    using server_post_callback      = std::function<void()>;
    using server_endpoint_callback  = std::function<void(const Server &)>;
    using cli_opt_register_callback = std::function<void(CLI::App &)>;
    using interactive_line_callback = std::function<void(const std::string &inputLine, bool &processedLine, bool &shouldQuit)>;

    extern std::vector<server_pre_callback> serverPreStartCallbacks;  // NOLINT(cppcoreguidelines-avoid-non-const-global-variables)

    extern std::vector<server_endpoint_callback> serverEndpointCallbacks;  // NOLINT(cppcoreguidelines-avoid-non-const-global-variables)

    extern std::vector<server_post_callback> serverPostCallbacks;  // NOLINT(cppcoreguidelines-avoid-non-const-global-variables)

    extern std::vector<cli_opt_register_callback> cliOptionRegisterCallbacks;  // NOLINT(cppcoreguidelines-avoid-non-const-global-variables)

    extern std::map<std::string, std::vector<interactive_line_callback>, std::less<>>
        interactiveCallbacks;  // NOLINT(cppcoreguidelines-avoid-non-const-global-variables)

    struct PreServerBuilder {};
    struct PostServerBuilder {};

    template<typename F>
    auto operator+(PreServerBuilder, F f) -> bool {
        impl::serverPreStartCallbacks.emplace_back(f);
        return true;
    }

    template<typename F>
    auto operator+(PostServerBuilder, F f) -> bool {
        impl::serverPostCallbacks.emplace_back(f);
        return true;
    }

    enum METHOD {
        GET,
        POST,
        PUT,
        OPTIONS,
        DELETE,
        PATCH
    };

    struct InteractiveLineRegister {
        std::string hint;
    };

    inline auto register_interactive_line_processor(const std::string &commandHint) -> InteractiveLineRegister {
        return InteractiveLineRegister{commandHint};
    }

    inline auto operator+(const InteractiveLineRegister &r, interactive_line_callback callback) -> bool {
        interactiveCallbacks[ r.hint ].emplace_back(callback);
        return true;
    }

    template<typename... Methods>
    auto register_endpoint(const char *path, Methods... methodArgs) {
        return [ path, methods = std::array{methodArgs...} ](auto callback) {
            serverEndpointCallbacks.emplace_back([ = ](const Server &serv) {
                std::visit(common::core::overload{[ = ](auto &s) {
                               for (const auto &method : methods) {
                                   switch (method) {
                                       case METHOD::GET:
                                           s->Get(path, callback);
                                           break;
                                       case METHOD::POST:
                                           s->Post(path, callback);
                                           break;
                                       case METHOD::PUT:
                                           s->Put(path, callback);
                                           break;
                                       case METHOD::PATCH:
                                           s->Patch(path, callback);
                                           break;
                                       case METHOD::DELETE:
                                           s->Delete(path, callback);
                                           break;
                                       case METHOD::OPTIONS:
                                           s->Options(path, callback);
                                           break;
                                   }
                               }
                           }},
                           serv);
            });
            return true;
        };
    }

    template<typename T>
    auto register_cli_opt(const std::string &optionName, T &outVariable) {
        return [ =, &outVariable ](const std::string &optionDescription = "") {
            cliOptionRegisterCallbacks.emplace_back(
                [ =, &outVariable ](CLI::App &app) { app.add_option(optionName, outVariable, optionDescription); });
            return true;
        };
    }
}  // namespace impl

#define SERVER_CAT_IMPL(s1, s2) s1##s2
#define SERVER_CAT(s1, s2)      SERVER_CAT_IMPL(s1, s2)
#ifdef __COUNTER__  // not standard and may be missing for some compilers
#define SERVER_ANONYMOUS(x) SERVER_CAT(x, __COUNTER__)
#else  // __COUNTER__
#define SERVER_ANONYMOUS(x) SERVER_CAT(x, __LINE__)
#endif  // __COUNTER__
/** @endcond */

/**
 * Gets a value from an environment variable, or if it is not set in the environment it sets it to a default value
 *
 * Example:
 *
 * ```cpp
 *  std::string myVar;
 *  GET_ENV_OR_DEFAULT(myVar, "MY_PARAM", "Default Value");
 * ```
 */
#define GET_ENV_OR_DEFAULT(STRING_VAR_NAME, ENV_NAME, DEFAULT_VALUE)                                                                                 \
    {                                                                                                                                                \
        auto *envTmp = std::getenv(ENV_NAME);                                                                                                        \
        if (envTmp == nullptr) {                                                                                                                     \
            STRING_VAR_NAME = DEFAULT_VALUE;                                                                                                         \
        }                                                                                                                                            \
        else {                                                                                                                                       \
            STRING_VAR_NAME = envTmp;                                                                                                                \
        }                                                                                                                                            \
    }

/**
 * Gets a value from an environment variable
 * If the value is not in the environment, it will be set to an empty string
 *
 * Example:
 *
 * ```cpp
 *  std::string myVar;
 *  GET_ENV(myVar, "MY_PARAM");
 * ```
 */
#define GET_ENV(STRING_VAR_NAME, ENV_NAME) GET_ENV_OR_DEFAULT(STRING_VAR_NAME, ENV_NAME, "")

/**
 * Defines a new interactive command for interactive mode
 *
 *
 * Example:
 * ```cpp
 * // Single, large command
 * DEF_INTERACTIVE_COMMAND("info") [](const std::string &inputLine, bool &processedLine, bool &shouldQuit) {
 *    // Set this to "true" to indicate that you've processed the line and no further handlers need to look at this
 *    processedLine = true;
 *    if (inputLine == "info server") {
 *        std::cout << "SaaS Data Server - Prototype\n";
 *    }
 *    else if (inputLine == "info author") {
 *        std::cout << "mtolman\n";
 *    }
 *    else if (inputLine == "info help") {
 *        std::cout << "Usage: info2 [server|author]\n";
 *    }
 *    else if (inputLine == "info") {
 *      std::cout << "SaaS Data Server - Prototype\nAuthor: mtolman\n";
 *    }
 *    else {
 *        std::cerr << "Unknown info command, expected one of \"server\" or \"author\"\n";
 *        std::cout << "Usage: info [server|author]\n";
 *    }
 * };
 *
 * // Many, smaller commands
 * // This pattern can be useful for keeping functions and logic small
 * // or it can be used for "fallthrough" cases (similar to switch statements)
 * DEF_INTERACTIVE_COMMAND("info2") [](const std::string &inputLine, bool &processedLine, bool &shouldQuit) {
 *    if (inputLine == "info2 server") {
 *        // When we don't set processedLine, then the interactive session will try another session
 *        processedLine = true;
 *        std::cout << "SaaS Data Server - Prototype\n";
 *    }
 *  };
 *
 * // This will only run when the previous handler didn't mark "processedLine"
 * DEF_INTERACTIVE_COMMAND("info2") [](const std::string &inputLine, bool &processedLine, bool &shouldQuit) {
 *    if (inputLine == "info2 author") {
 *        processedLine = true;
 *        std::cout << "mtolman\n";
 *    }
 * };
 *
 * // This will only run when the previous handler didn't mark "processedLine"
 * DEF_INTERACTIVE_COMMAND("info2") [](const std::string &inputLine, bool &processedLine, bool &shouldQuit) {
 *    if (inputLine == "info2 help") {
 *        processedLine = true;
 *        std::cout << "Usage: info [server|author]\n";
 *    }
 * };
 *
 * // This is the last default case
 * DEF_INTERACTIVE_COMMAND("info2") [](const std::string &inputLine, bool &processedLine, bool &shouldQuit) {
 *     processedLine = true;
 *     std::cerr << "Unknown info command, expected one of \"server\" or \"author\"\n";
 *     std::cout << "Usage: info [server|author]\n";
 * }
 * ```
 */
#define DEF_INTERACTIVE_COMMAND(COMMAND_NAME) const bool SERVER_ANONYMOUS(interactive) = impl::register_interactive_line_processor(COMMAND_NAME) +

/**
 * Represents an HTTP GET method for use with DEF_ROUTE
 */
[[maybe_unused]] constexpr impl::METHOD HTTP_GET = impl::METHOD::GET;  // NOLINT(readability-identifier-naming)

/**
 * Represents an HTTP POST method for use with DEF_ROUTE
 */
[[maybe_unused]] constexpr impl::METHOD HTTP_POST = impl::METHOD::POST;  // NOLINT(readability-identifier-naming)

/**
 * Represents an HTTP PUT method for use with DEF_ROUTE
 */
[[maybe_unused]] constexpr impl::METHOD HTTP_PUT = impl::METHOD::PUT;  // NOLINT(readability-identifier-naming)

/**
 * Represents an HTTP OPTIONS method for use with DEF_ROUTE
 */
[[maybe_unused]] constexpr impl::METHOD HTTP_OPTIONS = impl::METHOD::OPTIONS;  // NOLINT(readability-identifier-naming)

/**
 * Represents an HTTP DELETE method for use with DEF_ROUTE
 */
[[maybe_unused]] constexpr impl::METHOD HTTP_DELETE = impl::METHOD::DELETE;  // NOLINT(readability-identifier-naming)

/**
 * Represents an HTTP PATCH method for use with DEF_ROUTE
 */
[[maybe_unused]] constexpr impl::METHOD HTTP_PATCH = impl::METHOD::PATCH;  // NOLINT(readability-identifier-naming)

/**
 * Defines a server route. It may accept more than one HTTP method if the route should serve multiple HTTP methods
 *
 * A callable surrounded by parenthesis after the macro will define a route.
 *
 * Example:
 *
 * ```cpp
 *  DEF_ROUTE("/", HTTP_GET, HTTP_POST)([](const Req &req, Res &res) { res.set_content(htmlDef, mimeHtml); });
 *
 *  DEF_ROUTE("/openapi.json", HTTP_GET)([](const Req &req, Res &res) { res.set_content(openapiDef, mimeJson); });
 * ```
 */
#define DEF_ROUTE(PATH, ...) const bool SERVER_ANONYMOUS(endpoint) = impl::register_endpoint(PATH, __VA_ARGS__)

/**
 * Defines a block of code that will run before the server is started
 *
 * Example:
 *
 * ```cpp
 *  PRE_SERVER_START {
 *      std::cout << "Running code before the server starts\n";
 *  };
 * ```
 */
#define PRE_SERVER_START const bool SERVER_ANONYMOUS(pre_start) = impl::PreServerBuilder{} + []()

/**
 * Defines a block of code that will run after the server is stopped
 *
 * Example:
 *
 * ```cpp
 *  POST_SERVER_STOP {
 *      std::cout << "Running code after the server stops\n";
 *  };
 * ```
 */
#define POST_SERVER_STOP const bool SERVER_ANONYMOUS(pre_start) = impl::PostServerBuilder{} + []()

/**
 * Defines a new CLI option that will be parsed before the server starts
 *
 * Note: The value of the variable will be initialized with its default constructible value
 *
 * Example:
 * ```cpp
 *  DEF_CLI_OPTION(std::string, variableName, "--cli-flag", "Cli description (optional, but recommended)")
 * ```
 */
#define DEF_CLI_OPTION(TYPE, VAR_NAME, FLAG_NAME, ...)                                                                                               \
    TYPE       VAR_NAME{};                                                                                                                           \
    const bool SERVER_ANONYMOUS(cli_opt) = impl::register_cli_opt(FLAG_NAME, VAR_NAME)(__VA_ARGS__);

/**
 * Defines a new CLI option that will be parsed before the server starts with a custom default value
 *
 * Note: The value of the variable will be initialized with its default constructible value
 *
 * Example:
 * ```cpp
 *  DEF_CLI_OPTION_DEFAULT(std::string, variableName, "defaultValue", "--cli-flag", "Cli description (optional, but recommended)")
 * ```
 */
#define DEF_CLI_OPTION_DEFAULT(TYPE, VAR_NAME, DEFAULT_VALUE, FLAG_NAME, ...)                                                                        \
    TYPE       VAR_NAME                  = (DEFAULT_VALUE);                                                                                          \
    const bool SERVER_ANONYMOUS(cli_opt) = impl::register_cli_opt(FLAG_NAME, VAR_NAME)(__VA_ARGS__);

/**
 * Main function that handles CLI parsing, server admin, etc
 * @param app CLI app for parsing
 * @param argc CLI arg count
 * @param argv CLI args
 * @return program status code
 */
int real_main(CLI::App &&app, int argc, char **argv);

/**
 * Runs an interactive session with the server
 * @param shouldStop Reference to flag about whether the server should stop
 */
void interactive_session(bool &shouldStop);

/**
 * If DEFINE_SERVER_MAIN is defined, then a main method will be provided
 */
#ifdef DEFINE_SERVER_MAIN

/**
 * APP_INFO defines information about the application on the CLI (such as the app name)
 */
#ifndef APP_INFO
#warning "You should define APP_INFO to be about your application"
#define APP_INFO "Please define APP_INFO"
#endif

namespace impl {
    std::vector<server_pre_callback> serverPreStartCallbacks =
        {};  // NOLINT(misc-definitions-in-headers,cppcoreguidelines-avoid-non-const-global-variables)

    std::vector<server_endpoint_callback> serverEndpointCallbacks =
        {};  // NOLINT(misc-definitions-in-headers,cppcoreguidelines-avoid-non-const-global-variables)

    std::vector<cli_opt_register_callback> cliOptionRegisterCallbacks =
        {};  // NOLINT(misc-definitions-in-headers,cppcoreguidelines-avoid-non-const-global-variables)

    std::vector<server_post_callback> serverPostCallbacks =
        {};  // NOLINT(misc-definitions-in-headers,cppcoreguidelines-avoid-non-const-global-variables)
    std::map<std::string, std::vector<interactive_line_callback>, std::less<>> interactiveCallbacks =
        {};  // NOLINT(misc-definitions-in-headers,cppcoreguidelines-avoid-non-const-global-variables)
}  // namespace impl

void interactive_session(bool &shouldStop) {
    auto getCommand = [](const std::string &line) -> std::optional<std::tuple<std::string, std::string>> {
        constexpr auto searchChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_";

        auto commandStart = line.find_first_of(searchChars);
        if (commandStart == std::string::npos) {
            return std::nullopt;
        }
        auto firstWordBreak = line.find_first_not_of(searchChars, commandStart);
        if (firstWordBreak == std::string::npos) {
            return std::make_tuple(std::string{line.substr(commandStart)}, std::string{line.substr(commandStart)});
        }

        return std::make_tuple(std::string{line.substr(commandStart, firstWordBreak)}, std::string{line.substr(commandStart)});
    };

    std::stringstream helpStream{};
    helpStream << "Available Commands:\n\t- help\n\t- quit\n\t- detach";

    for (const auto &ic : impl::interactiveCallbacks) {
        helpStream << "\n\t- " << ic.first;
    }
    helpStream << "\n";
    auto helpString = helpStream.str();

    std::cout << "Welcome to the server!\n> " << std::flush;
    bool shouldQuit = false;
    for (std::string line; std::getline(std::cin, line) && !shouldQuit;) {
        defer {
            if (!shouldQuit) {
                std::cout << "> " << std::flush;
            };
        };
        auto commandOpt = getCommand(line);
        if (!commandOpt) {
            continue;
        }
        auto [ command, lineToProcess ] = *commandOpt;

        if (lineToProcess == "help") {
            std::cout << helpString << std::endl;
            continue;
        }
        else if (lineToProcess == "quit") {
            std::cout << "Shutting down the server...\n";
            shouldStop = true;
            shouldQuit = true;
            break;
        }
        else if (lineToProcess == "detach") {
            std::cout << "Detaching stdin from the server...\n";
            shouldQuit = true;
            break;
        }

        bool processedLine = false;
        auto iter          = impl::interactiveCallbacks.find(command);
        if (iter != impl::interactiveCallbacks.end()) {
            for (const auto &callback : iter->second) {
                callback(lineToProcess, processedLine, shouldQuit);
                if (processedLine || shouldQuit) {
                    break;
                }
            }
        }

        if (!processedLine) {
            std::cout << "Unknown command '" << command << "'. Type 'help' for a list of commands.\n";
        }
    }
}

int main(int argc, char **argv) {
    CLI::App app{APP_INFO};

    for (const auto &cliRegister : impl::cliOptionRegisterCallbacks) {
        cliRegister(app);
    }
    auto res = real_main(std::move(app), argc, argv);

    spdlog::info("Running server cleanup steps...");
    for (const auto &postServer : impl::serverPostCallbacks) {
        postServer();
    }

    spdlog::info("Server cleanup done.");
    return res;
}  // NOLINT(misc-definitions-in-headers,cppcoreguidelines-avoid-non-const-global-variables)


#include "openapi.h"
DEF_ROUTE("/", HTTP_GET, HTTP_POST)([](const Req &req, Res &res) { res.set_content(htmlDef, mimeHtml); });

DEF_ROUTE("/openapi.json", HTTP_GET)([](const Req &req, Res &res) { res.set_content(openapiDef, mimeJson); });
#endif
