#include "server/unsafe/test-server.h"
#include <spdlog/spdlog.h>
#include <cuid/cuid.h>

[[nodiscard]] std::future<void> start_server(const Server &server, int port, const std::string &host);

std::future<void> start_server(const Server &server, int port, const std::string &host) {
    for (const auto &preCallback : impl::serverPreStartCallbacks) {
        preCallback();
    }

    for (const auto &endpoint : impl::serverEndpointCallbacks) {
        endpoint(server);
    }

    std::visit(
        common::core::overload{[](auto &s) {
            s->set_error_handler([](const Req &req, Res &res) {
                if (res.status == 404) {
                    spdlog::warn("[tid={}] [msg=missing_path] [path={}] [status={}]", res.get_header_value("tid"), req.method, req.path, res.status);
                }
                else if (res.status < 500) {
                    spdlog::warn("[tid={}] [msg=user_error_on_path] [method={}] [path={}] [status={}]",
                                 res.get_header_value("tid"),
                                 req.method,
                                 req.path,
                                 res.status);
                }
                else {
                    spdlog::critical("[tid={}] [msg=server_error_on_path] [method={}] [path={}] [status={}]",
                                     res.get_header_value("tid"),
                                     req.method,
                                     req.path,
                                     res.status);
                }
            });

            auto generator = std::make_shared<cuid::Generator>(cuid::CUID_LONG | cuid::CUID_REGENERATE_FINGERPRINT | cuid::CUID_THREAD);
            constexpr auto isSslServer = std::is_same_v<std::decay_t<decltype(s)>, std::shared_ptr<httplib::SSLServer>>;
            s->set_pre_routing_handler([generator](const Req &req, Res &res) {
                res.set_header("tid", generator->cuid());
                if (req.has_header("tid")) {
                    auto oldTid = req.get_header_value("tid");
                    if (oldTid.size() > 64) {
                        oldTid = oldTid.substr(0, 61) + "...";
                    }
                    if (!oldTid.empty()) {
                        spdlog::info("[tid={}] [msg=remapping_tid] [old_tid={}]", res.get_header_value("tid"), oldTid);
                    }
                }
                else {
                    res.set_header("tid", req.get_header_value("tid"));
                }
                spdlog::info("[tid={}] [msg=incoming_request] [method={}] [path={}]", res.get_header_value("tid"), req.method, req.path);
                if constexpr (isSslServer) {
                    return httplib::Server::HandlerResponse::Unhandled;
                }
                else {
                    return httplib::SSLServer::HandlerResponse::Unhandled;
                }
            });

            s->set_post_routing_handler([](const Req &req, Res &res) {
                if (res.has_header("tid")) {
                    spdlog::info("[tid={}] [msg=request_finished] [method={}] [path={}] [status={}]",
                                 res.get_header_value("tid"),
                                 req.method,
                                 req.path,
                                 res.status);
                }
            });

            s->set_exception_handler([](const auto &req, Res &res, const std::exception_ptr &ep) {
                try {
                    std::rethrow_exception(ep);
                }
                catch (std::exception &e) {
                    std::string tid = (res.has_header("tid")) ? res.get_header_value("tid") : "unknown";
                    spdlog::critical("[tid={}] [msg=unhandled_exception] [method={}] [path={}] [exception={}]",
                                     res.get_header_value("tid"),
                                     req.method,
                                     req.path,
                                     e.what());
                }
                catch (...) {
                    spdlog::critical("[tid={}] [msg=unhandled_exception] [method={}] [path={}] [exception={}]",
                                     res.get_header_value("tid"),
                                     req.method,
                                     req.path,
                                     "unknown");
                }
                res.status = 500;
                res.set_content("Server Error", mimeText);
            });
        }},
        server);

    return std::async(std::launch::async, [ = ]() {
        std::visit(common::core::overload{[ host, port ](auto &s) {
                       if constexpr (std::is_same_v<std::decay_t<decltype(s)>, std::shared_ptr<httplib::SSLServer>>) {
                           spdlog::info("Starting server on https://{}:{}", host, port);
                       }
                       else {
                           spdlog::info("Starting server on http://{}:{}", host, port);
                       }
                       s->listen(host, port);
                   }},
                   server);
    });
}

int real_main(CLI::App &&app, int argc, char **argv) {
    std::string testNode{};
    app.add_option("--test-node", testNode, "Path to node js for testing. Will stop server after tests run");

    std::string testScript = std::filesystem::current_path().parent_path() / "integration-tester" / "test.js";
    app.add_option("--test-script", testScript, "Path to script file for testing. Defaults to ../integration-tester/test.js");

    std::string testDir = std::filesystem::current_path();
    app.add_option("--test-directory", testDir, "Directory holding .http files which outline HTTP requests. Defaults to current working directory");

    std::string testEnv = "local-dev";
    app.add_option("--test-env", testEnv, "Environment for testing. Defaults to local-dev");

    std::string testEnvFile = std::filesystem::current_path() / "http-client.env.json";
    app.add_option("--test-env-file", testEnvFile, "Environment file for testing. Defaults to http-client.env.json");

    std::string logLevel = "info";
    app.add_option("--log-level", logLevel, "Log level for logging. Defaults to info. Values: trace, debug, info, warn, err, critical, silent");

    int testClientCount = 1;
    app.add_option("--test-client-count", testClientCount, "Number of clients to run for integration testing. Defaults to 1");

    bool interactive = false;
    app.add_flag("--interactive", interactive, "If present, will start an interactive terminal");

    int port = SERVER_DEFAULT_PORT;
    app.add_option("-p,--port", port, "Port to use for server. Defaults to " + std::to_string(port));

#if defined CPPHTTPLIB_OPENSSL_SUPPORT && SERVER_USE_HTTPS
    std::string certFile = "";
    std::string keyFile  = "";

    app.add_flag("--cert-file", certFile, "Certificate file to use");
    app.add_flag("--key-file", keyFile, "Key file to use");
#endif

    try {
        app.parse(argc, argv);
    }
    catch (const CLI::ParseError &e) {
        return app.exit(e);
    }

    std::transform(logLevel.begin(), logLevel.end(), logLevel.begin(), [](unsigned char c) { return std::tolower(c); });

    std::string nodeLogLevel = "info";

    if (logLevel == "trace") {
        spdlog::set_level(spdlog::level::trace);
        nodeLogLevel = "debug";  // for Node
    }
    else if (logLevel == "debug") {
        spdlog::set_level(spdlog::level::debug);
        nodeLogLevel = "debug";
    }
    else if (logLevel == "info") {
        spdlog::set_level(spdlog::level::info);
        nodeLogLevel = "info";
    }
    else if (logLevel == "warn") {
        spdlog::set_level(spdlog::level::warn);
        nodeLogLevel = "warn";
    }
    else if (logLevel == "error") {
        spdlog::set_level(spdlog::level::err);
        nodeLogLevel = "error";
    }
    else if (logLevel == "critical") {
        spdlog::set_level(spdlog::level::critical);
        nodeLogLevel = "error";
    }
    else if (logLevel == "silent") {
        spdlog::set_level(spdlog::level::off);
        nodeLogLevel = "info --no-log --silent";
    }
    else {
        spdlog::warn("Unknown log level {}, defaulting to info", logLevel);
        spdlog::set_level(spdlog::level::info);
    }

    Server server;
#if defined CPPHTTPLIB_OPENSSL_SUPPORT && SERVER_USE_HTTPS
    if (!certFile.empty() && !keyFile.empty()) {
        server = std::make_shared<httplib::SSLServer>(certFile.c_str(), keyFile.c_str());
    }
    else {
        server = std::make_shared<httplib::Server>();
    }
#else
    server = std::make_shared<httplib::Server>();
#endif

    const auto *host = "0.0.0.0";

    auto f = start_server(server, port, host);

    std::visit(
        [ host, port ](const auto &server) {
            server->wait_until_ready();
            if constexpr (std::is_same_v<std::decay_t<decltype(server)>, std::shared_ptr<httplib::SSLServer>>) {
                spdlog::info("Server started on https://{}:{}", host, port);
            }
            else {
                spdlog::info("Server started on http://{}:{}", host, port);
            }
        },
        server);

    auto retCode    = 0;
    bool shouldStop = false;

    if (!testNode.empty()) {
        const auto ready = std::visit(
            [](const auto &server) {
                if (!server->is_running()) {
                    std::cerr << "Server is no longer running!\n";
                    return false;
                }
                return true;
            },
            server);

        if (ready) {
            std::this_thread::sleep_for(std::chrono::seconds(4));
            auto [ code, output ] = server::unsafe::process::exec::Command::run(testNode + " " + testScript + " --test-directory " + testDir
                                                                                + " --env-file " + testEnvFile + " --env " + testEnv + " --log-level "
                                                                                + nodeLogLevel + " --num-users " + std::to_string(testClientCount))
                                        .get();
            if (code) {
                spdlog::error("INTEGRATION TESTS FAILED!\n");
                retCode = 1;
            }
            else {
                spdlog::info("INTEGRATION TESTS PASSED!\n");
                retCode = code;
            }

            spdlog::debug("====================TEST OUTPUT====================\n\n{}\n", output);
            spdlog::debug("====================END TEST OUTPUT====================\n\n");
        }
        else {
            retCode = 5;
        }

        shouldStop = true;
    }
    else if (interactive) {
        interactive_session(shouldStop);
    }

    if (shouldStop) {
        std::visit(
            [](const auto &server) {
                spdlog::info("Shutting down the server...");
                server->stop();
            },
            server);
    }

    f.wait();
    spdlog::info("Server stopped. Ending program. Return code: {}", retCode);
    return retCode;
}
