find_program(NPM_BUN bun npm REQUIRED)
find_program(NODE bun node nodejs REQUIRED)

set(SERVER_COMMON_DIR ${CMAKE_CURRENT_LIST_DIR}/../common)

function(add_test_server)
  if(NOT EXCLUDE_TEST_SERVERS)
    set(SINGLE_VALUE_KEYWORDS
        LIBRARY_NAME
        OPENAPI_TARGET_NAME
        TEST_SERVER_TARGET_NAME
        SERVER_DIR
        OPENAPI_DIR
        OUT_DIR
        BYPRODUCT_DIR
        DOCS_DIR
        DOCS_NAME
        CLIENT_COUNT)
    set(MULTI_VALUE_KEYWORDS SERVER_FILES)

    if(add_test_server_CLIENT_COUNT)
      set(CLIENT_COUNT ${add_test_server_CLIENT_COUNT})
    else()
      set(CLIENT_COUNT 10)
    endif()

    if(add_test_server_LIBRARY_NAME)
      set(LIBRARY_NAME ${add_test_server_LIBRARY_NAME})
    else()
      set(LIBRARY_NAME ${PROJECT_NAME})
    endif()

    if(add_test_server_OPENAPI_TARGET_NAME)
      set(OPENAPI_TARGET_NAME ${add_test_server_OPENAPI_TARGET_NAME})
    else()
      set(OPENAPI_TARGET_NAME openapi-${PROJECT_NAME})
    endif()

    if(add_test_server_TEST_SERVER_TARGET_NAME)
      set(TEST_SERVER_TARGET_NAME ${add_test_server_TEST_SERVER_TARGET_NAME})
    else()
      set(TEST_SERVER_TARGET_NAME ${PROJECT_NAME}-test-server)
    endif()

    if(add_test_server_SERVER_DIR)
      set(SERVER_DIR ${add_test_server_SERVER_DIR})
    else()
      set(SERVER_DIR ${CMAKE_CURRENT_SOURCE_DIR}/test-server)
    endif()

    if(add_test_server_SERVER_FILES)
      set(SERVER_FILES ${add_test_server_SERVER_FILES})
    else()
      set(SERVER_FILES ${SERVER_DIR}/server.cpp)
    endif()

    if(add_test_server_OPENAPI_DIR)
      set(OPENAPI_DIR ${add_test_server_OPENAPI_DIR})
    else()
      set(OPENAPI_DIR ${SERVER_COMMON_DIR}/openapi)
    endif()

    if(add_test_server_OUT_DIR)
      set(OUT_DIR ${add_test_server_OUT_DIR})
    else()
      set(OUT_DIR ${CMAKE_CURRENT_BINARY_DIR}/server)
    endif()

    if(add_test_server_BYPRODUCT_DIR)
      set(BYPRODUCT_DIR ${add_test_server_BYPRODUCT_DIR})
    else()
      set(BYPRODUCT_DIR ${CMAKE_CURRENT_BINARY_DIR}/server)
    endif()

    if(add_test_server_DOCS_DIR)
      set(DOCS_DIR ${add_test_server_DOCS_DIR})
    else()
      set(DOCS_DIR ${CMAKE_SOURCE_DIR}/docs/api)
    endif()

    if(add_test_server_DOCS_NAME)
      set(DOCS_NAME ${add_test_server_DOCS_NAME})
    else()
      set(DOCS_NAME ${PROJECT_NAME})
    endif()

    file(
      WRITE ${OUT_DIR}/openapi.html
      "<!doctype html>\n\
  <html lang='en'>\n\
  <head>\n\
      <meta charset='utf-8'>\n\
      <meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>\n\
      <title>${DOCS_NAME} | API Docs</title>\n\
      <script src='https://unpkg.com/@stoplight/elements/web-components.min.js'></script>\n\
      <link rel='stylesheet' href='https://unpkg.com/@stoplight/elements/styles.min.css'>\n\
      <style>.sl-elements-api.sl-flex.sl-inset-0.sl-h-full > .sl-flex { min-height: 100vh; }</style>\n\
  </head>\n\
  <body>\n\
      <elements-api\n\
          apiDescriptionUrl='/openapi.json'\n\
          router='hash'\n\
          layout='sidebar'\n\
      />\n\
  </body>\n\
  </html>")

    file(
      WRITE ${OUT_DIR}/openapi.h
      "#pragma once \n\
  \n\
  extern const char* openapiDef;\n\
  extern const char* htmlDef;\n\
  ")

    add_custom_target(
      ${OPENAPI_TARGET_NAME}
      COMMAND ${NPM_BUN} run openapi-gen -- ${SERVER_DIR}
              ${OUT_DIR}/openapi.json
      COMMAND ${NODE} gen_cpp_file.js openapiDef ${OUT_DIR}/openapi.json
              ${OUT_DIR}/openapi_json.cpp
      COMMAND ${NODE} gen_cpp_file.js htmlDef ${OUT_DIR}/openapi.html
              ${OUT_DIR}/openapi_html.cpp
      COMMAND ${NPM_BUN} run pdf-gen -- --out ${DOCS_DIR}/${DOCS_NAME}.pdf
              --spec ${OUT_DIR}/openapi.json
      WORKING_DIRECTORY ${OPENAPI_DIR}
      BYPRODUCTS ${BYPRODUCT_DIR}/openapi_json.cpp
                 ${BYPRODUCT_DIR}/openapi_html.cpp
                 ${BYPRODUCT_DIR}/openapi-dummy.h)

    add_executable(
      ${TEST_SERVER_TARGET_NAME}
      ${SERVER_DIR}/server.cpp ${BYPRODUCT_DIR}/openapi_json.cpp
      ${BYPRODUCT_DIR}/openapi_html.cpp ${BYPRODUCT_DIR}/openapi-dummy.h)
    target_link_libraries(${TEST_SERVER_TARGET_NAME}
                          PRIVATE ${LIBRARY_NAME} server::common server::unsafe)
    target_include_directories(${TEST_SERVER_TARGET_NAME} PRIVATE ${OUT_DIR})

    add_dependencies(main_build ${TEST_SERVER_TARGET_NAME})
    clang_tidy(${TEST_SERVER_TARGET_NAME})

    target_compile_definitions(
      ${TEST_SERVER_TARGET_NAME} PRIVATE -DSERVER_WITH_PODMAN_CALLS=1
                                         -DSERVER_WITH_CLI_ACCESS=1)
  endif()
endfunction()
