cmake_minimum_required(VERSION 3.25)
project(${SERVICE_NAME}-service-api)

include(../prod_ready_checks.cmake)

include(${CMAKE_SOURCE_DIR}/cmake/deps.cmake)

add_executable(${PROJECT_NAME} src/main.cpp src/api/endpoints.cpp)
add_dependencies(main_build ${PROJECT_NAME})
target_link_libraries(${PROJECT_NAME} PUBLIC httplib::httplib server::common server::compute server::data common::core cuid::thread_safe)
target_include_directories(${PROJECT_NAME} PRIVATE include)
clang_tidy(${PROJECT_NAME})

ensure_no_disallowed_libs(${PROJECT_NAME} ${DISALLOWED_PRODUCTION_LIBRARIES})
ensure_required_libs(${PROJECT_NAME} ${REQUIRED_PRODUCTION_LIBRARIES})

format_current_cmake()

make_docs()
