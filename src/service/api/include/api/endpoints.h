#pragma once

#include "httplib.h"

namespace service::api {
    auto initialize_server(httplib::SSLServer& server) -> void;
}
