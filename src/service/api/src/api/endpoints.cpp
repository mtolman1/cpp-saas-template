#include <api/endpoints.h>
#include <nlohmann/json.hpp>

auto service::api::initialize_server(httplib::SSLServer& server) -> void {
    server.Get("/", [](const httplib::Request& req, httplib::Response& res) {
        nlohmann::json val{};
        val["message"] = "Hello World";
        res.set_content(nlohmann::to_string(val), "application/json");
    });
}