#include <server/common/CLI11.hpp>
#include <spdlog/spdlog.h>
#include <future>
#include "httplib.h"
#include <api/endpoints.h>
#include <cuid/cuid.h>

#ifndef SERVICE_DEFAULT_PORT
#define SERVICE_DEFAULT_PORT 8080
#endif

#ifndef APP_INFO
#warning "You should define APP_INFO to be about your service and delete this line"
#define APP_INFO "Please define APP_INFO"
#endif

#define GET_ENV_IF_EMPTY(STRING_VAR_NAME, ENV_NAME)                                                                                 \
if (STRING_VAR_NAME.empty()) {                                                                                                                                                \
    auto *envTmp = std::getenv(ENV_NAME);                                                                                                        \
    if (envTmp != nullptr) {                                                                                                                     \
        STRING_VAR_NAME = envTmp;                                                                                                                \
    }                                                                                                                                            \
}

std::future<void> start_server(httplib::SSLServer &server, int port, const std::string &host);
void set_log_level(std::string& logLevel);

int main(int argc, char** argv) {
    CLI::App app{APP_INFO};

    std::string logLevel = "info";
    app.add_option("--log-level", logLevel, "Log level for logging. Defaults to info. Values: trace, debug, info, warn, err, critical, silent");

    int port = SERVICE_DEFAULT_PORT;
    app.add_option("-p,--port", port, "Port to use for server. Defaults to " + std::to_string(port));

    std::string host = "0.0.0.0";
    app.add_option("--hostname", "Hostname or host IP to use. Defaults to 0.0.0.0.");

    std::string certFile = "";
    std::string keyFile  = "";

    app.add_flag("--cert-file", certFile, "Certificate file to use");
    app.add_flag("--key-file", keyFile, "Key file to use");

    try {
        app.parse(argc, argv);
    }
    catch (const CLI::ParseError &e) {
        return app.exit(e);
    }
    GET_ENV_IF_EMPTY(certFile, "CERT");
    GET_ENV_IF_EMPTY(keyFile, "KEY");
    if (certFile.empty() || keyFile.empty()){
        throw std::runtime_error("SSL certificate cert file and key file not provided!");
    }

    auto serverApp = httplib::SSLServer{certFile.c_str(), keyFile.c_str()};

    // Default error handler
    serverApp.set_error_handler([](const httplib::Request &req, httplib::Response &res) {
        if (res.status == 404) {
            spdlog::warn("[tid={}] [msg=missing_path] [path={}] [status={}]", res.get_header_value("tid"), req.method, req.path, res.status);
        }
        else if (res.status < 500) {
            spdlog::warn("[tid={}] [msg=user_error_on_path] [method={}] [path={}] [status={}]",
                         res.get_header_value("tid"),
                         req.method,
                         req.path,
                         res.status);
        }
        else {
            spdlog::critical("[tid={}] [msg=server_error_on_path] [method={}] [path={}] [status={}]",
                             res.get_header_value("tid"),
                             req.method,
                             req.path,
                             res.status);
        }
    });

    auto generator = std::make_shared<cuid::Generator>(cuid::CUID_LONG | cuid::CUID_REGENERATE_FINGERPRINT | cuid::CUID_THREAD);
    serverApp.set_pre_routing_handler([generator](const httplib::Request &req, httplib::Response &res) {
        res.set_header("tid", generator->cuid());
        if (req.has_header("tid")) {
            auto oldTid = req.get_header_value("tid");
            if (oldTid.size() > 64) {
                oldTid = oldTid.substr(0, 61) + "...";
            }
            if (!oldTid.empty()) {
                spdlog::info("[tid={}] [msg=remapping_tid] [old_tid={}]", res.get_header_value("tid"), oldTid);
            }
        }
        else {
            res.set_header("tid", req.get_header_value("tid"));
        }
        spdlog::info("[tid={}] [msg=incoming_request] [method={}] [path={}]", res.get_header_value("tid"), req.method, req.path);
        return httplib::Server::HandlerResponse::Unhandled;
    });

    // TODO: Replace this as needed (or override in the init method)
    serverApp.set_exception_handler([](const httplib::Request &req, httplib::Response &res, const std::exception_ptr &ep) {
        try {
            std::rethrow_exception(ep);
        }
        catch (std::exception &e) {
            std::string tid = (res.has_header("tid")) ? res.get_header_value("tid") : "unknown";
            spdlog::critical("[tid={}] [msg=unhandled_exception] [method={}] [path={}] [exception={}]",
                             res.get_header_value("tid"),
                             req.method,
                             req.path,
                             e.what());
        }
        catch (...) {
            spdlog::critical("[tid={}] [msg=unhandled_exception] [method={}] [path={}] [exception={}]",
                             res.get_header_value("tid"),
                             req.method,
                             req.path,
                             "unknown");
        }
        res.status = 500;
        res.set_content("Server Error", "text/plain");
    });

    service::api::initialize_server(serverApp);
    auto res = start_server(serverApp, port, host);

    serverApp.wait_until_ready();
    spdlog::info("Server started on https://{}:{}", host, port);

    // Do any background tasks here

    res.wait();
}

void set_log_level(std::string& logLevel) {
    std::transform(logLevel.begin(), logLevel.end(), logLevel.begin(), [](unsigned char c) { return std::tolower(c); });

    if (logLevel == "trace") {
        spdlog::set_level(spdlog::level::trace);
    }
    else if (logLevel == "debug") {
        spdlog::set_level(spdlog::level::debug);
    }
    else if (logLevel == "info") {
        spdlog::set_level(spdlog::level::info);
    }
    else if (logLevel == "warn") {
        spdlog::set_level(spdlog::level::warn);
    }
    else if (logLevel == "error") {
        spdlog::set_level(spdlog::level::err);
    }
    else if (logLevel == "critical") {
        spdlog::set_level(spdlog::level::critical);
    }
    else if (logLevel == "silent") {
        spdlog::set_level(spdlog::level::off);
    }
    else {
        spdlog::set_level(spdlog::level::info);
        spdlog::warn("Unknown log level {}, defaulting to info", logLevel);
    }
}

std::future<void> start_server(httplib::SSLServer &server, int port, const std::string &host) {
    return std::async(std::launch::async, [=,&server]() {
        spdlog::info("Starting server on https://{}:{}", host, port);
        server.listen(host, port);
    });
}
