# Hypermedia Service

This module is for the full, proper, production ready hypermedia service. There is no built-in
integration tester; it is assumed that you will have a proper CI/CD process for running integration
tests. The built-in integration tester is not present to prevent the service from having CLI access.

This service is meant to return HTML pages and/or snippets (snippets can be used with something like
[HTMX](https://htmx.org/)). The HTML should include fully processed data that's been properly
encoded/sanitized (HTML encoding, JS encoding, etc).

This service is meant for server-rendered interfaces. If you wish to just have a SPA which uses the
API service, or you wish for this service to only be usable via an API/RPC (e.g. in service-oriented
architectures), then feel free to remove this CMake target.

## Documentation

Doxygen documentation is automatically created for the project. The CMake targets which build the docs include:

* main_build (does formatting, executable building, etc)
* doc (builds all docs)
* doc-saas-template-common-core (the name changes if you change the root CMake's PROJECT_NAME)

Documentation is stored under you're output folder in docs/code/common/core. We output dockbook
templates, HTML code, latex templates, and man pages.

## Current Dependencies (remove, update, or move this section as desired)

**IMPORTANT!** This project does **NOT** come with a Localization/Internationalization library!
If you need localization/internationalization, please integrate a library which works with your
translation pipeline. Some possible libraries include:

* [ICU4C](https://unicode-org.github.io/icu/userguide/icu4c/)
* [ICU4X](https://github.com/unicode-org/icu4x)
  * [See the C++ tutorial](https://github.com/unicode-org/icu4x/blob/main/docs/tutorials/cpp.md)
* [tinygettext](https://github.com/tinygettext/tinygettext)
* [Boost Locale](https://github.com/boostorg/locale)
* [GNU gettext](https://github.com/gitGNU/gnu_gettext) **!!GPL LICENCE!!**

The example code in this module can (and should) be replaced. Additionally, feel free to unlink
any example libraries that are linked. The libraries linked by default are:

### httplib

https://github.com/yhirose/cpp-httplib

Basic HTTP server/client library for C++.

Alternatives include:

* [CppCMS](http://cppcms.com/wikipp/en/page/main)
  * Also includes HTML templating
* [drogon](https://github.com/drogonframework/drogon)
* [userver](https://userver.tech/)
* [pistache](https://pistacheio.github.io/pistache/)
* [oatpp](https://oatpp.io/)
* [crow](https://github.com/CrowCpp/Crow)
* [facil.io](http://facil.io/)
* [kore](https://kore.io/)
* [libonion](https://www.coralbits.com/libonion/)

### server::common

The server common module.

### server::compute

The server compute module.

### server::data

The server data module.

### bustache

https://github.com/jamboree/bustache

A mustache template implementation. Alternatives mustache implementations include:

* [kainjow/Mustache](https://github.com/kainjow/Mustache)
* [mstche](https://github.com/no1msd/mstch)

Additionally, there are other template engines:

* [Jinja2C++](https://github.com/jinja2cpp/Jinja2Cpp)
* [inja](https://github.com/pantor/inja)
* [Clearsilver](http://clearsilver.net/)
* [TEng](https://seznam.github.io/teng/index.html)
* [CppCMS](http://cppcms.com/wikipp/en/page/main)
  * Also includes an HTTP server

Additionally, you could use an HTML builder library.

* [HtmlBuilder](https://github.com/SRombauts/HtmlBuilder)
* [CTML](https://github.com/tinfoilboy/CTML)
* [html-plus-plus](https://github.com/csb6/html-plus-plus) - though this one is not recommended by the author
  * It may inspire someone to make something like this, though I doubt it
    * Notable improvements for someone wanting to try:
      * Typed attributes (Instead of `"href=examle.com"`, do `Href<"example.com">`)
        * Maybe even add type checking with `static_assert` and `if constexpr`
      * Make it HTML5 complete
      * Add support for custom [web components](https://developer.mozilla.org/en-US/docs/Web/API/Web_Components)
      * Add localization support (somehow)
        * Even better, add compile time localization support (IDK how you'll do that)
      * Add HTML encoding
      * JS support somehow? Actually, no, that sounds like a terrible idea
        * TypeScript support sounds simultaneously both better and worse
      * If you couldn't tell, this section is a joke. Though I may end up making it (it does sounds funny)

### cuid

https://gitlab.com/mtolman/cuid

A port of [cuid](https://www.npmjs.com/package/cuid) (V1) with a few custom extensions. Used for linking logs to the same request (assigns each request a cuid
and then logs the request).

> The port was made before CUID V2 came out. CUID V1 is deprecated since it's possible to predict
> future or past values, and that ability to predict future or past values could lead to security
> issues in some instances where a user being able to predict future ids could lead to information
> leakage (e.g. knowing how many accounts a client has, bypassing insufficient access controls,
> etc).
>
> For this project, we're just using CUID for log association and correlation, which is not at high
> risk for id prediction. Additionally, we don't have to have extremely high collision resistance
> (i.e. we don't have to worry about new ids colliding with old ids from 10 years ago). Generally,
> any ofthe id generation methodologies listed here should work for this purpose.
>
> The CUID is returned in an HTTP header so developers can quickly find the logs associated with a
> request.
>
> **IMPORTANT:** While CUID V1 is acceptable for just logging, don't blindly use it for other use
> cases (e.g. database ids)! Additionally, if you are needing something secure, make sure you use
> a secure random number generator (the default random number generator for cuid is not secure)!

Alternatives include:

* [stduuid](https://github.com/mariusbancila/stduuid) ([UUID](https://en.wikipedia.org/wiki/Universally_unique_identifier), [C++ proposal](https://github.com/mariusbancila/stduuid/blob/master/P0959.md))
* [nanoid_cpp](https://github.com/mcmikecreations/nanoid_cpp) ([Nanoid](https://github.com/ai/nanoid))
* [ulid](https://github.com/suyash/ulid) ([Ulid](https://github.com/ulid/spec))
* [snowflake-cpp](https://github.com/sniper00/snowflake-cpp) ([Snowflake IDs](https://en.wikipedia.org/wiki/Snowflake_ID))
* [KsuidCPP](https://github.com/medamine101/KsuidCPP) ([KSUID](https://github.com/segmentio/ksuid))

### Project Example Usage

ADD EXAMPLES OF YOUR COMMON CODE HERE
