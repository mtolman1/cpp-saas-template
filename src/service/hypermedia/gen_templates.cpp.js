const fs = require('fs')
const path = require("path");

const root = path.join(__dirname, 'templates')
const templates = findExtension('.html', root)

let mapStatement = 'static std::map<std::string, std::string, std::less<>> template_map = {\n';
let strStatements = ''

templates.forEach(template => {
    const templateName = template.replace(root + path.sep, '')
    const contents = fs.readFileSync(template, 'utf-8');
    const encoded = [...(new TextEncoder()).encode(contents)].map(c => printable(c) ? String.fromCharCode(c) : "\\x" + c.toString(16) + "\" \"")
    const varName = templateName.replace(/[^\w]/g, '__');

    strStatements += `static const char* ${varName} = "${encoded.join('')}";\n`

    mapStatement += `\t{"${templateName}", std::string{${varName}, ${encoded.length}}},\n`;
})

mapStatement += "};"

const file = `
#include <map>
#include <vector>
#include <hypermedia/templates.h>

${strStatements}
${mapStatement}

auto service::hypermedia::templates::template_from_binary(const std::string& name) -> std::optional<std::string> {
    auto it = template_map.find(name);
    if (it == template_map.end()) {
        return std::nullopt;
    }
    return it->second;
}
`

fs.writeFileSync(process.argv[2] || (path.join(__dirname, 'template.cpp')), file)

function printable(char) {
    return char >= 32 && char < 127 && char !== '"'.charCodeAt(0) && char !== '\r'.charCodeAt(0) && char !== '\n'.charCodeAt(0)
}

function findExtension(ext, dir) {
    return fs.readdirSync(dir)
        .flatMap(file => {
            const filePath = path.join(dir, file)
            if (!fs.lstatSync(filePath).isDirectory()) {
                return path.extname(file) === ext ? [filePath] : []
            }
            return findExtension(ext, filePath)
        })
}
