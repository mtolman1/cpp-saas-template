#pragma once

#include "hypermedia/templates.h"
#include <bustache/format.hpp>
#include <bustache/render.hpp>
#include <bustache/render/string.hpp>
#include <unordered_map>
#include <filesystem>

namespace service::hypermedia {
    struct BinaryLoader{};

    struct FileLoader {
        std::filesystem::path directory;
    };

    using Loader = std::variant<BinaryLoader, FileLoader>;

    auto load_template(const std::string& templateName, const Loader& loader = BinaryLoader{}) -> std::optional<std::string>;

    template<typename T = std::unordered_map<std::string, std::string>, typename B = decltype(bustache::escape_html)>
    auto hydrate(
            const std::string& templateName,
            const T& data = {},
            const Loader& loader = BinaryLoader{},
            const B& escape = bustache::escape_html
    ) -> std::optional<std::string> {
        auto templateOpt = load_template(templateName, loader);
        if (!templateOpt.has_value()) {
            return std::nullopt;
        }
        auto formatter = bustache::format(*templateOpt);
        return bustache::to_string(formatter(data).escape(escape));
    }

    template<typename C, typename T = std::unordered_map<std::string, std::string>, typename B = decltype(bustache::escape_html)>
    auto hydrate(
            const C& contextHandler,
            const std::string& templateName,
            const T& data = {},
            const Loader& loader = BinaryLoader{},
            const B& escape = bustache::escape_html
    ) -> std::optional<std::string> {
        auto templateOpt = load_template(templateName, loader);
        if (!templateOpt.has_value()) {
            return std::nullopt;
        }
        auto formatter = bustache::format(*templateOpt);
        return bustache::to_string(formatter(data).context(contextHandler).escape(escape));
    }
}
