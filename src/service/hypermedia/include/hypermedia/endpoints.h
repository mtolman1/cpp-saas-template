#pragma once

#include "httplib.h"

namespace service::hypermedia {
    auto initialize_server(httplib::SSLServer& server) -> void;
}
