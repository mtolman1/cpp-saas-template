#pragma once

#include <optional>
#include <string>
#include <spdlog/spdlog.h>
#include <filesystem>

namespace service::hypermedia::templates {
    auto template_from_binary(const std::string& name) -> std::optional<std::string>;

    auto template_from_file(const std::filesystem::path& templateDir, const std::string& templateName) -> std::optional<std::string>;
}