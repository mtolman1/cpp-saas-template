#include <hypermedia.h>
#include <hypermedia/templates.h>
#include <common/core/visitor.h>
#include <common/core/defer.h>
#include <spdlog/spdlog.h>
#include <fstream>

#pragma clang diagnostic push
#pragma ide diagnostic ignored "cppcoreguidelines-owning-memory"
auto service::hypermedia::load_template(const std::string& templateName, const Loader& loader) -> std::optional<std::string> {
    return std::visit(
        common::core::overload{
            [&templateName](const BinaryLoader&) -> std::optional<std::string> {
                return templates::template_from_binary(templateName);
            },
            [&templateName](const FileLoader& fileLoader) -> std::optional<std::string> {
                return templates::template_from_file(fileLoader.directory, templateName);
            }
        },
        loader
    );
};
#pragma clang diagnostic pop

auto service::hypermedia::templates::template_from_file(const std::filesystem::path &templateDir,
                                                                const std::string &templateName) -> std::optional<std::string> {
    const auto file = std::filesystem::absolute((templateDir / templateName));
    const auto dirRoot = std::filesystem::absolute(templateDir);
    if (!file.string().starts_with(dirRoot.string())) {
        spdlog::warn("Someone tried to access template file outside of template directory. Template file: {}", templateName);
        return std::nullopt;
    }

    if (!std::filesystem::exists(file)) {
        spdlog::warn("Could not load template {}, template does not exist", templateName);
        return std::nullopt;
    }

    std::ifstream in(file, std::ios::in | std::ios::binary);
    if (!in) {
        spdlog::critical("Unable to open template file {}", templateName);
        return std::nullopt;
    }
    defer { in.close(); };

    std::string contents{};
    in.seekg(0, std::ios::end);
    contents.resize(in.tellg());
    in.seekg(0, std::ios::beg);
    in.read(contents.data(), contents.size());
    return(contents);
}
