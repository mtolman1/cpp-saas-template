#include <server/common/CLI11.hpp>
#include <future>
#include "httplib.h"
#include <hypermedia/templates.h>

#ifndef SERVICE_DEFAULT_PORT
#define SERVICE_DEFAULT_PORT 8080
#endif

#ifndef APP_INFO
#warning "You should define APP_INFO to be about your service and delete this line"
#define APP_INFO "Please define APP_INFO"
#endif

#define GET_ENV_IF_EMPTY(STRING_VAR_NAME, ENV_NAME)                                                                                 \
if (STRING_VAR_NAME.empty()) {                                                                                                                                                \
    auto *envTmp = std::getenv(ENV_NAME);                                                                                                        \
    if (envTmp != nullptr) {                                                                                                                     \
        STRING_VAR_NAME = envTmp;                                                                                                                \
    }                                                                                                                                            \
}

std::future<void> start_server(const httplib::SSLServer &server, int port, const std::string &host);
void set_log_level(std::string& logLevel);

int main(int argc, char** argv) {
    CLI::App app{APP_INFO};

    std::string logLevel = "info";
    app.add_option("--log-level", logLevel, "Log level for logging. Defaults to info. Values: trace, debug, info, warn, err, critical, silent");

    int port = SERVICE_DEFAULT_PORT;
    app.add_option("-p,--port", port, "Port to use for server. Defaults to " + std::to_string(port));

    std::string certFile = "";
    std::string keyFile  = "";

    app.add_flag("--cert-file", certFile, "Certificate file to use");
    app.add_flag("--key-file", keyFile, "Key file to use");

    try {
        app.parse(argc, argv);
    }
    catch (const CLI::ParseError &e) {
        return app.exit(e);
    }
    GET_ENV_IF_EMPTY(certFile, "CERT");
    GET_ENV_IF_EMPTY(keyFile, "KEY");
    if (certFile.empty() || keyFile.empty()){
        throw std::runtime_error("SSL certificate cert file and key file not provided!");
    }

    auto server = httplib::SSLServer{certFile.c_str(), keyFile.c_str()};

    const auto *host = "0.0.0.0";
}

void set_log_level(std::string& logLevel) {
    std::transform(logLevel.begin(), logLevel.end(), logLevel.begin(), [](unsigned char c) { return std::tolower(c); });

    if (logLevel == "trace") {
        spdlog::set_level(spdlog::level::trace);
    }
    else if (logLevel == "debug") {
        spdlog::set_level(spdlog::level::debug);
    }
    else if (logLevel == "info") {
        spdlog::set_level(spdlog::level::info);
    }
    else if (logLevel == "warn") {
        spdlog::set_level(spdlog::level::warn);
    }
    else if (logLevel == "error") {
        spdlog::set_level(spdlog::level::err);
    }
    else if (logLevel == "critical") {
        spdlog::set_level(spdlog::level::critical);
    }
    else if (logLevel == "silent") {
        spdlog::set_level(spdlog::level::off);
    }
    else {
        spdlog::set_level(spdlog::level::info);
        spdlog::warn("Unknown log level {}, defaulting to info", logLevel);
    }
}
