#include <doctest/doctest.h>
#include <hypermedia/templates.h>
#include <hypermedia.h>

TEST_SUITE("Templates") {
    using namespace service::hypermedia::templates;
    using namespace service::hypermedia;
    TEST_CASE("index.html") {
        SUBCASE("Loadable from binary") {
            auto templateFileOpt = template_from_binary("index.html");
            CHECK(templateFileOpt.has_value());
        }

        SUBCASE("Loadable from disk") {
            auto templateFileOpt = template_from_binary("index.html");
            CHECK(templateFileOpt.has_value());
        }

        SUBCASE("Usable") {
            std::unordered_map<std::string, std::string> args = {{"title",   "Home"},
                                                                 {"appName", "My App"}};
            const auto expected = R"HTML(<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Home - My App</title>
</head>
<body>
  <h1>Home</h1>
</body>
</html>)HTML";

            SUBCASE("From binary") {
                auto res = hydrate("index.html", args, BinaryLoader{});
                REQUIRE(res.has_value());
                CHECK_EQ(*res, expected);
            }

            SUBCASE("From File") {
                auto res = hydrate("index.html", args, FileLoader{std::filesystem::current_path() / "templates"});
                REQUIRE(res.has_value());
                CHECK_EQ(*res, expected);
            }
        }
    }
}
